<?php

namespace App\Providers;

use App\Shop\Carts\Repositories\CartRepository;
use App\Shop\Carts\ShoppingCart;
use App\Shop\Categories\Category;
use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Employees\Employee;
use App\Shop\Employees\Repositories\EmployeeRepository;
use App\Shop\Couriers\Courier;
use App\Shop\Couriers\Repositories\CourierRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

/**
 * Class GlobalTemplateServiceProvider
 * @package App\Providers
 * @codeCoverageIgnore
 */
class GlobalTemplateServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer([
            'layouts.admin.app',
            'layouts.admin.sidebar',
            'admin.shared.products',
            'admin.products.list'
        ], function ($view) {
            $view->with('admin', Auth::guard('employee')->user());
        });

        view()->composer(['frontend.includes.featured-categories', 'front.categories.sidebar-category','frontend.includes.main', 'cartCount'], function ($view) {
            $view->with('categories', $this->getCategories());
            $view->with('cartCount', $this->getCartCount());
        });

        view()->composer(['frontend.includes.header','frontend.includes.mobile-view', 'front.categories.sidebar-category',
            'frontend.bank-transfer-redirect'], function ($view) {
            $view->with('categories', $this->getCategories());
            $view->with('cartCount', $this->getCartCount());
            $view->with('wishlistCount', $this->getWishlistCount());
            $view->with('wishlistItems', $this->getWishlistItems());
            $view->with('cartItems', $this->getCartItems());
            $view->with('subtotal', $this->getSubTotal());
            $view->with('tax', $this->getTax());
            $view->with('shippingFee', $this->getShippingFee());
            $view->with('total', $this->TotalFee());
        });

        /**
         * breadcrumb
         */
        view()->composer([
            "layouts.admin.app"
        ], function ($view) {
            $breadcrumb = [
                ["name" => "Dashboard", "url" => route("admin.dashboard"), "icon" => "fa fa-dashboard"],
            ];
            $paths = request()->segments();
            if (count($paths) > 1) {
                foreach ($paths as $key => $pah) {
                    if ($key == 1)
                        $breadcrumb[] = ["name" => ucfirst($pah), "url" => request()->getBaseUrl() . "/" . $paths[0] . "/" . $paths[$key], 'icon' => config("module.admin." . $pah . ".icon")];
                    elseif ($key == 2)
                        $breadcrumb[] = ["name" => ucfirst($pah), "url" => request()->getBaseUrl() . "/" . $paths[0] . "/" . $paths[1] . "/" . $paths[$key], 'icon' => config("module.admin." . $pah . ".icon")];
                }
            }
            $view->with(
                [
                    "breadcrumbs" => $breadcrumb
                ]
            );
        });


        view()->composer(['layouts.front.category-nav'], function ($view) {
            $view->with('categories', $this->getCategories());
        });
    }

    /**
     * Get all the categories
     *
     */
    private function getCategories()
    {
        $categoryRepo = new CategoryRepository(new Category);
        return $categoryRepo->listCategories('name', 'asc', 1)->whereIn('parent_id', [1]);
    }

    /**
     * @return int
     */
    private function getCartCount()
    {
        $cartRepo = new CartRepository(new ShoppingCart);
        return $cartRepo->countItems();
    }

    private function getWishlistCount()
    {
        $cartRepo = new CartRepository(new ShoppingCart);
        return $cartRepo->countWishlistItems();
    }

    private function getWishlistItems()
    {
        $cartRepo = new CartRepository(new ShoppingCart);
        return $cartRepo->getWishlistItemsTransformed();
    }

    private function getCartItems()
    {
        $cartRepo = new CartRepository(new ShoppingCart);
        return $cartRepo->getCartItemsTransformed();
    }

    private function getSubTotal()
    {
        $cartRepo = new CartRepository(new ShoppingCart);
        return $cartRepo->getSubTotal();
    }

    private function getTax()
    {
        $cartRepo = new CartRepository(new ShoppingCart);
        return $cartRepo->getTax();
    }

    private function getShippingFee()
    {
        $courierRepo = new CourierRepository(new Courier);
        $courier = $courierRepo->findCourierById(request()->session()->get('courierId', 1));
        $cartRepo = new CartRepository(new ShoppingCart);
        return $cartRepo->getShippingFee($courier);
    }

    private function TotalFee()
    {
        $courierRepo = new CourierRepository(new Courier);
        $courier = $courierRepo->findCourierById(request()->session()->get('courierId', 1));
        $cartRepo = new CartRepository(new ShoppingCart);
        $shippingFee = $cartRepo->getShippingFee($courier);

        return $cartRepo->getTotal(2, $shippingFee);
    }

    /**
     * @param Employee $employee
     * @return bool
     */
    private function isAdmin(Employee $employee)
    {
        $employeeRepo = new EmployeeRepository($employee);
        return $employeeRepo->hasRole('admin');
    }


}
