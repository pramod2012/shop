<?php

namespace App\Http\Controllers\Front;

use App\Shop\Categories\Repositories\CategoryRepository;
use App\Shop\Categories\Repositories\Interfaces\CategoryRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Shop\Products\Product;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepo;

    /**
     * CategoryController constructor.
     *
     * @param CategoryRepositoryInterface $categoryRepository
     */
    public function __construct(
        CategoryRepositoryInterface $categoryRepository
    )
    {
        $this->categoryRepo = $categoryRepository;
    }

    /**
     * Find the category via the slug
     *
     * @param string $slug
     * @return \App\Shop\Categories\Category
     */
    public function getCategory(Request $request, string $slug)
    {
        $category = $this->categoryRepo->findCategoryBySlug(['slug' => $slug]);

        $repo = new CategoryRepository($category);

        $product = $repo->findProducts()->where('status', 1)->all();

        $product = new Product();
        $queries = [];
        if (request()->has('price')){
         $product = $product->orderBy('price', request('price'));
         $queries['price'] = request('price');
        }
        if (request()->has('views')){
         $product = $product->orderBy('views', request('views'));
         $queries['views'] = request('views');
        }
        $min_price = $request->input('min_price');
        $max_price = $request->input('max_price');

        if(isset($min_price) && isset($max_price)){
            $product = $product->where('price' , '>=', $min_price)
                ->where('price', '<=', $max_price);
            $queries['min_price'] = request('min_price');
            $queries['max_price'] = request('max_price');
            }
        $queries['sort'] = request('sort');
        $id = $category->id;
        $products = $product->whereHas('categories',function($query) use ($id){
                    $query->whereCategoryId($id);
        })->where('status',1)->orderBy('created_at', 'desc')
                            ->paginate(12)
                            ->appends($queries);

        return view('frontend.category', [
            'category' => $category,
            'products' => $products        
        ]);
    }

    public function getListCategory(string $slug)
    {
        $category = $this->categoryRepo->findCategoryBySlug(['slug' => $slug]);

        $repo = new CategoryRepository($category);

        $products = $repo->findProducts()->where('status', 1)->all();

        return view('frontend.category-list', [
            'category' => $category,
            'products' => $repo->paginateArrayResults($products, 20)
        ]);
    }

    public function search(Request $request)
    {
        $product = new Product();
        $queries = [];
        if (request()->has('price')){
         $product = $product->orderBy('price', request('price'));
         $queries['price'] = request('price');
        }
        $title = $request->input('name');
        if(isset($title)){
                $product = $product->where('name', 'like', "%$title%");
                $queries['name'] = $title;
            }
        if (request()->has('views')){
         $product = $product->orderBy('views', request('views'));
         $queries['views'] = request('views');
        }
        $min_price = $request->input('min_price');
        $max_price = $request->input('max_price');

        if(isset($min_price) && isset($max_price)){
            $product = $product->where('price' , '>=', $min_price)
                ->where('price', '<=', $max_price);
            $queries['min_price'] = request('min_price');
            $queries['max_price'] = request('max_price');
            }
        $category = $request->input('category_id');
        if(isset($category))
            {
              $product =  $product->whereHas('categories',function($q) use ($category)
                {
                    $q->whereCategoryId($category);
                });
              $queries['category_id'] = request('category_id');
            }
        $queries['sort'] = request('sort');

        $products = $product->where('status',1)->orderBy('created_at', 'desc')
                            ->paginate(12)
                            ->appends($queries);

        return view('frontend.search', [
            'products' => $products        
        ]);
    }
}
