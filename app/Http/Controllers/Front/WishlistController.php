<?php

namespace App\Http\Controllers\Front;

use App\Shop\Carts\Repositories\Interfaces\CartRepositoryInterface;
use App\Shop\Products\Repositories\Interfaces\ProductRepositoryInterface;
use App\Shop\ProductAttributes\Repositories\ProductAttributeRepositoryInterface;
use App\Shop\Products\Transformations\ProductTransformable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cart;

class WishlistController extends Controller
{
    use ProductTransformable;

    private $cartRepo;

    private $productRepo;

    private $productAttributeRepo;

    public function __construct(
        CartRepositoryInterface $cartRepository,
        ProductRepositoryInterface $productRepository,
        ProductAttributeRepositoryInterface $productAttributeRepository
    ) {
        $this->cartRepo = $cartRepository;
        $this->productRepo = $productRepository;
        $this->productAttributeRepo = $productAttributeRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('frontend.wishlist',[
            'items' => $this->cartRepo->getWishlistItemsTransformed()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  AddToCartRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    	$product = $this->productRepo->findProductById($request->input('product'));

        if ($product->attributes()->count() > 0) {
            $productAttr = $product->attributes()->where('default', 1)->first();

            if (isset($productAttr->sale_price)) {
                $product->price = $productAttr->price;

                if (!is_null($productAttr->sale_price)) {
                    $product->price = $productAttr->sale_price;
                }
            }
        }

        $options = [];
        if ($request->has('productAttribute')) {

            $attr = $this->productAttributeRepo->findProductAttributeById($request->input('productAttribute'));

            $product->price = $attr->sale_price ?? $attr->price;

            $options['product_attribute_id'] = $request->input('productAttribute');
            $options['combination'] = $attr->attributesValues->toArray();
        }

        $this->cartRepo->addToWishlist($product, 1, $options);

        return redirect()->route('wishlist.index')
            ->with('message', 'Add to wishlist successful');
    }

    public function destroy($id)
    {
        Cart::instance('wishlist')->remove($id);

        request()->session()->flash('message', 'Removed to wishlist successful');
        return array('status'=>'1');
    }
}
