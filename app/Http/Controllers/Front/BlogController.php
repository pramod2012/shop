<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Shop\Posts\Post;
use App\Shop\Mcategories\Mcategory;
use App\Shop\Tags\Tag;
use App\Shop\Authors\Author;

class BlogController extends Controller
{

    public function blog()
    {
    	$posts = Post::where('is_blog',1)->orderBy('order','desc')->paginate(1);
    	$post_related = Post::where('is_blog',1)->orderBy('created_at','desc')->paginate(4);
    	$mcategories = Mcategory::all();
    	$tags = Tag::all();
        return view('frontend.blog',compact('posts','post_related','mcategories','tags'));
    }

    public function blogShow($slug){
        $post = Post::where('is_blog',1)->where('slug',$slug)->firstOrFail();
        $post_related = Post::where('is_blog',1)->orderBy('created_at','desc')->paginate(4);
        $mcategories = Mcategory::all();
        $tags = Tag::all();
        return view('frontend.post',compact('post','mcategories','tags','post_related'));
    }

    public function pageShow($slug){
        $page = Post::where('is_blog',0)->where('slug',$slug)->firstOrFail();
        return view('frontend.page',compact('page'));
    }

    public function findByTag($slug){
        $tag = Tag::where('slug',$slug)->firstOrFail();
        $id  = $tag->id;
        $posts = Post::where('is_blog',1)->where('status',1)->orderBy('created_at', 'desc')
      ->whereHas('tags',function($query) use ($id){
                    $query->whereTagId($id);
        })->paginate(12);
      $post_related = Post::where('is_blog',1)->orderBy('created_at','desc')->paginate(4);
        
        return view('frontend.findByTag',compact('tag','posts','post_related'));

    }

    public function findByCategory($slug){
        $mcategory = Mcategory::where('slug',$slug)->firstOrFail();
        $id  = $mcategory->id;
        $posts = Post::where('is_blog',1)->where('mcategory_id',$id)->paginate(12);
        $post_related = Post::where('is_blog',1)->orderBy('created_at','desc')->paginate(4);
        return view('frontend.findByCategory',compact('mcategory','posts','post_related'));

    }

    public function findByAuthor($slug){
        $author = Author::where('slug',$slug)->firstOrFail();
        $id  = $author->id;
        $posts = Post::where('is_blog',1)->where('author_id',$id)->paginate(12);
        $post_related = Post::where('is_blog',1)->orderBy('created_at','desc')->paginate(4);
        return view('frontend.findByAuthor',compact('author','posts','post_related'));

    }
}
