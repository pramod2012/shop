<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop\Ratings\Rating;

class HomeController extends Controller
{
    public function ratingStore(Request $request){
        $data = new Rating();
        $data->name = $request->name;
        $data->email = $request->email;
        $data->mobile = $request->mobile;
        $data->subject = $request->subject;
        $data->body = $request->body;
        $data->rating = $request->rating;
        $data->product_id = $request->product_id;
        $data->save();
        return response()->json(['status' => 'success']);
    }
    
    public function cartCount(){

        return view('cartCount');
    }
}
