<?php

namespace App\Http\Controllers\Admin\Tags;

use App\Shop\Tags\Tag;
use App\Shop\Tags\Repositories\TagRepository;
use App\Shop\Tags\Repositories\Interfaces\TagRepositoryInterface;
use App\Shop\Tags\Requests\CreateTagRequest;
use App\Shop\Tags\Requests\UpdateTagRequest;
use App\Shop\Tags\Transformations\TagTransformable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TagController extends Controller
{
    use TagTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $tagRepo;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(TagRepositoryInterface $tagRepository)
    {
        $this->tagRepo = $tagRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->tagRepo->listTags('order', 'asc');

        if (request()->has('q')) {
            $list = $this->tagRepo->searchTag(request()->input('q'));
        }

        $tags = $list->map(function (Tag $tag) {
            return $this->transformTag($tag);
        })->all();


        return view('admin.tags.list', [
            'tags' => $this->tagRepo->paginateArrayResults($tags)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTagRequest $request)
    {
        $this->tagRepo->createTag($request->except('_token', '_method'));

        return redirect()->route('admin.tags.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $tag = $this->tagRepo->findTagById($id);
        
        return view('admin.tags.show', [
            'tag' => $tag,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.tags.edit', ['tag' => $this->tagRepo->findTagById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCustomerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTagRequest $request, $id)
    {
        $data = $this->tagRepo->findTagById($id);

        $update = new TagRepository($data);
        $update->updateTag($request->except('_method', '_token'));

        $request->session()->flash('message', 'Update successful');
        return redirect()->route('admin.tags.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $data = $this->tagRepo->findTagById($id);

        $tagRepo = new TagRepository($data);
        $tagRepo->deleteTag();

        return redirect()->route('admin.tags.index')->with('message', 'Delete successful');
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('tags');
        foreach($allArticles as $_article){
            $article = Tag::find($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
