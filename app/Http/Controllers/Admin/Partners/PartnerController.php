<?php

namespace App\Http\Controllers\Admin\Partners;

use App\Shop\Partners\Partner;
use App\Shop\Partners\Repositories\PartnerRepository;
use App\Shop\Partners\Repositories\Interfaces\PartnerRepositoryInterface;
use App\Shop\Partners\Requests\CreatePartnerRequest;
use App\Shop\Partners\Requests\UpdatePartnerRequest;
use App\Shop\Partners\Transformations\PartnerTransformable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    use PartnerTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $partnerRepo;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(PartnerRepositoryInterface $partnerRepository)
    {
        $this->partnerRepo = $partnerRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->partnerRepo->listPartners('order', 'asc');

        if (request()->has('q')) {
            $list = $this->partnerRepo->searchPartner(request()->input('q'));
        }

        $partners = $list->map(function (Partner $partner) {
            return $this->transformPartner($partner);
        })->all();


        return view('admin.partners.list', [
            'partners' => $this->partnerRepo->paginateArrayResults($partners)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePartnerRequest $request)
    {
        $this->partnerRepo->createPartner($request->except('_token', '_method'));

        return redirect()->route('admin.partners.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $partner = $this->partnerRepo->findPartnerById($id);
        
        return view('admin.partners.show', [
            'partner' => $partner,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.partners.edit', ['partner' => $this->partnerRepo->findPartnerById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCustomerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePartnerRequest $request, $id)
    {
        $data = $this->partnerRepo->findPartnerById($id);

        $update = new PartnerRepository($data);
        $update->updatePartner($request->except('_method', '_token'));

        $request->session()->flash('message', 'Update successful');
        return redirect()->route('admin.partners.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $data = $this->partnerRepo->findPartnerById($id);

        $partnerRepo = new PartnerRepository($data);
        $partnerRepo->deletePartner();

        return redirect()->route('admin.partners.index')->with('message', 'Delete successful');
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('partners');
        foreach($allArticles as $_article){
            $article = Partner::find($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
