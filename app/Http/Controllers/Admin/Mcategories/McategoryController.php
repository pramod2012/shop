<?php

namespace App\Http\Controllers\Admin\Mcategories;

use App\Shop\Mcategories\Mcategory;
use App\Shop\Mcategories\Repositories\McategoryRepository;
use App\Shop\Mcategories\Repositories\Interfaces\McategoryRepositoryInterface;
use App\Shop\Mcategories\Requests\CreateMcategoryRequest;
use App\Shop\Mcategories\Requests\UpdateMcategoryRequest;
use App\Shop\Mcategories\Transformations\McategoryTransformable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class McategoryController extends Controller
{
    use McategoryTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $mcategoryRepo;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(McategoryRepositoryInterface $mcategoryRepository)
    {
        $this->mcategoryRepo = $mcategoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->mcategoryRepo->listMcategories('order', 'asc');

        if (request()->has('q')) {
            $list = $this->mcategoryRepo->searchMcategory(request()->input('q'));
        }

        $mcategories = $list->map(function (Mcategory $mcategory) {
            return $this->transformMcategory($mcategory);
        })->all();


        return view('admin.mcategories.list', [
            'mcategories' => $this->mcategoryRepo->paginateArrayResults($mcategories)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.mcategories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateMcategoryRequest $request)
    {
        $this->mcategoryRepo->createMcategory($request->except('_token', '_method'));

        return redirect()->route('admin.mcategories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $mcategory = $this->mcategoryRepo->findMcategoryById($id);
        
        return view('admin.mcategories.show', [
            'mcategory' => $mcategory,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.mcategories.edit', ['mcategory' => $this->mcategoryRepo->findMcategoryById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCustomerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMcategoryRequest $request, $id)
    {
        $data = $this->mcategoryRepo->findMcategoryById($id);

        $update = new McategoryRepository($data);
        $update->updateMcategory($request->except('_method', '_token'));

        $request->session()->flash('message', 'Update successful');
        return redirect()->route('admin.mcategories.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $data = $this->mcategoryRepo->findMcategoryById($id);

        $mcategoryRepo = new McategoryRepository($data);
        $mcategoryRepo->deleteMcategory();

        return redirect()->route('admin.mcategories.index')->with('message', 'Delete successful');
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('mcategories');
        foreach($allArticles as $_article){
            $article = Mcategory::find($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
