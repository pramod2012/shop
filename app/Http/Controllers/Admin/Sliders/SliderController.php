<?php

namespace App\Http\Controllers\Admin\Sliders;

use App\Shop\Sliders\Slider;
use App\Shop\Sliders\Repositories\SliderRepository;
use App\Shop\Sliders\Repositories\Interfaces\SliderRepositoryInterface;
use App\Shop\Sliders\Requests\CreateSliderRequest;
use App\Shop\Sliders\Requests\UpdateSliderRequest;
use App\Shop\Sliders\Transformations\SliderTransformable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    use SliderTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $sliderRepo;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(SliderRepositoryInterface $sliderRepository)
    {
        $this->sliderRepo = $sliderRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->sliderRepo->listSliders('order', 'asc');

        if (request()->has('q')) {
            $list = $this->sliderRepo->searchSlider(request()->input('q'));
        }

        $sliders = $list->map(function (Slider $slider) {
            return $this->transformSlider($slider);
        })->all();


        return view('admin.sliders.list', [
            'sliders' => $this->sliderRepo->paginateArrayResults($sliders)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.sliders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSliderRequest $request)
    {
        $this->sliderRepo->createSlider($request->except('_token', '_method'));

        return redirect()->route('admin.sliders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $slider = $this->sliderRepo->findSliderById($id);
        
        return view('admin.sliders.show', [
            'slider' => $slider,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.sliders.edit', ['slider' => $this->sliderRepo->findSliderById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCustomerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSliderRequest $request, $id)
    {
        $data = $this->sliderRepo->findSliderById($id);

        $update = new SliderRepository($data);
        $update->updateSlider($request->except('_method', '_token'));

        $request->session()->flash('message', 'Update successful');
        return redirect()->route('admin.sliders.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $data = $this->sliderRepo->findSliderById($id);

        $sliderRepo = new SliderRepository($data);
        $sliderRepo->deleteSlider();

        return redirect()->route('admin.sliders.index')->with('message', 'Delete successful');
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('sliders');
        foreach($allArticles as $_article){
            $article = Slider::find($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
