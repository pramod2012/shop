<?php

namespace App\Http\Controllers\Admin\Flashes;

use App\Shop\Flashes\Flash;
use App\Shop\Flashes\Repositories\FlashRepository;
use App\Shop\Flashes\Repositories\Interfaces\FlashRepositoryInterface;
use App\Shop\Flashes\Requests\CreateFlashRequest;
use App\Shop\Flashes\Requests\UpdateFlashRequest;
use App\Shop\Flashes\Transformations\FlashTransformable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FlashController extends Controller
{
    use FlashTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $flashRepo;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(FlashRepositoryInterface $flashRepository)
    {
        $this->flashRepo = $flashRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->flashRepo->listFlashes('order', 'asc');

        if (request()->has('q')) {
            $list = $this->flashRepo->searchFlash(request()->input('q'));
        }

        $flashes = $list->map(function (Flash $flash) {
            return $this->transformFlash($flash);
        })->all();


        return view('admin.flashes.list', [
            'flashes' => $this->flashRepo->paginateArrayResults($flashes)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.flashes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateFlashRequest $request)
    {
        $this->flashRepo->createFlash($request->except('_token', '_method'));

        return redirect()->route('admin.flashes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $flash = $this->flashRepo->findFlashById($id);
        
        return view('admin.flashes.show', [
            'flash' => $flash,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.flashes.edit', ['flash' => $this->flashRepo->findFlashById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCustomerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFlashRequest $request, $id)
    {
        $data = $this->flashRepo->findFlashById($id);

        $update = new FlashRepository($data);
        $update->updateFlash($request->except('_method', '_token'));

        $request->session()->flash('message', 'Update successful');
        return redirect()->route('admin.flashes.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $data = $this->flashRepo->findFlashById($id);

        $flashRepo = new FlashRepository($data);
        $flashRepo->deleteFlash();

        return redirect()->route('admin.flashes.index')->with('message', 'Delete successful');
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('flashes');
        foreach($allArticles as $_article){
            $article = Flash::find($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
