<?php

namespace App\Http\Controllers\Admin\Emails;

use App\Shop\Emails\Email;
use App\Shop\Emails\Repositories\EmailRepository;
use App\Shop\Emails\Repositories\Interfaces\EmailRepositoryInterface;
use App\Shop\Emails\Requests\CreateEmailRequest;
use App\Shop\Emails\Requests\UpdateEmailRequest;
use App\Shop\Emails\Transformations\EmailTransformable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class EmailController extends Controller
{
    use EmailTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $emailRepo;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(EmailRepositoryInterface $emailRepository)
    {
        $this->emailRepo = $emailRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->emailRepo->listEmails('order', 'asc');

        if (request()->has('q')) {
            $list = $this->emailRepo->searchEmail(request()->input('q'));
        }

        $emails = $list->map(function (Email $email) {
            return $this->transformEmail($email);
        })->all();


        return view('admin.emails.list', [
            'emails' => $this->emailRepo->paginateArrayResults($emails)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.emails.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEmailRequest $request)
    {
        $this->emailRepo->createEmail($request->except('_token', '_method'));

        return redirect()->route('admin.emails.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $email = $this->emailRepo->findEmailById($id);
        
        return view('admin.emails.show', [
            'email' => $email,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.emails.edit', ['email' => $this->emailRepo->findEmailById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCustomerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEmailRequest $request, $id)
    {
        $data = $this->emailRepo->findEmailById($id);

        $update = new EmailRepository($data);
        $update->updateEmail($request->except('_method', '_token'));

        $request->session()->flash('message', 'Update successful');
        return redirect()->route('admin.emails.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $data = $this->emailRepo->findEmailById($id);

        $emailRepo = new EmailRepository($data);
        $emailRepo->deleteEmail();

        return redirect()->route('admin.emails.index')->with('message', 'Delete successful');
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('emails');
        foreach($allArticles as $_article){
            $article = Email::find($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
