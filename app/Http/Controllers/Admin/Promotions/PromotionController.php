<?php

namespace App\Http\Controllers\Admin\Promotions;

use App\Shop\Promotions\Promotion;
use App\Shop\Promotions\Repositories\PromotionRepository;
use App\Shop\Promotions\Repositories\Interfaces\PromotionRepositoryInterface;
use App\Shop\Promotions\Requests\CreatePromotionRequest;
use App\Shop\Promotions\Requests\UpdatePromotionRequest;
use App\Shop\Promotions\Transformations\PromotionTransformable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    use PromotionTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $promotionRepo;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(PromotionRepositoryInterface $promotionRepository)
    {
        $this->promotionRepo = $promotionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->promotionRepo->listPromotions('order', 'asc');

        if (request()->has('q')) {
            $list = $this->promotionRepo->searchPromotion(request()->input('q'));
        }

        $promotions = $list->map(function (Promotion $promotion) {
            return $this->transformPromotion($promotion);
        })->all();


        return view('admin.promotions.list', [
            'promotions' => $this->promotionRepo->paginateArrayResults($promotions)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.promotions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePromotionRequest $request)
    {
        $this->promotionRepo->createPromotion($request->except('_token', '_method'));

        return redirect()->route('admin.promotions.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $promotion = $this->promotionRepo->findPromotionById($id);
        
        return view('admin.promotions.show', [
            'promotion' => $promotion,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.promotions.edit', ['promotion' => $this->promotionRepo->findPromotionById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCustomerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePromotionRequest $request, $id)
    {
        $data = $this->promotionRepo->findPromotionById($id);

        $update = new PromotionRepository($data);
        $update->updatePromotion($request->except('_method', '_token'));

        $request->session()->flash('message', 'Update successful');
        return redirect()->route('admin.promotions.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $data = $this->promotionRepo->findPromotionById($id);

        $promotionRepo = new PromotionRepository($data);
        $promotionRepo->deletePromotion();

        return redirect()->route('admin.promotions.index')->with('message', 'Delete successful');
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('promotions');
        foreach($allArticles as $_article){
            $article = Promotion::find($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
