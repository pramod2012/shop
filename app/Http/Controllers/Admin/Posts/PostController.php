<?php

namespace App\Http\Controllers\Admin\Posts;

use App\Shop\Posts\Post;
use App\Shop\Posts\Repositories\PostRepository;
use App\Shop\Posts\Repositories\Interfaces\PostRepositoryInterface;
use App\Shop\Posts\Requests\CreatePostRequest;
use App\Shop\Posts\Requests\UpdatePostRequest;
use App\Shop\Posts\Transformations\PostTransformable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PostController extends Controller
{
    use PostTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $postRepo;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(PostRepositoryInterface $postRepository)
    {
        $this->postRepo = $postRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->postRepo->listPosts('order', 'asc');

        if (request()->has('q')) {
            $list = $this->postRepo->searchPost(request()->input('q'));
        }

        $posts = $list->map(function (Post $post) {
            return $this->transformPost($post);
        })->all();


        return view('admin.posts.list', [
            'posts' => $this->postRepo->paginateArrayResults($posts)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreatePostRequest $request)
    {
        $this->postRepo->createPost($request->except('_token', '_method'));

        return redirect()->route('admin.posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $post = $this->postRepo->findPostById($id);
        
        return view('admin.posts.show', [
            'post' => $post,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.posts.edit', ['post' => $this->postRepo->findPostById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCustomerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, $id)
    {
        $data = $this->postRepo->findPostById($id);

        $update = new PostRepository($data);
        $update->updatePost($request->except('_method', '_token'));

        $request->session()->flash('message', 'Update successful');
        return redirect()->route('admin.posts.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $data = $this->postRepo->findPostById($id);

        $postRepo = new PostRepository($data);
        $postRepo->deletePost();

        return redirect()->route('admin.posts.index')->with('message', 'Delete successful');
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('posts');
        foreach($allArticles as $_article){
            $article = Post::find($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
