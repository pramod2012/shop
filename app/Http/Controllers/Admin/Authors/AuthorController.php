<?php

namespace App\Http\Controllers\Admin\Authors;

use App\Shop\Authors\Author;
use App\Shop\Authors\Repositories\AuthorRepository;
use App\Shop\Authors\Repositories\Interfaces\AuthorRepositoryInterface;
use App\Shop\Authors\Requests\CreateAuthorRequest;
use App\Shop\Authors\Requests\UpdateAuthorRequest;
use App\Shop\Authors\Transformations\AuthorTransformable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    use AuthorTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $authorRepo;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(AuthorRepositoryInterface $authorRepository)
    {
        $this->authorRepo = $authorRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->authorRepo->listAuthors('order', 'asc');

        if (request()->has('q')) {
            $list = $this->authorRepo->searchAuthor(request()->input('q'));
        }

        $authors = $list->map(function (Author $author) {
            return $this->transformAuthor($author);
        })->all();


        return view('admin.authors.list', [
            'authors' => $this->authorRepo->paginateArrayResults($authors)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.authors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAuthorRequest $request)
    {
        $this->authorRepo->createAuthor($request->except('_token', '_method'));

        return redirect()->route('admin.authors.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $author = $this->authorRepo->findAuthorById($id);
        
        return view('admin.authors.show', [
            'author' => $author,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.authors.edit', ['author' => $this->authorRepo->findAuthorById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCustomerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAuthorRequest $request, $id)
    {
        $data = $this->authorRepo->findAuthorById($id);

        $update = new AuthorRepository($data);
        $update->updateAuthor($request->except('_method', '_token'));

        $request->session()->flash('message', 'Update successful');
        return redirect()->route('admin.authors.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $data = $this->authorRepo->findAuthorById($id);

        $authorRepo = new AuthorRepository($data);
        $authorRepo->deleteAuthor();

        return redirect()->route('admin.authors.index')->with('message', 'Delete successful');
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('authors');
        foreach($allArticles as $_article){
            $article = Author::find($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
