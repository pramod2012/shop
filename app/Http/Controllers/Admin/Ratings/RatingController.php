<?php

namespace App\Http\Controllers\Admin\Ratings;

use App\Shop\Ratings\Rating;
use App\Shop\Ratings\Repositories\RatingRepository;
use App\Shop\Ratings\Repositories\Interfaces\RatingRepositoryInterface;
use App\Shop\Ratings\Requests\CreateRatingRequest;
use App\Shop\Ratings\Requests\UpdateRatingRequest;
use App\Shop\Ratings\Transformations\RatingTransformable;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    use RatingTransformable;

    /**
     * @var CustomerRepositoryInterface
     */
    private $ratingRepo;

    /**
     * CustomerController constructor.
     * @param CustomerRepositoryInterface $customerRepository
     */
    public function __construct(RatingRepositoryInterface $ratingRepository)
    {
        $this->ratingRepo = $ratingRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = $this->ratingRepo->listRatings('order', 'asc');

        if (request()->has('q')) {
            $list = $this->ratingRepo->searchRating(request()->input('q'));
        }

        $ratings = $list->map(function (Rating $rating) {
            return $this->transformRating($rating);
        })->all();


        return view('admin.ratings.list', [
            'ratings' => $this->ratingRepo->paginateArrayResults($ratings)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ratings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateCustomerRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRatingRequest $request)
    {
        $this->ratingRepo->createRating($request->except('_token', '_method'));

        return redirect()->route('admin.ratings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $rating = $this->ratingRepo->findRatingById($id);
        
        return view('admin.ratings.show', [
            'rating' => $rating,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.ratings.edit', ['rating' => $this->ratingRepo->findRatingById($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateCustomerRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRatingRequest $request, $id)
    {
        $data = $this->ratingRepo->findRatingById($id);

        $update = new RatingRepository($data);
        $update->updateRating($request->except('_method', '_token'));

        $request->session()->flash('message', 'Update successful');
        return redirect()->route('admin.ratings.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $data = $this->ratingRepo->findRatingById($id);

        $ratingRepo = new RatingRepository($data);
        $ratingRepo->deleteRating();

        return redirect()->route('admin.ratings.index')->with('message', 'Delete successful');
    }

    public function saveOrders(Request $request){
        $allArticles = $request->get('ratings');
        foreach($allArticles as $_article){
            $article = Rating::find($_article['id']);
            if( $article ){
                $article->order = $_article['order'];
                $article->save();
            }
        }
        return ['success' => true];
    }
}
