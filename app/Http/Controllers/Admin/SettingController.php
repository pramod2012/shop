<?php

namespace App\Http\Controllers\Admin;

use App\Models\Setting;
use App\Traits\UploadAble;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use App\Http\Controllers\BaseController;

/**
 * Class SettingController
 * @package App\Http\Controllers\Admin
 */
class SettingController extends BaseController
{
    use UploadAble;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $this->setPageTitle('Settings', 'Manage Settings');
        return view('admin.settings.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        if ($request->has('logo') && ($request->file('logo') instanceof UploadedFile)) {

            if (config('settings.logo') != null) {
                $this->deleteOne(config('settings.logo'));
            }
            $logo = $this->uploadOne($request->file('logo'), 'img');
            Setting::set('logo', $logo);

        } elseif ($request->has('favicon') && ($request->file('favicon') instanceof UploadedFile)) {

            if (config('settings.favicon') != null) {
                $this->deleteOne(config('settings.favicon'));
            }
            $favicon = $this->uploadOne($request->file('favicon'), 'img');
            Setting::set('favicon', $favicon);

        } elseif ($request->has('company_logo') && ($request->file('company_logo') instanceof UploadedFile)) {

            if (config('settings.company_logo') != null) {
                $this->deleteOne(config('settings.company_logo'));
            }
            $company_logo = $this->uploadOne($request->file('company_logo'), 'img');
            Setting::set('company_logo', $company_logo);

        } elseif ($request->has('error_image') && ($request->file('error_image') instanceof UploadedFile)) {

            if (config('settings.error_image') != null) {
                $this->deleteOne(config('settings.error_image'));
            }
            $error_image = $this->uploadOne($request->file('error_image'), 'img');
            Setting::set('error_image', $error_image);
        
        } elseif ($request->has('contactus_image') && ($request->file('contactus_image') instanceof UploadedFile)) {

            if (config('settings.contactus_image') != null) {
                $this->deleteOne(config('settings.contactus_image'));
            }
            $contactus_image = $this->uploadOne($request->file('contactus_image'), 'img');
            Setting::set('contactus_image', $contactus_image);
        
        } elseif ($request->has('quote_image') && ($request->file('quote_image') instanceof UploadedFile)) {

            if (config('settings.quote_image') != null) {
                $this->deleteOne(config('settings.quote_image'));
            }
            $quote_image = $this->uploadOne($request->file('quote_image'), 'img');
            Setting::set('quote_image', $quote_image);
        
        } elseif ($request->has('work_image') && ($request->file('work_image') instanceof UploadedFile)) {

            if (config('settings.work_image') != null) {
                $this->deleteOne(config('settings.work_image'));
            }
            $work_image = $this->uploadOne($request->file('work_image'), 'img');
            Setting::set('work_image', $work_image);
        
        } elseif ($request->has('footer_image') && ($request->file('footer_image') instanceof UploadedFile)) {

            if (config('settings.footer_image') != null) {
                $this->deleteOne(config('settings.footer_image'));
            }
            $footer_image = $this->uploadOne($request->file('footer_image'), 'img');
            Setting::set('footer_image', $footer_image);
        
        } elseif ($request->has('group_photo') && ($request->file('group_photo') instanceof UploadedFile)) {

            if (config('settings.group_photo') != null) {
                $this->deleteOne(config('settings.group_photo'));
            }
            $group_photo = $this->uploadOne($request->file('group_photo'), 'img');
            Setting::set('group_photo', $group_photo);
        
        } elseif ($request->has('enroll_image') && ($request->file('enroll_image') instanceof UploadedFile)) {

            if (config('settings.enroll_image') != null) {
                $this->deleteOne(config('settings.enroll_image'));
            }
            $enroll_image = $this->uploadOne($request->file('enroll_image'), 'img');
            Setting::set('enroll_image', $enroll_image);
        
        }

        else {

            $keys = $request->except('_token');

            foreach ($keys as $key => $value)
            {
                Setting::set($key, $value);
            }
        }
        return $this->responseRedirectBack('Settings updated successfully.', 'success');
    }
}
