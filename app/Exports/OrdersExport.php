<?php

namespace App\Exports;

use App\Shop\Orders\Order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithColumnWidths;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class OrdersExport implements FromView,WithColumnWidths,WithStyles
{
    public function view(): View
    {
        return view('invoices.exportOrder', [
            'orders' => Order::orderBy('created_at','desc')->limit(20)->get()
        ]);
    }

    public function columnWidths(): array
    {
        return [
            'A' => 20,
            'B' => 20,
            'C' => 55,
            'D' => 35,
            'E' => 20,
            'F' => 12,
            'G' => 13,           
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1    => ['font' => ['bold' => true]],

            // Styling a specific cell by coordinate.
            'B' => ['font' => ['italic' => true]],
        ];
    }
    
}
