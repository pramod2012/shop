<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Console\ModelMakeCommand; 

class LaraStructure extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:structure {model} {--m|migration}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command generate model and a base repository out of the box.';

    /**
     * Filesystem instance
     * 
     * @var string
     */
    protected $filesystem;

    /**
     * Default laracom folder
     * 
     * @var string
     */
    protected $folder;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $filesystem)
    {
        parent::__construct();
        $this->filesystem = $filesystem;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Get model name from developer
        $this->model = ucfirst($this->argument('model'));

        $singularLcModel = lcfirst($this->argument('model'));

        // Get plural name for the given model
        $pluralModel = str_plural($this->model);

        $pluralLcModel = lcfirst($pluralModel);

        $timeStamp = date('Y_m_d_His');

        // Check if the model already created
        if ( $this->filesystem->exists(app_path("Shop/{$pluralModel}/{$this->model}.php")) ){
            return $this->error("The given model already exists!");
        }

        // create all structured folders
        $this->createFolders('Shop');

        $this->createFile(
            app_path('Console/Stubs/DummyRepository.stub'),
            app_path("Shop/{$pluralModel}/Repositories/{$this->model}Repository.php")
        );

        $this->createFile(
            app_path('Console/Stubs/DummyRepositoryInterface.stub'),
            app_path("Shop/{$pluralModel}/Repositories/Interfaces/{$this->model}RepositoryInterface.php")
        );

        $this->createFile(
            app_path('Console/Stubs/CreateDummyErrorException.stub'),
            app_path("Shop/{$pluralModel}/Exceptions/Create{$this->model}ErrorException.php")
        );

        $this->createFile(
            app_path('Console/Stubs/UpdateDummyErrorException.stub'),
            app_path("Shop/{$pluralModel}/Exceptions/Update{$this->model}ErrorException.php")
        );

        $this->info('File structure for ' . $this->model . ' created.');
        
        // Create Model under default instalation folder
        $this->createFile(
            app_path('Console/Stubs/DummyModel.stub'),
            app_path("Shop/{$pluralModel}/{$this->model}.php")
        );

        $this->createFile(
            app_path('Console/Stubs/DummyController.stub'),
            app_path("Http/Controllers/Admin/{$pluralModel}/{$this->model}Controller.php")
        );

        $this->createFile(
            app_path('Console/Stubs/create.stub'),
            ("resources/views/admin/{$pluralLcModel}/create.blade.php")
        );

        $this->createFile(
            app_path('Console/Stubs/edit.stub'),
            ("resources/views/admin/{$pluralLcModel}/edit.blade.php")
        );

        $this->createFile(
            app_path('Console/Stubs/list.stub'),
            ("resources/views/admin/{$pluralLcModel}/list.blade.php")
        );

        $this->createFile(
            app_path('Console/Stubs/show.stub'),
            ("resources/views/admin/{$pluralLcModel}/show.blade.php")
        );

        $this->createFile(
            app_path('Console/Stubs/CreateDummyRequest.stub'),
            app_path("Shop/{$pluralModel}/Requests/Create{$this->model}Request.php")
        );

        $this->createFile(
            app_path('Console/Stubs/UpdateDummyRequest.stub'),
            app_path("Shop/{$pluralModel}/Requests/Update{$this->model}Request.php")
        );

        $this->createFile(
            app_path('Console/Stubs/DummyTransformable.stub'),
            app_path("Shop/{$pluralModel}/Transformations/{$this->model}Transformable.php")
        );

        $this->createFile(
            app_path('Console/Stubs/timeStamp_create_samples_table.stub'),
            ("database/migrations/{$timeStamp}_create_{$pluralLcModel}_table.php")
        );


    }

    /**
     * Create source from dummy model name
     * 
     * @param  string $dummy        
     * @param  string $destinationPath
     * @return void
     */
    protected function createFile($dummySource, $destinationPath)
    {
        $pluralModel = str_plural($this->model);
        $pluralLcModel = lcfirst($pluralModel);
        $timeStamp = date('Y_m_d_His');

        $singularLcModel = lcfirst($this->argument('model'));
        $dummyRepository = $this->filesystem->get($dummySource);
        $repositoryContent = str_replace(['Dummy', 'Dummies', 'samply', 'samples', 'timeStamp'], [$this->model, $pluralModel, $singularLcModel, $pluralLcModel, $timeStamp], $dummyRepository);
        $this->filesystem->put($dummySource, $repositoryContent);
        $this->filesystem->copy($dummySource, $destinationPath);
        $this->filesystem->put($dummySource, $dummyRepository);
    }

    /**
     * Create all required folders
     * 
     * @return void
     */
    protected function createFolders($baseFolder)
    {
        // get plural from model name
        $pluralModel = str_plural($this->model);
        $pluralLcModel = lcfirst($pluralModel);
         // create container folder
        $this->filesystem->makeDirectory(app_path($baseFolder."/{$pluralModel}"));
         // add requests folder
        $this->filesystem->makeDirectory(app_path($baseFolder."/{$pluralModel}/Requests"));
         // add repositories folder
        $this->filesystem->makeDirectory(app_path($baseFolder."/{$pluralModel}/Repositories/"));
         // add Interfaces folder
        $this->filesystem->makeDirectory(app_path($baseFolder."/{$pluralModel}/Repositories/Interfaces"));
        $this->filesystem->makeDirectory(app_path("Http/Controllers/Admin/{$pluralModel}"));
        $this->filesystem->makeDirectory("resources/views/admin/{$pluralLcModel}");
        $this->filesystem->makeDirectory(app_path($baseFolder."/{$pluralModel}/Transformations"));
        $this->filesystem->makeDirectory(app_path($baseFolder."/{$pluralModel}/Exceptions"));
    }
}
