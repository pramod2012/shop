<?php

namespace App\Shop\Partners\Repositories;

use App\Shop\Partners\Partner;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepository;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Shop\Partners\Exceptions\CreatePartnerErrorException;
use App\Shop\Partners\Exceptions\UpdatePartnerErrorException;
use App\Shop\Partners\Exceptions\PartnerNotFoundException;
use App\Shop\Partners\Repositories\Interfaces\PartnerRepositoryInterface;
use App\Shop\Partners\Transformations\PartnerTransformable;
use App\Shop\Tools\UploadableTrait;

class PartnerRepository extends BaseRepository implements PartnerRepositoryInterface
{
    use UploadableTrait, PartnerTransformable;
	/**
     * PartnerRepository constructor.
     * 
     * @param Partner $dummy
     */
    public function __construct(Partner $partner)
    {
        parent::__construct($partner);
        $this->model = $partner;
    }

    /**
     * List all the Partners
     *
     * @param string $order
     * @param string $sort
     * @param array $except
     * @return \Illuminate\Support\Collection
     */
    public function listPartners(string $order = 'id', string $sort = 'desc', $except = []) : Collection
    {
        return $this->model->orderBy($order, $sort)->get()->except($except);
    }

    /**
     * Create Partner
     *
     * @param array $params
     *
     * @return Partner
     * @throws InvalidArgumentException
     */
    public function createPartner(array $params) : Partner
    {
        try {
        	return $this->createNewPartner($params);
        } catch (QueryException $e) {
            throw new CreatePartnerErrorException($e->getMessage());
        }
    }

    /**
     * Update the dummy
     *
     * @param array $params
     * @return Partner
     */
    public function updatePartner(array $params) : Partner
    {
        try {
            $id = $this->findOneOrFail($this->model->id);
            return $this->updateExistingPartner($id, $params);
        } catch (QueryException $e) {
            throw new UpdatePartnerErrorException($e->getMessage());
        }
        
    }

    /**
     * @param int $id
     * 
     * @return Partner
     * @throws ModelNotFoundException
     */
    public function findPartnerById(int $id) : Partner
    {
        try {
            return $this->transformPartner($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new PartnerNotFoundException($e->getMessage());
        }
    }

    /**
     * Delete a dummy
     *
     * @return bool
     */
    public function deletePartner() : bool
    {
        $data = $this->findOneOrFail($this->model->id);
        if ($data->image != null) {
            $this->deleteOne($data->image);
        }
        return $this->model->where('id', $this->model->id)->delete();
    }

    public function searchPartner(string $text = null) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchPartner($text);
        } else {
            return $this->listPartners();
        }
    }
}