<?php

namespace App\Shop\Partners\Repositories\Interfaces;

use App\Shop\Partners\Partner;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepositoryInterface;

interface PartnerRepositoryInterface extends BaseRepositoryInterface
{
    public function listPartners(string $order = 'id', string $sort = 'desc', $except = []) : Collection;

    public function createPartner(array $params) : Partner;

    public function updatePartner(array $params) : Partner;

    public function findPartnerById(int $id) : Partner;
    
    public function deletePartner() : bool;

    public function searchPartner(string $text = null) : Collection;
}
