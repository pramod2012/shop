<?php

namespace App\Shop\Partners\Transformations;

use App\Shop\Partners\Partner;
use Illuminate\Support\Facades\Storage;
use App\Shop\Tools\UploadableTrait;

trait PartnerTransformable
{
    use UploadableTrait;
    /**
     * Transform the partner
     *
     * @param partner $partner
     * @return partner
     */
    protected function transformPartner(Partner $partner)
    {
        $prod = new Partner;
        $prod->id = (int) $partner->id;
        $prod->name = $partner->name;
        $prod->body = $partner->body;
        $prod->image = asset("storage/$partner->image");
        $prod->status = $partner->status;
        $prod->feature = $partner->feature;
        $prod->slug = $partner->slug;
        $prod->order = $partner->order;
        $prod->created_at = $partner->created_at;
        $prod->updated_at = $partner->updated_at;
        $prod->meta_title = $partner->meta_title;
        $prod->meta_desc = $partner->meta_desc;
        $prod->meta_keywords = $partner->meta_keywords;

        return $prod;
    }

    protected function createNewPartner($params)
    {
        $prod = new Partner;
        
        $prod->name = $params['name'];
        $prod->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                $image = $this->uploadOne($params['image'], 'partners');
                $prod->image = $image;
            }
        $prod->status = $params['status'];
        $prod->feature = $params['feature'];
        $prod->meta_title = $params['meta_title'];
        $prod->meta_desc = $params['meta_desc'];
        $prod->meta_keywords = $params['meta_keywords'];
        $prod->slug = $this->createSlug($params['name']);
        $prod->save();

        return $prod;
    }

    protected function updateExistingPartner($id, $params)
    {
        $id->name = $params['name'];
        $id->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                if ($params['image'] != null) {
                $this->deleteOne($id->image);
                }
                $image = $this->uploadOne($params['image'], 'partners');
                $id->image = $image;
            }
        $id->status = $params['status'];
        $id->feature = $params['feature'];
        $id->meta_title = $params['meta_title'];
        $id->meta_desc = $params['meta_desc'];
        $id->meta_keywords = $params['meta_keywords'];
        $id->save();

        return $id;
    }

    public function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Partner::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
}
