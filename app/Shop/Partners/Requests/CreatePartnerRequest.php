<?php

namespace App\Shop\Partners\Requests;

use App\Shop\Base\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreatePartnerRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required']
        ];
    }
}
