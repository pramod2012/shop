<?php

namespace App\Shop\Categories;

use App\Shop\Products\Product;
use App\Shop\Promotions\Promotion;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Support\Collection;

class Category extends Model
{
    use NodeTrait, SearchableTrait;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'description',
        'cover',
        'status',
        'parent',
        'order',
        'meta_title',
        'meta_desc',
        'meta_keywords',
        'icon',
        'feature',
        'homepage'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    protected $searchable = [
        'columns' => [
            'categories.name' => 10,
            'categories.description' => 5
        ]
    ];

    public function category()
    {
        return $this->belongsTo('App\Shop\Categories\Category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Shop\Categories\Category', 'parent_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class);
    }

    public function promotions()
    {
        return $this->hasMany(Promotion::class)->orderBy('order','asc');
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->order = $model->max('order') + 1;
        });
    }

    public function searchCategory(string $term) : Collection
    {
        return self::search($term)->get();
    }
}
