<?php

namespace App\Shop\Sliders\Transformations;

use App\Shop\Sliders\Slider;
use Illuminate\Support\Facades\Storage;
use App\Shop\Tools\UploadableTrait;

trait SliderTransformable
{
    use UploadableTrait;
    /**
     * Transform the slider
     *
     * @param slider $slider
     * @return slider
     */
    protected function transformSlider(Slider $slider)
    {
        $prod = new Slider;
        $prod->id = (int) $slider->id;
        $prod->name = $slider->name;
        $prod->body = $slider->body;
        $prod->image = asset("storage/$slider->image");
        $prod->mobile_image = asset("storage/$slider->mobile_image");
        $prod->status = $slider->status;
        $prod->feature = $slider->feature;
        $prod->slug = $slider->slug;
        $prod->order = $slider->order;
        $prod->created_at = $slider->created_at;
        $prod->updated_at = $slider->updated_at;
        $prod->meta_title = $slider->meta_title;
        $prod->meta_desc = $slider->meta_desc;
        $prod->meta_keywords = $slider->meta_keywords;

        return $prod;
    }

    protected function createNewSlider($params)
    {
        $prod = new Slider;
        
        $prod->name = $params['name'];
        $prod->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                $image = $this->uploadOne($params['image'], 'sliders');
                $prod->image = $image;
            }
        if (isset($params['mobile_image']) && ($params['mobile_image'])) {
                $image = $this->uploadOne($params['mobile_image'], 'sliders');
                $prod->mobile_image = $image;
            }
        $prod->status = $params['status'];
        $prod->feature = $params['feature'];
        $prod->meta_title = $params['meta_title'];
        $prod->meta_desc = $params['meta_desc'];
        $prod->meta_keywords = $params['meta_keywords'];
        $prod->slug = $this->createSlug($params['name']);
        $prod->save();

        return $prod;
    }

    protected function updateExistingSlider($id, $params)
    {
        $id->name = $params['name'];
        $id->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                if ($params['image'] != null) {
                $this->deleteOne($id->image);
                }
                $image = $this->uploadOne($params['image'], 'sliders');
                $id->image = $image;
            }
        if (isset($params['mobile_image']) && ($params['mobile_image'])) {
                if ($params['mobile_image'] != null) {
                $this->deleteOne($id->mobile_image);
                }
                $mobile_image = $this->uploadOne($params['mobile_image'], 'sliders');
                $id->mobile_image = $mobile_image;
            }
        $id->status = $params['status'];
        $id->feature = $params['feature'];
        $id->meta_title = $params['meta_title'];
        $id->meta_desc = $params['meta_desc'];
        $id->meta_keywords = $params['meta_keywords'];
        $id->save();

        return $id;
    }

    public function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Slider::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
}
