<?php

namespace App\Shop\Sliders;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Support\Collection;

class Slider extends Model
{
    use SearchableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name',
        'order',
        'body',
        'image',
        'status',
        'feature',
        'meta_title',
        'meta_desc',
        'meta_keywords',
        'mobile_image'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    	'created_at',
        'updated_at'
    ];

    protected $searchable = [
        'columns' => [
            'sliders.name' => 10,
            'sliders.body' => 5
        ]
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->order = $model->max('order') + 1;
        });
    }

    public function searchSlider(string $term) : Collection
    {
        return self::search($term)->get();
    }
}
