<?php

namespace App\Shop\Sliders\Repositories\Interfaces;

use App\Shop\Sliders\Slider;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepositoryInterface;

interface SliderRepositoryInterface extends BaseRepositoryInterface
{
    public function listSliders(string $order = 'id', string $sort = 'desc', $except = []) : Collection;

    public function createSlider(array $params) : Slider;

    public function updateSlider(array $params) : Slider;

    public function findSliderById(int $id) : Slider;
    
    public function deleteSlider() : bool;

    public function searchSlider(string $text = null) : Collection;
}
