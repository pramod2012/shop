<?php

namespace App\Shop\Sliders\Repositories;

use App\Shop\Sliders\Slider;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepository;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Shop\Sliders\Repositories\Interfaces\SliderRepositoryInterface;
use App\Shop\Sliders\Transformations\SliderTransformable;
use App\Shop\Tools\UploadableTrait;

class SliderRepository extends BaseRepository implements SliderRepositoryInterface
{
    use UploadableTrait, SliderTransformable;
	/**
     * SliderRepository constructor.
     * 
     * @param Slider $dummy
     */
    public function __construct(Slider $slider)
    {
        parent::__construct($slider);
        $this->model = $slider;
    }

    /**
     * List all the Sliders
     *
     * @param string $order
     * @param string $sort
     * @param array $except
     * @return \Illuminate\Support\Collection
     */
    public function listSliders(string $order = 'id', string $sort = 'desc', $except = []) : Collection
    {
        return $this->model->orderBy($order, $sort)->get()->except($except);
    }

    /**
     * Create Slider
     *
     * @param array $params
     *
     * @return Slider
     * @throws InvalidArgumentException
     */
    public function createSlider(array $params) : Slider
    {
        try {
        	return $this->createNewSlider($params);
        } catch (QueryException $e) {
            throw new InvalidArgumentException($e->getMessage());
        }
    }

    /**
     * Update the dummy
     *
     * @param array $params
     * @return Slider
     */
    public function updateSlider(array $params) : Slider
    {
        $id = $this->findOneOrFail($this->model->id);
        return $this->updateExistingSlider($id, $params);
    }

    /**
     * @param int $id
     * 
     * @return Slider
     * @throws ModelNotFoundException
     */
    public function findSliderById(int $id) : Slider
    {
        try {
            return $this->transformSlider($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException($e->getMessage());
        }
    }

    /**
     * Delete a dummy
     *
     * @return bool
     */
    public function deleteSlider() : bool
    {
        $data = $this->findOneOrFail($this->model->id);
        if ($data->image != null) {
            $this->deleteOne($data->image);
        }
        return $this->model->where('id', $this->model->id)->delete();
    }

    public function searchSlider(string $text = null) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchSlider($text);
        } else {
            return $this->listSliders();
        }
    }
}