<?php

namespace App\Shop\Authors\Repositories;

use App\Shop\Authors\Author;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepository;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Shop\Authors\Exceptions\CreateAuthorErrorException;
use App\Shop\Authors\Exceptions\UpdateAuthorErrorException;
use App\Shop\Authors\Exceptions\AuthorNotFoundException;
use App\Shop\Authors\Repositories\Interfaces\AuthorRepositoryInterface;
use App\Shop\Authors\Transformations\AuthorTransformable;
use App\Shop\Tools\UploadableTrait;

class AuthorRepository extends BaseRepository implements AuthorRepositoryInterface
{
    use UploadableTrait, AuthorTransformable;
	/**
     * AuthorRepository constructor.
     * 
     * @param Author $dummy
     */
    public function __construct(Author $author)
    {
        parent::__construct($author);
        $this->model = $author;
    }

    /**
     * List all the Authors
     *
     * @param string $order
     * @param string $sort
     * @param array $except
     * @return \Illuminate\Support\Collection
     */
    public function listAuthors(string $order = 'id', string $sort = 'desc', $except = []) : Collection
    {
        return $this->model->orderBy($order, $sort)->get()->except($except);
    }

    /**
     * Create Author
     *
     * @param array $params
     *
     * @return Author
     * @throws InvalidArgumentException
     */
    public function createAuthor(array $params) : Author
    {
        try {
        	return $this->createNewAuthor($params);
        } catch (QueryException $e) {
            throw new CreateAuthorErrorException($e->getMessage());
        }
    }

    /**
     * Update the dummy
     *
     * @param array $params
     * @return Author
     */
    public function updateAuthor(array $params) : Author
    {
        try {
            $id = $this->findOneOrFail($this->model->id);
            return $this->updateExistingAuthor($id, $params);
        } catch (QueryException $e) {
            throw new UpdateAuthorErrorException($e->getMessage());
        }
        
    }

    /**
     * @param int $id
     * 
     * @return Author
     * @throws ModelNotFoundException
     */
    public function findAuthorById(int $id) : Author
    {
        try {
            return $this->transformAuthor($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new AuthorNotFoundException($e->getMessage());
        }
    }

    /**
     * Delete a dummy
     *
     * @return bool
     */
    public function deleteAuthor() : bool
    {
        $data = $this->findOneOrFail($this->model->id);
        if ($data->image != null) {
            $this->deleteOne($data->image);
        }
        return $this->model->where('id', $this->model->id)->delete();
    }

    public function searchAuthor(string $text = null) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchAuthor($text);
        } else {
            return $this->listAuthors();
        }
    }
}