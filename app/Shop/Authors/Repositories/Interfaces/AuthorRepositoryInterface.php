<?php

namespace App\Shop\Authors\Repositories\Interfaces;

use App\Shop\Authors\Author;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepositoryInterface;

interface AuthorRepositoryInterface extends BaseRepositoryInterface
{
    public function listAuthors(string $order = 'id', string $sort = 'desc', $except = []) : Collection;

    public function createAuthor(array $params) : Author;

    public function updateAuthor(array $params) : Author;

    public function findAuthorById(int $id) : Author;
    
    public function deleteAuthor() : bool;

    public function searchAuthor(string $text = null) : Collection;
}
