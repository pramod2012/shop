<?php

namespace App\Shop\Authors;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Support\Collection;
use App\Shop\Authors\Author;

class Author extends Model
{
    use SearchableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name',
        'order',
        'body',
        'image',
        'status',
        'feature',
        'meta_title',
        'meta_desc',
        'meta_keywords'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    	'created_at',
        'updated_at'
    ];

    protected $searchable = [
        'columns' => [
            'authors.name' => 10,
            'authors.body' => 5
        ]
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->order = $model->max('order') + 1;
        });
    }

    public function searchAuthor(string $term) : Collection
    {
        return self::search($term)->get();
    }

    public function posts() {
        return $this->hasMany(Post::class,'author_id');
    }
}
