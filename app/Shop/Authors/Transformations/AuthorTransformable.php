<?php

namespace App\Shop\Authors\Transformations;

use App\Shop\Authors\Author;
use Illuminate\Support\Facades\Storage;
use App\Shop\Tools\UploadableTrait;

trait AuthorTransformable
{
    use UploadableTrait;
    /**
     * Transform the author
     *
     * @param author $author
     * @return author
     */
    protected function transformAuthor(Author $author)
    {
        $prod = new Author;
        $prod->id = (int) $author->id;
        $prod->name = $author->name;
        $prod->body = $author->body;
        $prod->image = asset("storage/$author->image");
        $prod->status = $author->status;
        $prod->feature = $author->feature;
        $prod->slug = $author->slug;
        $prod->order = $author->order;
        $prod->created_at = $author->created_at;
        $prod->updated_at = $author->updated_at;
        $prod->meta_title = $author->meta_title;
        $prod->meta_desc = $author->meta_desc;
        $prod->meta_keywords = $author->meta_keywords;

        return $prod;
    }

    protected function createNewAuthor($params)
    {
        $prod = new Author;
        
        $prod->name = $params['name'];
        $prod->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                $image = $this->uploadOne($params['image'], 'authors');
                $prod->image = $image;
            }
        $prod->status = $params['status'];
        $prod->feature = $params['feature'];
        $prod->meta_title = $params['meta_title'];
        $prod->meta_desc = $params['meta_desc'];
        $prod->meta_keywords = $params['meta_keywords'];
        $prod->slug = $this->createSlug($params['name']);
        $prod->save();

        return $prod;
    }

    protected function updateExistingAuthor($id, $params)
    {
        $id->name = $params['name'];
        $id->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                if ($params['image'] != null) {
                $this->deleteOne($id->image);
                }
                $image = $this->uploadOne($params['image'], 'authors');
                $id->image = $image;
            }
        $id->status = $params['status'];
        $id->feature = $params['feature'];
        $id->meta_title = $params['meta_title'];
        $id->meta_desc = $params['meta_desc'];
        $id->meta_keywords = $params['meta_keywords'];
        $id->save();

        return $id;
    }

    public function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Author::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
}
