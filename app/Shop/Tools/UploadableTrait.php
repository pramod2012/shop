<?php

namespace App\Shop\Tools;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

trait UploadableTrait
{
    /**
     * Upload a single file in the server
     *
     * @param UploadedFile $file
     * @param null $folder
     * @param string $disk
     * @param null $filename
     * @return false|string
     */
    public function uploadOne(UploadedFile $file, $folder = null, $disk = 'public', $filename = null)
    {
        $name = !is_null($filename) ? $filename : str_random(25);
        
        $files = Image::make($file)->resize(null, 900, function ($constraint) {
                    $constraint->aspectRatio(); //to preserve the aspect ratio
                    $constraint->upsize();
                })->save(public_path("upsize_images/$folder/" . $name . "." . $file->getClientOriginalExtension() ));
        
        $waterMarkUrl = public_path('images/www.png');
        $waterMark = Image::make($file)->insert($waterMarkUrl, 'center', 5, 5)->save(public_path("watermark/$folder/" . $name . "." . $file->getClientOriginalExtension() ));
        $croppath = public_path("images_small/$folder/" . $name . "." . $file->getClientOriginalExtension() );
 
        $files->crop(400, 250)->save($croppath);
        
        return $file->storeAs(
            $folder,
            $name . "." . $file->getClientOriginalExtension(),
            $disk
        );
    }

    /**
     * @param UploadedFile $file
     *
     * @param string $folder
     * @param string $disk
     *
     * @return false|string
     */
    public function storeFile(UploadedFile $file, $folder = 'products', $disk = 'public')
    {
        return $file->store($folder, ['disk' => $disk]);
    }

    public function deleteOne($path = null, $disk = 'public')
    {
        @unlink('watermark/' . $path);
        @unlink('images_small/' . $path);
        Storage::disk($disk)->delete($path);
    }
}
