<?php

namespace App\Shop\Flashes\Transformations;

use App\Shop\Flashes\Flash;
use Illuminate\Support\Facades\Storage;
use App\Shop\Tools\UploadableTrait;

trait FlashTransformable
{
    use UploadableTrait;
    /**
     * Transform the flash
     *
     * @param flash $flash
     * @return flash
     */
    protected function transformFlash(Flash $flash)
    {
        $prod = new Flash;
        $prod->id = (int) $flash->id;
        $prod->name = $flash->name;
        $prod->body = $flash->body;
        $prod->image = asset("storage/$flash->image");
        $prod->status = $flash->status;
        $prod->feature = $flash->feature;
        $prod->slug = $flash->slug;
        $prod->order = $flash->order;
        $prod->link = $flash->link;
        $prod->created_at = $flash->created_at;
        $prod->updated_at = $flash->updated_at;
        $prod->meta_title = $flash->meta_title;
        $prod->meta_desc = $flash->meta_desc;
        $prod->meta_keywords = $flash->meta_keywords;

        return $prod;
    }

    protected function createNewFlash($params)
    {
        $prod = new Flash;
        
        $prod->name = $params['name'];
        $prod->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                $image = $this->uploadOne($params['image'], 'flashes');
                $prod->image = $image;
            }
        $prod->status = $params['status'];
        $prod->feature = $params['feature'];
        $prod->meta_title = $params['meta_title'];
        $prod->meta_desc = $params['meta_desc'];
        $prod->meta_keywords = $params['meta_keywords'];
        $prod->slug = $this->createSlug($params['name']);
        $prod->link = $params['link'];
        $prod->save();

        return $prod;
    }

    protected function updateExistingFlash($id, $params)
    {
        $id->name = $params['name'];
        $id->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                if ($params['image'] != null) {
                $this->deleteOne($id->image);
                }
                $image = $this->uploadOne($params['image'], 'flashes');
                $id->image = $image;
            }
        $id->status = $params['status'];
        $id->feature = $params['feature'];
        $id->meta_title = $params['meta_title'];
        $id->meta_desc = $params['meta_desc'];
        $id->meta_keywords = $params['meta_keywords'];
        $prod->link = $params['link'];
        $id->save();

        return $id;
    }

    public function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Flash::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
}
