<?php

namespace App\Shop\Flashes\Repositories\Interfaces;

use App\Shop\Flashes\Flash;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepositoryInterface;

interface FlashRepositoryInterface extends BaseRepositoryInterface
{
    public function listFlashes(string $order = 'id', string $sort = 'desc', $except = []) : Collection;

    public function createFlash(array $params) : Flash;

    public function updateFlash(array $params) : Flash;

    public function findFlashById(int $id) : Flash;
    
    public function deleteFlash() : bool;

    public function searchFlash(string $text = null) : Collection;
}
