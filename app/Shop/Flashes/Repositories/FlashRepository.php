<?php

namespace App\Shop\Flashes\Repositories;

use App\Shop\Flashes\Flash;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepository;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Shop\Flashes\Repositories\Interfaces\FlashRepositoryInterface;
use App\Shop\Flashes\Transformations\FlashTransformable;
use App\Shop\Tools\UploadableTrait;

class FlashRepository extends BaseRepository implements FlashRepositoryInterface
{
    use UploadableTrait, FlashTransformable;
	/**
     * FlashRepository constructor.
     * 
     * @param Flash $dummy
     */
    public function __construct(Flash $flash)
    {
        parent::__construct($flash);
        $this->model = $flash;
    }

    /**
     * List all the Flashes
     *
     * @param string $order
     * @param string $sort
     * @param array $except
     * @return \Illuminate\Support\Collection
     */
    public function listFlashes(string $order = 'id', string $sort = 'desc', $except = []) : Collection
    {
        return $this->model->orderBy($order, $sort)->get()->except($except);
    }

    /**
     * Create Flash
     *
     * @param array $params
     *
     * @return Flash
     * @throws InvalidArgumentException
     */
    public function createFlash(array $params) : Flash
    {
        try {
        	return $this->createNewFlash($params);
        } catch (QueryException $e) {
            throw new InvalidArgumentException($e->getMessage());
        }
    }

    /**
     * Update the dummy
     *
     * @param array $params
     * @return Flash
     */
    public function updateFlash(array $params) : Flash
    {
        $id = $this->findOneOrFail($this->model->id);
        return $this->updateExistingFlash($id, $params);
    }

    /**
     * @param int $id
     * 
     * @return Flash
     * @throws ModelNotFoundException
     */
    public function findFlashById(int $id) : Flash
    {
        try {
            return $this->transformFlash($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException($e->getMessage());
        }
    }

    /**
     * Delete a dummy
     *
     * @return bool
     */
    public function deleteFlash() : bool
    {
        $data = $this->findOneOrFail($this->model->id);
        if ($data->image != null) {
            $this->deleteOne($data->image);
        }
        return $this->model->where('id', $this->model->id)->delete();
    }

    public function searchFlash(string $text = null) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchFlash($text);
        } else {
            return $this->listFlashes();
        }
    }
}