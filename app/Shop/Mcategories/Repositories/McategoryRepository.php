<?php

namespace App\Shop\Mcategories\Repositories;

use App\Shop\Mcategories\Mcategory;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepository;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Shop\Mcategories\Exceptions\CreateMcategoryErrorException;
use App\Shop\Mcategories\Exceptions\UpdateMcategoryErrorException;
use App\Shop\Mcategories\Exceptions\McategoryNotFoundException;
use App\Shop\Mcategories\Repositories\Interfaces\McategoryRepositoryInterface;
use App\Shop\Mcategories\Transformations\McategoryTransformable;
use App\Shop\Tools\UploadableTrait;

class McategoryRepository extends BaseRepository implements McategoryRepositoryInterface
{
    use UploadableTrait, McategoryTransformable;
	/**
     * McategoryRepository constructor.
     * 
     * @param Mcategory $dummy
     */
    public function __construct(Mcategory $mcategory)
    {
        parent::__construct($mcategory);
        $this->model = $mcategory;
    }

    /**
     * List all the Mcategories
     *
     * @param string $order
     * @param string $sort
     * @param array $except
     * @return \Illuminate\Support\Collection
     */
    public function listMcategories(string $order = 'id', string $sort = 'desc', $except = []) : Collection
    {
        return $this->model->orderBy($order, $sort)->get()->except($except);
    }

    /**
     * Create Mcategory
     *
     * @param array $params
     *
     * @return Mcategory
     * @throws InvalidArgumentException
     */
    public function createMcategory(array $params) : Mcategory
    {
        try {
        	return $this->createNewMcategory($params);
        } catch (QueryException $e) {
            throw new CreateMcategoryErrorException($e->getMessage());
        }
    }

    /**
     * Update the dummy
     *
     * @param array $params
     * @return Mcategory
     */
    public function updateMcategory(array $params) : Mcategory
    {
        try {
            $id = $this->findOneOrFail($this->model->id);
            return $this->updateExistingMcategory($id, $params);
        } catch (QueryException $e) {
            throw new UpdateMcategoryErrorException($e->getMessage());
        }
        
    }

    /**
     * @param int $id
     * 
     * @return Mcategory
     * @throws ModelNotFoundException
     */
    public function findMcategoryById(int $id) : Mcategory
    {
        try {
            return $this->transformMcategory($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new McategoryNotFoundException($e->getMessage());
        }
    }

    /**
     * Delete a dummy
     *
     * @return bool
     */
    public function deleteMcategory() : bool
    {
        $data = $this->findOneOrFail($this->model->id);
        if ($data->image != null) {
            $this->deleteOne($data->image);
        }
        return $this->model->where('id', $this->model->id)->delete();
    }

    public function searchMcategory(string $text = null) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchMcategory($text);
        } else {
            return $this->listMcategories();
        }
    }
}