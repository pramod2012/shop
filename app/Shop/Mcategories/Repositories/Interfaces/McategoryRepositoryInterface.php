<?php

namespace App\Shop\Mcategories\Repositories\Interfaces;

use App\Shop\Mcategories\Mcategory;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepositoryInterface;

interface McategoryRepositoryInterface extends BaseRepositoryInterface
{
    public function listMcategories(string $order = 'id', string $sort = 'desc', $except = []) : Collection;

    public function createMcategory(array $params) : Mcategory;

    public function updateMcategory(array $params) : Mcategory;

    public function findMcategoryById(int $id) : Mcategory;
    
    public function deleteMcategory() : bool;

    public function searchMcategory(string $text = null) : Collection;
}
