<?php

namespace App\Shop\Mcategories\Transformations;

use App\Shop\Mcategories\Mcategory;
use Illuminate\Support\Facades\Storage;
use App\Shop\Tools\UploadableTrait;

trait McategoryTransformable
{
    use UploadableTrait;
    /**
     * Transform the mcategory
     *
     * @param mcategory $mcategory
     * @return mcategory
     */
    protected function transformMcategory(Mcategory $mcategory)
    {
        $prod = new Mcategory;
        $prod->id = (int) $mcategory->id;
        $prod->name = $mcategory->name;
        $prod->body = $mcategory->body;
        $prod->image = asset("storage/$mcategory->image");
        $prod->status = $mcategory->status;
        $prod->feature = $mcategory->feature;
        $prod->slug = $mcategory->slug;
        $prod->order = $mcategory->order;
        $prod->created_at = $mcategory->created_at;
        $prod->updated_at = $mcategory->updated_at;
        $prod->meta_title = $mcategory->meta_title;
        $prod->meta_desc = $mcategory->meta_desc;
        $prod->meta_keywords = $mcategory->meta_keywords;

        return $prod;
    }

    protected function createNewMcategory($params)
    {
        $prod = new Mcategory;
        
        $prod->name = $params['name'];
        $prod->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                $image = $this->uploadOne($params['image'], 'mcategories');
                $prod->image = $image;
            }
        $prod->status = $params['status'];
        $prod->feature = $params['feature'];
        $prod->meta_title = $params['meta_title'];
        $prod->meta_desc = $params['meta_desc'];
        $prod->meta_keywords = $params['meta_keywords'];
        $prod->slug = $this->createSlug($params['name']);
        $prod->save();

        return $prod;
    }

    protected function updateExistingMcategory($id, $params)
    {
        $id->name = $params['name'];
        $id->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                if ($params['image'] != null) {
                $this->deleteOne($id->image);
                }
                $image = $this->uploadOne($params['image'], 'mcategories');
                $id->image = $image;
            }
        $id->status = $params['status'];
        $id->feature = $params['feature'];
        $id->meta_title = $params['meta_title'];
        $id->meta_desc = $params['meta_desc'];
        $id->meta_keywords = $params['meta_keywords'];
        $id->save();

        return $id;
    }

    public function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Mcategory::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
}
