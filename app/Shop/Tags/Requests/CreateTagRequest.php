<?php

namespace App\Shop\Tags\Requests;

use App\Shop\Base\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateTagRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required']
        ];
    }
}
