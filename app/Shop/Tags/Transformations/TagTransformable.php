<?php

namespace App\Shop\Tags\Transformations;

use App\Shop\Tags\Tag;
use Illuminate\Support\Facades\Storage;
use App\Shop\Tools\UploadableTrait;

trait TagTransformable
{
    use UploadableTrait;
    /**
     * Transform the tag
     *
     * @param tag $tag
     * @return tag
     */
    protected function transformTag(Tag $tag)
    {
        $prod = new Tag;
        $prod->id = (int) $tag->id;
        $prod->name = $tag->name;
        $prod->body = $tag->body;
        $prod->image = asset("storage/$tag->image");
        $prod->status = $tag->status;
        $prod->feature = $tag->feature;
        $prod->slug = $tag->slug;
        $prod->order = $tag->order;
        $prod->created_at = $tag->created_at;
        $prod->updated_at = $tag->updated_at;
        $prod->meta_title = $tag->meta_title;
        $prod->meta_desc = $tag->meta_desc;
        $prod->meta_keywords = $tag->meta_keywords;

        return $prod;
    }

    protected function createNewTag($params)
    {
        $prod = new Tag;
        
        $prod->name = $params['name'];
        $prod->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                $image = $this->uploadOne($params['image'], 'tags');
                $prod->image = $image;
            }
        $prod->status = $params['status'];
        $prod->feature = $params['feature'];
        $prod->meta_title = $params['meta_title'];
        $prod->meta_desc = $params['meta_desc'];
        $prod->meta_keywords = $params['meta_keywords'];
        $prod->slug = $this->createSlug($params['name']);
        $prod->save();

        return $prod;
    }

    protected function updateExistingTag($id, $params)
    {
        $id->name = $params['name'];
        $id->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                if ($params['image'] != null) {
                $this->deleteOne($id->image);
                }
                $image = $this->uploadOne($params['image'], 'tags');
                $id->image = $image;
            }
        $id->status = $params['status'];
        $id->feature = $params['feature'];
        $id->meta_title = $params['meta_title'];
        $id->meta_desc = $params['meta_desc'];
        $id->meta_keywords = $params['meta_keywords'];
        $id->save();

        return $id;
    }

    public function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Tag::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
}
