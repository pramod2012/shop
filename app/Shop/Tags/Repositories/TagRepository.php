<?php

namespace App\Shop\Tags\Repositories;

use App\Shop\Tags\Tag;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepository;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Shop\Tags\Exceptions\CreateTagErrorException;
use App\Shop\Tags\Exceptions\UpdateTagErrorException;
use App\Shop\Tags\Exceptions\TagNotFoundException;
use App\Shop\Tags\Repositories\Interfaces\TagRepositoryInterface;
use App\Shop\Tags\Transformations\TagTransformable;
use App\Shop\Tools\UploadableTrait;

class TagRepository extends BaseRepository implements TagRepositoryInterface
{
    use UploadableTrait, TagTransformable;
	/**
     * TagRepository constructor.
     * 
     * @param Tag $dummy
     */
    public function __construct(Tag $tag)
    {
        parent::__construct($tag);
        $this->model = $tag;
    }

    /**
     * List all the Tags
     *
     * @param string $order
     * @param string $sort
     * @param array $except
     * @return \Illuminate\Support\Collection
     */
    public function listTags(string $order = 'id', string $sort = 'desc', $except = []) : Collection
    {
        return $this->model->orderBy($order, $sort)->get()->except($except);
    }

    /**
     * Create Tag
     *
     * @param array $params
     *
     * @return Tag
     * @throws InvalidArgumentException
     */
    public function createTag(array $params) : Tag
    {
        try {
        	return $this->createNewTag($params);
        } catch (QueryException $e) {
            throw new CreateTagErrorException($e->getMessage());
        }
    }

    /**
     * Update the dummy
     *
     * @param array $params
     * @return Tag
     */
    public function updateTag(array $params) : Tag
    {
        try {
            $id = $this->findOneOrFail($this->model->id);
            return $this->updateExistingTag($id, $params);
        } catch (QueryException $e) {
            throw new UpdateTagErrorException($e->getMessage());
        }
        
    }

    /**
     * @param int $id
     * 
     * @return Tag
     * @throws ModelNotFoundException
     */
    public function findTagById(int $id) : Tag
    {
        try {
            return $this->transformTag($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new TagNotFoundException($e->getMessage());
        }
    }

    /**
     * Delete a dummy
     *
     * @return bool
     */
    public function deleteTag() : bool
    {
        $data = $this->findOneOrFail($this->model->id);
        if ($data->image != null) {
            $this->deleteOne($data->image);
        }
        return $this->model->where('id', $this->model->id)->delete();
    }

    public function searchTag(string $text = null) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchTag($text);
        } else {
            return $this->listTags();
        }
    }
}