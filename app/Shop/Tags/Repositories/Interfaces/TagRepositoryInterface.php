<?php

namespace App\Shop\Tags\Repositories\Interfaces;

use App\Shop\Tags\Tag;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepositoryInterface;

interface TagRepositoryInterface extends BaseRepositoryInterface
{
    public function listTags(string $order = 'id', string $sort = 'desc', $except = []) : Collection;

    public function createTag(array $params) : Tag;

    public function updateTag(array $params) : Tag;

    public function findTagById(int $id) : Tag;
    
    public function deleteTag() : bool;

    public function searchTag(string $text = null) : Collection;
}
