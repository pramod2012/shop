<?php

namespace App\Shop\Ratings\Repositories\Interfaces;

use App\Shop\Ratings\Rating;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepositoryInterface;

interface RatingRepositoryInterface extends BaseRepositoryInterface
{
    public function listRatings(string $order = 'id', string $sort = 'desc', $except = []) : Collection;

    public function createRating(array $params) : Rating;

    public function updateRating(array $params) : Rating;

    public function findRatingById(int $id) : Rating;
    
    public function deleteRating() : bool;

    public function searchRating(string $text = null) : Collection;
}
