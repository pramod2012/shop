<?php

namespace App\Shop\Ratings\Repositories;

use App\Shop\Ratings\Rating;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepository;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Shop\Ratings\Exceptions\CreateRatingErrorException;
use App\Shop\Ratings\Exceptions\UpdateRatingErrorException;
use App\Shop\Ratings\Exceptions\RatingNotFoundException;
use App\Shop\Ratings\Repositories\Interfaces\RatingRepositoryInterface;
use App\Shop\Ratings\Transformations\RatingTransformable;
use App\Shop\Tools\UploadableTrait;

class RatingRepository extends BaseRepository implements RatingRepositoryInterface
{
    use UploadableTrait, RatingTransformable;
	/**
     * RatingRepository constructor.
     * 
     * @param Rating $dummy
     */
    public function __construct(Rating $rating)
    {
        parent::__construct($rating);
        $this->model = $rating;
    }

    /**
     * List all the Ratings
     *
     * @param string $order
     * @param string $sort
     * @param array $except
     * @return \Illuminate\Support\Collection
     */
    public function listRatings(string $order = 'id', string $sort = 'desc', $except = []) : Collection
    {
        return $this->model->orderBy($order, $sort)->get()->except($except);
    }

    /**
     * Create Rating
     *
     * @param array $params
     *
     * @return Rating
     * @throws InvalidArgumentException
     */
    public function createRating(array $params) : Rating
    {
        try {
        	return $this->createNewRating($params);
        } catch (QueryException $e) {
            throw new CreateRatingErrorException($e->getMessage());
        }
    }

    /**
     * Update the dummy
     *
     * @param array $params
     * @return Rating
     */
    public function updateRating(array $params) : Rating
    {
        try {
            $id = $this->findOneOrFail($this->model->id);
            return $this->updateExistingRating($id, $params);
        } catch (QueryException $e) {
            throw new UpdateRatingErrorException($e->getMessage());
        }
        
    }

    /**
     * @param int $id
     * 
     * @return Rating
     * @throws ModelNotFoundException
     */
    public function findRatingById(int $id) : Rating
    {
        try {
            return $this->transformRating($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new RatingNotFoundException($e->getMessage());
        }
    }

    /**
     * Delete a dummy
     *
     * @return bool
     */
    public function deleteRating() : bool
    {
        $data = $this->findOneOrFail($this->model->id);
        if ($data->image != null) {
            $this->deleteOne($data->image);
        }
        return $this->model->where('id', $this->model->id)->delete();
    }

    public function searchRating(string $text = null) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchRating($text);
        } else {
            return $this->listRatings();
        }
    }
}