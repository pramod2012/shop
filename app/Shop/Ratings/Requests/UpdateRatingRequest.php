<?php

namespace App\Shop\Ratings\Requests;

use App\Shop\Base\BaseFormRequest;
use Illuminate\Validation\Rule;

class UpdateRatingRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'product_id' => ['required'],
            'rating' => ['required'],
            'body' => ['required'],
        ];
    }
}
