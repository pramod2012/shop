<?php

namespace App\Shop\Ratings\Transformations;

use App\Shop\Ratings\Rating;
use Illuminate\Support\Facades\Storage;
use App\Shop\Tools\UploadableTrait;

trait RatingTransformable
{
    use UploadableTrait;
    /**
     * Transform the rating
     *
     * @param rating $rating
     * @return rating
     */
    protected function transformRating(Rating $rating)
    {
        $prod = new Rating;
        $prod->id = (int) $rating->id;
        $prod->name = $rating->name;
        $prod->body = $rating->body;

        $prod->mobile = $rating->mobile;
        $prod->email = $rating->email;
        $prod->subject = $rating->subject;
        $prod->rating = $rating->rating;

        $prod->status = $rating->status;
        $prod->feature = $rating->feature;
        $prod->slug = $rating->slug;
        $prod->order = $rating->order;
        $prod->created_at = $rating->created_at;
        $prod->updated_at = $rating->updated_at;
        $prod->meta_title = $rating->meta_title;
        $prod->meta_desc = $rating->meta_desc;
        $prod->meta_keywords = $rating->meta_keywords;

        return $prod;
    }

    protected function createNewRating($params)
    {
        $prod = new Rating;
        
        $prod->name = $params['name'];
        $prod->body = $params['body'];

        $prod->mobile = $params['mobile'];
        $prod->email = $params['email'];
        $prod->subject = $params['subject'];
        $prod->rating = $params['rating'];
        $prod->product_id = $params['product_id'];

        $prod->status = $params['status'];
        $prod->feature = $params['feature'];
        $prod->meta_title = $params['meta_title'];
        $prod->meta_desc = $params['meta_desc'];
        $prod->meta_keywords = $params['meta_keywords'];
        $prod->slug = $this->createSlug($params['name']);
        $prod->save();

        return $prod;
    }

    protected function updateExistingRating($id, $params)
    {
        $id->name = $params['name'];
        $id->body = $params['body'];
        
        $id->mobile = $params['mobile'];
        $id->email = $params['email'];
        $id->subject = $params['subject'];
        $id->rating = $params['rating'];
        $id->product_id = $params['product_id'];

        $id->status = $params['status'];
        $id->feature = $params['feature'];
        $id->meta_title = $params['meta_title'];
        $id->meta_desc = $params['meta_desc'];
        $id->meta_keywords = $params['meta_keywords'];
        $id->slug = $params['slug'];
        $id->save();

        return $id;
    }

    public function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Rating::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
}
