<?php

namespace App\Shop\Ratings;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Support\Collection;

class Rating extends Model
{
    use SearchableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name',
        'order',
        'body',
        'image',
        'status',
        'feature',
        'meta_title',
        'meta_desc',
        'meta_keywords',
        'product_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    	'created_at',
        'updated_at'
    ];

    protected $searchable = [
        'columns' => [
            'ratings.name' => 10,
            'ratings.body' => 5
        ]
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->order = $model->max('order') + 1;
        });
    }

    public function searchRating(string $term) : Collection
    {
        return self::search($term)->get();
    }
}
