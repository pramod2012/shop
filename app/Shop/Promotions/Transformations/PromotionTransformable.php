<?php

namespace App\Shop\Promotions\Transformations;

use App\Shop\Promotions\Promotion;
use Illuminate\Support\Facades\Storage;
use App\Shop\Tools\UploadableTrait;

trait PromotionTransformable
{
    use UploadableTrait;
    /**
     * Transform the promotion
     *
     * @param promotion $promotion
     * @return promotion
     */
    protected function transformPromotion(Promotion $promotion)
    {
        $prod = new Promotion;
        $prod->id = (int) $promotion->id;
        $prod->name = $promotion->name;
        $prod->body = $promotion->body;
        $prod->image = asset("storage/$promotion->image");
        $prod->status = $promotion->status;
        $prod->feature = $promotion->feature;
        $prod->slug = $promotion->slug;
        $prod->order = $promotion->order;
        $prod->created_at = $promotion->created_at;
        $prod->updated_at = $promotion->updated_at;
        $prod->meta_title = $promotion->meta_title;
        $prod->meta_desc = $promotion->meta_desc;
        $prod->meta_keywords = $promotion->meta_keywords;
        $prod->link = $promotion->link;
        $prod->category_id = $promotion->category_id;

        return $prod;
    }

    protected function createNewPromotion($params)
    {
        $prod = new Promotion;
        
        $prod->name = $params['name'];
        $prod->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                $image = $this->uploadOne($params['image'], 'promotions');
                $prod->image = $image;
            }
        $prod->status = $params['status'];
        $prod->feature = $params['feature'];
        $prod->meta_title = $params['meta_title'];
        $prod->meta_desc = $params['meta_desc'];
        $prod->meta_keywords = $params['meta_keywords'];
        $prod->slug = $this->createSlug($params['name']);
        $prod->link = $params['link'];
        $prod->category_id = $params['category_id'];
        $prod->save();

        return $prod;
    }

    protected function updateExistingPromotion($id, $params)
    {
        $id->name = $params['name'];
        $id->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                if ($params['image'] != null) {
                $this->deleteOne($id->image);
                }
                $image = $this->uploadOne($params['image'], 'promotions');
                $id->image = $image;
            }
        $id->status = $params['status'];
        $id->feature = $params['feature'];
        $id->meta_title = $params['meta_title'];
        $id->meta_desc = $params['meta_desc'];
        $id->meta_keywords = $params['meta_keywords'];
        $id->link = $params['link'];
        $id->category_id = $params['category_id'];
        $id->save();

        return $id;
    }

    public function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Promotion::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
}
