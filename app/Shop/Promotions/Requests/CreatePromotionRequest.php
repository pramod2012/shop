<?php

namespace App\Shop\Promotions\Requests;

use App\Shop\Base\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreatePromotionRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required']
        ];
    }
}
