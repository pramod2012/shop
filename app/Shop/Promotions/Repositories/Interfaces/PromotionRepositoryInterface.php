<?php

namespace App\Shop\Promotions\Repositories\Interfaces;

use App\Shop\Promotions\Promotion;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepositoryInterface;

interface PromotionRepositoryInterface extends BaseRepositoryInterface
{
    public function listPromotions(string $order = 'id', string $sort = 'desc', $except = []) : Collection;

    public function createPromotion(array $params) : Promotion;

    public function updatePromotion(array $params) : Promotion;

    public function findPromotionById(int $id) : Promotion;
    
    public function deletePromotion() : bool;

    public function searchPromotion(string $text = null) : Collection;
}
