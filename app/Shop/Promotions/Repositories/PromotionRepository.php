<?php

namespace App\Shop\Promotions\Repositories;

use App\Shop\Promotions\Promotion;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepository;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Shop\Promotions\Repositories\Interfaces\PromotionRepositoryInterface;
use App\Shop\Promotions\Transformations\PromotionTransformable;
use App\Shop\Tools\UploadableTrait;

class PromotionRepository extends BaseRepository implements PromotionRepositoryInterface
{
    use UploadableTrait, PromotionTransformable;
	/**
     * PromotionRepository constructor.
     * 
     * @param Promotion $dummy
     */
    public function __construct(Promotion $promotion)
    {
        parent::__construct($promotion);
        $this->model = $promotion;
    }

    /**
     * List all the Promotions
     *
     * @param string $order
     * @param string $sort
     * @param array $except
     * @return \Illuminate\Support\Collection
     */
    public function listPromotions(string $order = 'id', string $sort = 'desc', $except = []) : Collection
    {
        return $this->model->orderBy($order, $sort)->get()->except($except);
    }

    /**
     * Create Promotion
     *
     * @param array $params
     *
     * @return Promotion
     * @throws InvalidArgumentException
     */
    public function createPromotion(array $params) : Promotion
    {
        try {
        	return $this->createNewPromotion($params);
        } catch (QueryException $e) {
            throw new InvalidArgumentException($e->getMessage());
        }
    }

    /**
     * Update the dummy
     *
     * @param array $params
     * @return Promotion
     */
    public function updatePromotion(array $params) : Promotion
    {
        $id = $this->findOneOrFail($this->model->id);
        return $this->updateExistingPromotion($id, $params);
    }

    /**
     * @param int $id
     * 
     * @return Promotion
     * @throws ModelNotFoundException
     */
    public function findPromotionById(int $id) : Promotion
    {
        try {
            return $this->transformPromotion($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new ModelNotFoundException($e->getMessage());
        }
    }

    /**
     * Delete a dummy
     *
     * @return bool
     */
    public function deletePromotion() : bool
    {
        $data = $this->findOneOrFail($this->model->id);
        if ($data->image != null) {
            $this->deleteOne($data->image);
        }
        return $this->model->where('id', $this->model->id)->delete();
    }

    public function searchPromotion(string $text = null) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchPromotion($text);
        } else {
            return $this->listPromotions();
        }
    }
}