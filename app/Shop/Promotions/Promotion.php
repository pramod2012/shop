<?php

namespace App\Shop\Promotions;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Support\Collection;
use App\Shop\Categories\Category;

class Promotion extends Model
{
    use SearchableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name',
        'order',
        'body',
        'image',
        'status',
        'feature',
        'meta_title',
        'meta_desc',
        'meta_keywords',
        'link',
        'category_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    	'created_at',
        'updated_at'
    ];

    protected $searchable = [
        'columns' => [
            'promotions.name' => 10,
            'promotions.body' => 5
        ]
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->order = $model->max('order') + 1;
        });
    }

    public function searchPromotion(string $term) : Collection
    {
        return self::search($term)->get();
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
}
