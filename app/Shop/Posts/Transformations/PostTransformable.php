<?php

namespace App\Shop\Posts\Transformations;

use App\Shop\Posts\Post;
use Illuminate\Support\Facades\Storage;
use App\Shop\Tools\UploadableTrait;

trait PostTransformable
{
    use UploadableTrait;
    /**
     * Transform the post
     *
     * @param post $post
     * @return post
     */
    protected function transformPost(Post $post)
    {
        $prod = new Post;
        $prod->id = (int) $post->id;
        $prod->name = $post->name;
        $prod->body = $post->body;
        $prod->image = asset("storage/$post->image");
        $prod->status = $post->status;
        $prod->feature = $post->feature;
        $prod->slug = $post->slug;
        $prod->order = $post->order;
        $prod->created_at = $post->created_at;
        $prod->updated_at = $post->updated_at;
        $prod->meta_title = $post->meta_title;
        $prod->meta_desc = $post->meta_desc;
        $prod->meta_keywords = $post->meta_keywords;
        $prod->mcategory_id = $post->mcategory_id;
        $prod->author_id = $post->author_id;
        $prod->summary = $post->summary;

        return $prod;
    }

    protected function createNewPost($params)
    {
        $prod = new Post;
        
        $prod->name = $params['name'];
        $prod->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                $image = $this->uploadOne($params['image'], 'posts');
                $prod->image = $image;
            }
        $prod->status = $params['status'];
        $prod->feature = $params['feature'];
        $prod->meta_title = $params['meta_title'];
        $prod->meta_desc = $params['meta_desc'];
        $prod->meta_keywords = $params['meta_keywords'];
        $prod->slug = $this->createSlug($params['name']);
        $prod->author_id = $params['author_id'];
        $prod->mcategory_id = $params['mcategory_id'];
        $prod->is_blog = $params['is_blog'];
        $prod->summary = $params['summary'];
        $prod->save();

        if (isset($params['tag_id'])) {
                $prod->tags()->sync($params['tag_id']);
            }

        return $prod;
    }

    protected function updateExistingPost($id, $params)
    {
        $id->name = $params['name'];
        $id->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                if ($params['image'] != null) {
                $this->deleteOne($id->image);
                }
                $image = $this->uploadOne($params['image'], 'posts');
                $id->image = $image;
            }
        $id->status = $params['status'];
        $id->feature = $params['feature'];
        $id->meta_title = $params['meta_title'];
        $id->meta_desc = $params['meta_desc'];
        $id->meta_keywords = $params['meta_keywords'];
        $id->author_id = $params['author_id'];
        $id->mcategory_id = $params['mcategory_id'];
        $id->is_blog = $params['is_blog'];
        $id->summary = $params['summary'];
        $id->save();

        if ($params['tag_id']) {
                $id->tags()->sync($params['tag_id']);
            }

        return $id;
    }

    public function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Post::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
}
