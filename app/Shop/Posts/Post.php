<?php

namespace App\Shop\Posts;
use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Support\Collection;
use App\Shop\Tags\Tag;
use App\Shop\Authors\Author;
use App\Shop\Mcategories\Mcategory;

class Post extends Model
{
    use SearchableTrait;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name',
        'order',
        'body',
        'image',
        'status',
        'feature',
        'meta_title',
        'meta_desc',
        'meta_keywords',
        'is_blog',
        'author_id',
        'mcategory_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    	'created_at',
        'updated_at'
    ];

    protected $searchable = [
        'columns' => [
            'posts.name' => 10,
            'posts.body' => 5
        ]
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->order = $model->max('order') + 1;
        });
    }

    public function searchPost(string $term) : Collection
    {
        return self::search($term)->get();
    }

    public function mcategory(){
        return $this->belongsTo(Mcategory::class,'mcategory_id');
    }

    public function author(){
        return $this->belongsTo(Author::class,'author_id');
    }

    public function tags(){
        return $this->belongsToMany(Tag::class,'post_tag');
    }
}
