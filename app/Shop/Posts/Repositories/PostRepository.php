<?php

namespace App\Shop\Posts\Repositories;

use App\Shop\Posts\Post;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepository;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Shop\Posts\Exceptions\CreatePostErrorException;
use App\Shop\Posts\Exceptions\UpdatePostErrorException;
use App\Shop\Posts\Exceptions\PostNotFoundException;
use App\Shop\Posts\Repositories\Interfaces\PostRepositoryInterface;
use App\Shop\Posts\Transformations\PostTransformable;
use App\Shop\Tools\UploadableTrait;

class PostRepository extends BaseRepository implements PostRepositoryInterface
{
    use UploadableTrait, PostTransformable;
	/**
     * PostRepository constructor.
     * 
     * @param Post $dummy
     */
    public function __construct(Post $post)
    {
        parent::__construct($post);
        $this->model = $post;
    }

    /**
     * List all the Posts
     *
     * @param string $order
     * @param string $sort
     * @param array $except
     * @return \Illuminate\Support\Collection
     */
    public function listPosts(string $order = 'id', string $sort = 'desc', $except = []) : Collection
    {
        return $this->model->orderBy($order, $sort)->get()->except($except);
    }

    /**
     * Create Post
     *
     * @param array $params
     *
     * @return Post
     * @throws InvalidArgumentException
     */
    public function createPost(array $params) : Post
    {
        try {
        	return $this->createNewPost($params);
        } catch (QueryException $e) {
            throw new CreatePostErrorException($e->getMessage());
        }
    }

    /**
     * Update the dummy
     *
     * @param array $params
     * @return Post
     */
    public function updatePost(array $params) : Post
    {
        try {
            $id = $this->findOneOrFail($this->model->id);
            return $this->updateExistingPost($id, $params);
        } catch (QueryException $e) {
            throw new UpdatePostErrorException($e->getMessage());
        }
        
    }

    /**
     * @param int $id
     * 
     * @return Post
     * @throws ModelNotFoundException
     */
    public function findPostById(int $id) : Post
    {
        try {
            return $this->transformPost($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new PostNotFoundException($e->getMessage());
        }
    }

    /**
     * Delete a dummy
     *
     * @return bool
     */
    public function deletePost() : bool
    {
        $data = $this->findOneOrFail($this->model->id);
        if ($data->image != null) {
            $this->deleteOne($data->image);
        }
        return $this->model->where('id', $this->model->id)->delete();
    }

    public function searchPost(string $text = null) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchPost($text);
        } else {
            return $this->listPosts();
        }
    }
}