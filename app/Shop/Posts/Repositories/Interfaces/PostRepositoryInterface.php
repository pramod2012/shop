<?php

namespace App\Shop\Posts\Repositories\Interfaces;

use App\Shop\Posts\Post;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepositoryInterface;

interface PostRepositoryInterface extends BaseRepositoryInterface
{
    public function listPosts(string $order = 'id', string $sort = 'desc', $except = []) : Collection;

    public function createPost(array $params) : Post;

    public function updatePost(array $params) : Post;

    public function findPostById(int $id) : Post;
    
    public function deletePost() : bool;

    public function searchPost(string $text = null) : Collection;
}
