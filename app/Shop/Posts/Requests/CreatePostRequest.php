<?php

namespace App\Shop\Posts\Requests;

use App\Shop\Base\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreatePostRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required']
        ];
    }
}
