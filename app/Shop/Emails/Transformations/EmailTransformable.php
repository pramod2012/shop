<?php

namespace App\Shop\Emails\Transformations;

use App\Shop\Emails\Email;
use Illuminate\Support\Facades\Storage;
use App\Shop\Tools\UploadableTrait;

trait EmailTransformable
{
    use UploadableTrait;
    /**
     * Transform the email
     *
     * @param email $email
     * @return email
     */
    protected function transformEmail(Email $email)
    {
        $prod = new Email;
        $prod->id = (int) $email->id;
        $prod->name = $email->name;
        $prod->body = $email->body;
        $prod->image = asset("storage/$email->image");
        $prod->status = $email->status;
        $prod->feature = $email->feature;
        $prod->slug = $email->slug;
        $prod->order = $email->order;
        $prod->created_at = $email->created_at;
        $prod->updated_at = $email->updated_at;
        $prod->meta_title = $email->meta_title;
        $prod->meta_desc = $email->meta_desc;
        $prod->meta_keywords = $email->meta_keywords;

        return $prod;
    }

    protected function createNewEmail($params)
    {
        $prod = new Email;
        
        $prod->name = $params['name'];
        $prod->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                $image = $this->uploadOne($params['image'], 'emails');
                $prod->image = $image;
            }
        $prod->status = $params['status'];
        $prod->feature = $params['feature'];
        $prod->meta_title = $params['meta_title'];
        $prod->meta_desc = $params['meta_desc'];
        $prod->meta_keywords = $params['meta_keywords'];
        $prod->slug = $this->createSlug($params['name']);
        $prod->save();

        return $prod;
    }

    protected function updateExistingEmail($id, $params)
    {
        $id->name = $params['name'];
        $id->body = $params['body'];
        if (isset($params['image']) && ($params['image'])) {
                if ($params['image'] != null) {
                $this->deleteOne($id->image);
                }
                $image = $this->uploadOne($params['image'], 'emails');
                $id->image = $image;
            }
        $id->status = $params['status'];
        $id->feature = $params['feature'];
        $id->meta_title = $params['meta_title'];
        $id->meta_desc = $params['meta_desc'];
        $id->meta_keywords = $params['meta_keywords'];
        $id->save();

        return $id;
    }

    public function createSlug($title, $id = 0)
    {
        $slug = str_slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (! $allSlugs->contains('slug', $slug)){
            return $slug;
        }

        $i = 1;
        $is_contain = true;
        do {
            $newSlug = $slug . '-' . $i;
            if (!$allSlugs->contains('slug', $newSlug)) {
                $is_contain = false;
                return $newSlug;
            }
            $i++;
        } while ($is_contain);
    }
    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Email::select('slug')->where('slug', 'like', $slug.'%')
        ->where('id', '<>', $id)
        ->get();
    }
}
