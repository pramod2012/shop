<?php

namespace App\Shop\Emails\Repositories\Interfaces;

use App\Shop\Emails\Email;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepositoryInterface;

interface EmailRepositoryInterface extends BaseRepositoryInterface
{
    public function listEmails(string $order = 'id', string $sort = 'desc', $except = []) : Collection;

    public function createEmail(array $params) : Email;

    public function updateEmail(array $params) : Email;

    public function findEmailById(int $id) : Email;
    
    public function deleteEmail() : bool;

    public function searchEmail(string $text = null) : Collection;
}
