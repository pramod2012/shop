<?php

namespace App\Shop\Emails\Repositories;

use App\Shop\Emails\Email;
use Illuminate\Support\Collection;
use Jsdecena\Baserepo\BaseRepository;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Shop\Emails\Exceptions\CreateEmailErrorException;
use App\Shop\Emails\Exceptions\UpdateEmailErrorException;
use App\Shop\Emails\Exceptions\EmailNotFoundException;
use App\Shop\Emails\Repositories\Interfaces\EmailRepositoryInterface;
use App\Shop\Emails\Transformations\EmailTransformable;
use App\Shop\Tools\UploadableTrait;

class EmailRepository extends BaseRepository implements EmailRepositoryInterface
{
    use UploadableTrait, EmailTransformable;
	/**
     * EmailRepository constructor.
     * 
     * @param Email $dummy
     */
    public function __construct(Email $email)
    {
        parent::__construct($email);
        $this->model = $email;
    }

    /**
     * List all the Emails
     *
     * @param string $order
     * @param string $sort
     * @param array $except
     * @return \Illuminate\Support\Collection
     */
    public function listEmails(string $order = 'id', string $sort = 'desc', $except = []) : Collection
    {
        return $this->model->orderBy($order, $sort)->get()->except($except);
    }

    /**
     * Create Email
     *
     * @param array $params
     *
     * @return Email
     * @throws InvalidArgumentException
     */
    public function createEmail(array $params) : Email
    {
        try {
        	return $this->createNewEmail($params);
        } catch (QueryException $e) {
            throw new CreateEmailErrorException($e->getMessage());
        }
    }

    /**
     * Update the dummy
     *
     * @param array $params
     * @return Email
     */
    public function updateEmail(array $params) : Email
    {
        try {
            $id = $this->findOneOrFail($this->model->id);
            return $this->updateExistingEmail($id, $params);
        } catch (QueryException $e) {
            throw new UpdateEmailErrorException($e->getMessage());
        }
        
    }

    /**
     * @param int $id
     * 
     * @return Email
     * @throws ModelNotFoundException
     */
    public function findEmailById(int $id) : Email
    {
        try {
            return $this->transformEmail($this->findOneOrFail($id));
        } catch (ModelNotFoundException $e) {
            throw new EmailNotFoundException($e->getMessage());
        }
    }

    /**
     * Delete a dummy
     *
     * @return bool
     */
    public function deleteEmail() : bool
    {
        $data = $this->findOneOrFail($this->model->id);
        if ($data->image != null) {
            $this->deleteOne($data->image);
        }
        return $this->model->where('id', $this->model->id)->delete();
    }

    public function searchEmail(string $text = null) : Collection
    {
        if (!empty($text)) {
            return $this->model->searchEmail($text);
        } else {
            return $this->listEmails();
        }
    }
}