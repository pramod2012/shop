<?php

use App\Models\SiteConfig;

if (! function_exists('getSiteSettings')) {
    function getSiteSettings($id) {
        $config = SiteConfig::findorFail($id);
        return $config->config_value;
    }
}

?>