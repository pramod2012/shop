<?php


/**
 * Converts all special charaters and quotes to html codes
 * @param $data - string
 * @return string - formated value
 */

if (! function_exists('decode_string')) {
    function decode_string($string) {
        $content = '';
        $content = htmlspecialchars($string,ENT_QUOTES,'UTF-8');
        return trim($content);
     }
}

if (! function_exists('encode_string')) {
    function encode_string($string){
		$content = '';
		$content = htmlspecialchars_decode($string,ENT_QUOTES);
		return trim($content);
	}

}
if (! function_exists('_display_string')) {
    function _display_string ($string) {
        $content = '';
        $content = htmlspecialchars_decode($string,ENT_QUOTES);
        return $content;
    }
}
if (! function_exists('word_limiter')) {
    function word_limiter($phrase, $max_words) {
        $phrase_array = explode(' ',$phrase);
        if(count($phrase_array) > $max_words && $max_words > 0)
        $phrase = implode(' ',array_slice($phrase_array, 0, $max_words)).' ...';
        return $phrase;
    }
}
if (! function_exists('humanFilesize')) {
    function humanFilesize($size, $precision = 2) {
        $units = array('B','kB','MB','GB','TB','PB','EB','ZB','YB');
        $step = 1024;
        $i = 0;
        while (($size / $step) > 0.9) {
            $size = $size / $step;
            $i++;
        }
        return round($size, $precision).$units[$i];
    }
}


?>