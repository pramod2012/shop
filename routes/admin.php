<?php

Route::namespace('Admin')->group(function () {
    Route::get('admin/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('admin/login', 'LoginController@login')->name('admin.login');
    Route::get('admin/logout', 'LoginController@logout')->name('admin.logout');
});
Route::group(['prefix' => 'admin', 'middleware' => ['employee'], 'as' => 'admin.' ], function () {
    Route::namespace('Admin')->group(function () {
        Route::group(['middleware' => ['role:admin|superadmin|clerk, guard:employee']], function () {
            Route::get('/', 'DashboardController@index')->name('dashboard');
            Route::namespace('Products')->group(function () {
                Route::resource('products', 'ProductController');
                Route::get('remove-image-product', 'ProductController@removeImage')->name('product.remove.image');
                Route::get('remove-image-thumb', 'ProductController@removeThumbnail')->name('product.remove.thumb');
            });
            Route::namespace('Customers')->group(function () {
                Route::resource('customers', 'CustomerController');
                Route::resource('customers.addresses', 'CustomerAddressController');
            });
            Route::get('/settings', 'SettingController@index')->name('settings');
            Route::post('/settings', 'SettingController@update')->name('settings.update');
            Route::namespace('Categories')->group(function () {
                Route::resource('categories', 'CategoryController');
                Route::get('remove-image-category', 'CategoryController@removeImage')->name('category.remove.image');
            });
            Route::namespace('Orders')->group(function () {
                Route::resource('orders', 'OrderController');
                Route::resource('order-statuses', 'OrderStatusController');
                Route::get('orders/{id}/invoice', 'OrderController@generateInvoice')->name('orders.invoice.generate');
                Route::get('ordersAll/invoice', 'OrderController@generateAllInvoice')->name('orders.all.generate');
                Route::get('export', 'OrderController@fileExport')->name('orders.export');
            });
            Route::resource('addresses', 'Addresses\AddressController');
            Route::resource('countries', 'Countries\CountryController');
            Route::resource('countries.provinces', 'Provinces\ProvinceController');
            Route::resource('countries.provinces.cities', 'Cities\CityController');
            Route::resource('couriers', 'Couriers\CourierController');
            Route::resource('attributes', 'Attributes\AttributeController');
            Route::resource('attributes.values', 'Attributes\AttributeValueController');
            Route::resource('brands', 'Brands\BrandController');
            Route::resource('sliders', 'Sliders\SliderController');
            Route::resource('partners', 'Partners\PartnerController');
            Route::post('sliders/save_orders', 'Sliders\SliderController@saveOrders');
            Route::post('partners/save_orders', 'Partners\PartnerController@saveOrders');
            Route::resource('promotions', 'Promotions\PromotionController');
            Route::post('promotions/save_orders', 'Promotions\PromotionController@saveOrders');
            Route::resource('flashes', 'Flashes\FlashController');
            Route::post('flashes/save_orders', 'Flashes\FlashController@saveOrders');
            Route::post('categories/save_orders', 'Categories\CategoryController@saveOrders');
            Route::resource('tags', 'Tags\TagController');
            Route::post('tags/save_orders', 'Tags\TagController@saveOrders');
            Route::resource('mcategories', 'Mcategories\McategoryController');
            Route::post('mcategories/save_orders', 'Mcategories\McategoryController@saveOrders');
            Route::resource('posts', 'Posts\PostController');
            Route::post('posts/save_orders', 'Posts\PostController@saveOrders');
            Route::resource('authors', 'Authors\AuthorController');
            Route::post('authors/save_orders', 'Authors\AuthorController@saveOrders');
            Route::resource('emails', 'Emails\EmailController');
            Route::post('emails/save_orders', 'Emails\EmailController@saveOrders');
            Route::resource('ratings', 'Ratings\RatingController');
            Route::post('ratings/save_orders', 'Ratings\RatingController@saveOrders');

        });
        Route::group(['middleware' => ['role:admin|superadmin, guard:employee']], function () {
            Route::resource('employees', 'EmployeeController');
            Route::get('employees/{id}/profile', 'EmployeeController@getProfile')->name('employee.profile');
            Route::put('employees/{id}/profile', 'EmployeeController@updateProfile')->name('employee.profile.update');
            Route::resource('roles', 'Roles\RoleController');
            Route::resource('permissions', 'Permissions\PermissionController');
        });
    });
});

