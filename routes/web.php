<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/**
 * Admin routes
 */
require 'admin.php';
/**
 * Frontend routes
 */
Auth::routes();
Route::namespace('Auth')->group(function () {
    Route::get('cart/login', 'CartLoginController@showLoginForm')->name('cart.login');
    Route::post('cart/login', 'CartLoginController@login')->name('cart.login');
    Route::get('logout', 'LoginController@logout');
});

Route::namespace('Front')->group(function () {
    Route::get('/', 'HomeController@index')->name('index');
    Route::group(['middleware' => ['auth', 'web']], function () {

        Route::namespace('Payments')->group(function () {
            Route::get('bank-transfer', 'BankTransferController@index')->name('bank-transfer.index');
            Route::post('bank-transfer', 'BankTransferController@store')->name('bank-transfer.store');
        });

        Route::namespace('Addresses')->group(function () {
            Route::resource('country.state', 'CountryStateController');
            Route::resource('state.city', 'StateCityController');
        });

        Route::get('accounts', 'AccountsController@index')->name('accounts');
        Route::get('checkout', 'CheckoutController@index')->name('checkout.index');
        Route::post('checkout', 'CheckoutController@store')->name('checkout.store');
        Route::get('checkout/execute', 'CheckoutController@executePayPalPayment')->name('checkout.execute');
        Route::post('checkout/execute', 'CheckoutController@charge')->name('checkout.execute');
        Route::get('checkout/cancel', 'CheckoutController@cancel')->name('checkout.cancel');
        Route::get('checkout/success', 'CheckoutController@success')->name('checkout.success');
        Route::resource('customer.address', 'CustomerAddressController');
    });
    Route::resource('cart', 'CartController')->only('index','store','destroy','update');
    Route::resource('wishlist', 'WishlistController');
    Route::get("category/{slug}", 'CategoryController@getCategory')->name('category');
    Route::get("product/search", 'CategoryController@search')->name('search');
    Route::get("category-list/{slug}", 'CategoryController@getListCategory')->name('category.list');
    Route::get("search", 'ProductController@search')->name('search.product');
    Route::get("{product}", 'ProductController@show')->name('product');

    Route::get("blog/list", 'BlogController@blog')->name('blog');
    Route::get('blog/{slug}', 'BlogController@blogShow')->name('blog.show');
    Route::get('blog/category/{slug}', 'BlogController@findByCategory')->name('blog.category');
    Route::get('blog/tag/{slug}', 'BlogController@findByTag')->name('blog.tag');
    Route::get('blog/author/{slug}', 'BlogController@findByAuthor')->name('blog.author');
    Route::get('page/{slug}', 'BlogController@pageShow')->name('page.show');
});

Route::post('rating/store', 'HomeController@ratingStore')->name('rating.store');

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('login/{provider}/callback','Auth\LoginController@handleProviderCallback');
Route::get('api1/cartCount', 'HomeController@cartCount');
