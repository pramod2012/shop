/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	config.filebrowserBrowseUrl = '/bower_components/ckeditor/kcfinder-3.12/browse.php?opener=ckeditor&type=files';
    config.filebrowserImageBrowseUrl = '/bower_components/ckeditor/kcfinder-3.12/browse.php?opener=ckeditor&type=images';
    config.filebrowserFlashBrowseUrl = '/bower_components/ckeditor/kcfinder-3.12/browse.php?opener=ckeditor&type=flash';
    config.filebrowserUploadUrl = '/bower_components/ckeditor/kcfinder-3.12/upload.php?opener=ckeditor&type=files';
    config.filebrowserImageUploadUrl = '/bower_components/ckeditor/kcfinder-3.12/upload.php?opener=ckeditor&type=images';
    config.filebrowserFlashUploadUrl = '/bower_components/ckeditor/kcfinder/upload.php?opener=ckeditor&type=flash';
};
