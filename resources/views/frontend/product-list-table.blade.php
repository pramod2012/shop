<div class="shopping-cart__table">
                                    <div class="shopping-cart-light">
                                        <div class="shopping-cart-row">
                                            <div class="cart-product">Name</div>
                                            <div class="cart-price">Price</div>
                                            <div class="cart-quantity">Quantity</div>
                                            <div class="cart-total">Total</div>
                                            <div class="cart-action"> Action</div>
                                        </div>
                                    </div>
                                    <div class="shopping-cart-body">
                                   @foreach($cartItems as $cartItem)
                                        <div class="shopping-cart-row">
                                            <div class="cart-product">
                                                <div class="ps-product--mini-cart">
                                                	<a href="{{ route('product', [$cartItem->product->slug]) }}">
                                                	@if(isset($cartItem->cover))
                                                		<img class="ps-product__thumbnail" src="{{$cartItem->cover}}" alt="alt" />
                                                	@else
                                                    	<img src="https://placehold.it/120x120" alt="" class="img-responsive img-thumbnail">
                                                	@endif
                                                	</a>
                                                    <div class="ps-product__content">
                                                        <h5><a class="ps-product__name" href="#">{{ $cartItem->name }}</a></h5>
                                           @if($cartItem->options->has('combination'))
                                                <div style="margin-bottom:5px;">
                                                @foreach($cartItem->options->combination as $option)
                                                    <small class="label label-primary">{{$option['value']}}</small>
                                                @endforeach
                                                </div>
                                            @endif
                                                        <p class="ps-product__meta">Price: <span class="ps-product__price">{{config('cart.currency')}} {{ number_format($cartItem->price, 2) }}</span></p>
                                                        <div class="def-number-input number-input safari_only">
                                                            <button class="minus" onclick="this.parentNode.querySelector('input[type=number]').stepDown()"><i class="icon-minus"></i></button>
                                                            <input class="quantity" min="0" name="quantity" value="{{ $cartItem->qty }}" type="number" />
                                                            <button class="plus" onclick="this.parentNode.querySelector('input[type=number]').stepUp()"><i class="icon-plus"></i></button>
                                                        </div><span class="ps-product__total">Total: <span>{{config('cart.currency')}} {{ number_format(($cartItem->qty*$cartItem->price), 2) }} </span></span>
                                                    </div>
                                                    <div class="ps-product__remove">
                                                    	<form action="{{ route('cart.destroy', $cartItem->rowId) }}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="delete">
                                                <button onclick="return confirm('Are you sure?')"><i class="icon-trash2"></i></button>
                                            </form>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="cart-price"><span class="ps-product__price">{{config('cart.currency')}} {{ number_format($cartItem->price) }}</span>
                                            </div>
                                            <div class="cart-quantity">
                                                <div class="def-number-input number-input safari_only">
                                                    <button class="minus" onclick="this.parentNode.querySelector('input[type=number]').stepDown()"><i class="icon-minus"></i></button>
                                                    <input class="quantity" min="0" name="quantity" value="{{ $cartItem->qty }}" type="number" />
                                                    <button class="plus" onclick="this.parentNode.querySelector('input[type=number]').stepUp()"><i class="icon-plus"></i></button>
                                                </div>
                                            </div>
                                            <div class="cart-total"> <span class="ps-product__total">{{config('cart.currency')}}{{ number_format(($cartItem->qty*$cartItem->price), 2) }} </span>
                                            </div>

                                            <div class="cart-action">
                                            	<form action="{{ route('cart.destroy', $cartItem->rowId) }}" method="post">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="delete">
                                                <button onclick="return confirm('Are you sure?')"><i class="icon-trash2"></i></button>
                                            </form>
                                             
                                         </div>
                                        </div>
                                      @endforeach
                                        
                                    </div>
                                </div>