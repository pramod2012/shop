@section('title')
    {{$post->name}} | {{config('settings.site_title')}}
@endsection

@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
        <section class="section--single-post section--single-post-sidebar">
            <div class="blog__content post__category">
                <div class="container">
                    <div class="blog__list">
                        <ul class="blog__type">
@foreach(\App\Shop\Mcategories\Mcategory::orderBy('order','asc')->where('status',1)->get() as $value)
                            <li><a href="{{route('blog.category',$value->slug)}}">{{$value->name}}</a></li>
@endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container">
            	
                <div class="row">
                    <div class="col-12 col-lg-9">
                        <div class="post__header">
                            <div class="ps-breadcrumb">
                                <div class="container">
                                    <ul class="ps-breadcrumb__list">
                                        <li class="active"><a href="{{route('index')}}">Home</a></li>
                                        <li class="active"><a href="javascript:void(0);">{{$post->name}}</a></li>
                                    </ul>
                                </div>
                            </div>
                            <h2 class="post__title">{{$post->name}}</h2>
                            <div class="post__archives">
                            	<span class="archive__item blog__type">
                            		<i class="icon-folder"></i>{{$post->mcategory->name}}
                            	</span>
                            	<span class="archive__item">
                            		<i class="icon-bubbles"></i>15 comments
                            	</span>
                            	<span class="archive__item">{{$post->created_at->format('F j, Y')}} by <span class="blog__auth">{{$post->author->name}}</span>
                            	</span>
                                
                            </div>
                        </div>
                        <div class="post__content">
                            <div class="post__blog">
                            	<img src="{{asset('storage/'.$post->image)}}" alt>
                                <p>
                                	<?php
$str = $post->body;
echo html_entity_decode($str);
?> 
                                </p>
                                
                                
                                 @if($post->tags->count() > 0)
                                <div class="post__tags"><span class="tags__title">Tags: </span>
                                    <ul>
                                    	@foreach($post->tags as $value)
                                        <a href="{{route('blog.tag',$value->slug)}}"><li>{{$value->name}}</li></a>
                                        @endforeach
                                    </ul>
                                </div>@endif
                                <div class="post__auth">
                                    <div class="auth__avatar"><img src="{{@asset('storage'.$post->author->image)}}" alt></div>
                                    <div class="auth__content">
                                        <h4 class="auth__name">{{$post->author->name}}</h4>
                                        <p class="auth__des">{!! $post->author->body !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-3">
                        <div class="blog--widget">
                            <div class="widget__search">
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="Search in post...">
                                    <div class="input-group-append"><i class="icon-magnifier"></i></div>
                                </div>
                            </div>
                            
                            <div class="widget__posts">
                                <div class="widget__title">LATEST POSTS</div>
                                <ul class="post-list">
                                    @foreach($post_related as $value)
                                    <li>
                                    	<a href="{{route('blog.show',$value->slug)}}">
                                    		<img src="{{asset('storage/'.$value->image)}}" alt="alt" />
                                            <div class="post__content">
                                                <p class="post__date">{{$post->created_at->format('F j, Y')}} by <span>{{$post->author->name}}</span></p>
                                                <h5 class="post__title">{{$value->name}}</h5>
                                            </div>
                                        </a>
                                    </li>@endforeach
                                </ul>
                            </div>
                            
                            
                            
                            
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="ps--comments">
                <div class="container">
                    <h5 class="comment__title">5 Comments</h5>
                    <ul class="comment__list">
                        <li class="comment__item">
                            <div class="item__avatar"><img src="img/blogs/comment_avatar1.png" alt="alt" /></div>
                            <div class="item__content">
                                <div class="item__name">Carmen Merav</div>
                                <div class="item__date">- June 14, 2020</div>
                                <p class="item__des">I have gotten at least 50 timest 50 times the value from Farmart. I would like to personally thank you for your outstanding product. Farmart is great. It's exactly what I've been looking for.</p><a href="javascript:void(0);">REPLY </a>
                                <ul class="comment__list comment__sub">
                                    <li class="comment__item">
                                        <div class="item__avatar"><img src="img/blogs/comment_no_avatar.png" alt="alt" /></div>
                                        <div class="item__content">
                                            <div class="item__name">Paula Jordyn</div>
                                            <div class="item__date">- June 25, 2020</div>
                                            <p class="item__des">If you aren't sure, always go for Farmart. We're loving it. Farmart impressed me on multiple levels</p><a href="javascript:void(0);">REPLY</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="comment__item">
                            <div class="item__avatar"><img src="img/blogs/comment_avatar2.png" alt="alt" /></div>
                            <div class="item__content">
                                <div class="item__name">Abbey Vasu</div>
                                <div class="item__date">- June 14, 2020</div>
                                <p class="item__des">Farmart is great. Farmart is the most valuable business resource we have EVER purchased. I have gotten at least 50 times the value from Farmart. I just can't get enough of Farmart. I want to get a T-Shirt with Farmart on it so I can show it off to everyone.</p><a href="javascript:void(0);">REPLY </a>
                            </div>
                        </li>
                        <li class="comment__item">
                            <div class="item__avatar"><img src="img/blogs/comment_no_avatar.png" alt="alt" /></div>
                            <div class="item__content">
                                <div class="item__name">Avimelekh Tecla</div>
                                <div class="item__date">- June 10, 2020</div>
                                <p class="item__des">If you aren't sure, always go for Farmart. Not able to tell you how happy I am with Farmart. Definitely worth the investment. We've used Farmart foor the last five years.</p><a href="javascript:void(0);">REPLY </a>
                            </div>
                        </li>
                        <li class="comment__item">
                            <div class="item__avatar"><img src="img/blogs/comment_no_avatar.png" alt="alt" /></div>
                            <div class="item__content">
                                <div class="item__name">Poornima Jayda</div>
                                <div class="item__date">- May 14, 2020</div>
                                <p class="item__des">If you aren't sure, always go for Farmart. We're loving it. Farmart impressed me on multiple levels</p><a href="javascript:void(0);">REPLY </a>
                            </div>
                        </li>
                    </ul>
                    <h5 class="comment__title">Leave A Reply</h5>
                    <p class="comment__des">Your email will not be published. All fields are required.</p>
                    <form>
                        <div class="form-row">
                            <div class="col-12 form-group--block">
                                <textarea class="form-control" placeholder="Content"></textarea>
                            </div>
                            <div class="col-12 col-lg-4 form-group--block pt-4">
                                <input class="form-control" type="text" placeholder="Name">
                            </div>
                            <div class="col-12 col-lg-4 form-group--block pt-4">
                                <input class="form-control" type="email" placeholder="Email">
                            </div>
                            <div class="col-12 col-lg-4 form-group--block pt-4">
                                <input class="form-control" type="text" placeholder="Website">
                            </div>
                            <div class="col-12 col-lg-4 form-group--block">
                                <button class="btn ps-button">Post Comment</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </section>
        <div class="modal fade" id="popupQuickview" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl ps-quickview">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid quickview-body">
                            <div class="row">
                                <div class="col-12 col-lg-5">
                                    <div class="owl-carousel" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-item-xl="1" data-owl-duration="1000" data-owl-mousedrag="on">
                                        <div class="quickview-carousel"><img class="carousel__thumbnail" src="img/products/01-Fresh/01_1a.jpg" alt="alt" /></div>
                                        <div class="quickview-carousel"><img class="carousel__thumbnail" src="img/products/01-Fresh/01_2a.jpg" alt="alt" /></div>
                                        <div class="quickview-carousel"><img class="carousel__thumbnail" src="img/products/01-Fresh/01_4a.jpg" alt="alt" /></div>
                                        <div class="quickview-carousel"><img class="carousel__thumbnail" src="img/products/01-Fresh/01_9a.jpg" alt="alt" /></div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-7">
                                    <div class="quickview__product">
                                        <div class="product__header">
                                            <div class="product__title">Hovis Farmhouse Soft White Bread</div>
                                            <div class="product__meta">
                                                <div class="product__rating">
                                                    <select class="rating-stars">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4" selected="selected">4</option>
                                                        <option value="5">5</option>
                                                    </select><span>4 customer reviews</span>
                                                </div>
                                                <div class="product__code"><span>SKU: </span>#VEG20938</div>
                                            </div>
                                        </div>
                                        <div class="product__content">
                                            <div class="product__price"><span class="sale">$5.49</span><span class="price">$6.90</span><span class="off">25% Off</span></div>
                                            <p class="product__unit">300g</p>
                                            <div class="alert__success">Availability: <span>34 in stock</span></div>
                                            <ul>
                                                <li>Type: Organic</li>
                                                <li>MFG: Jun 4, 2020</li>
                                                <li>LIFE: 30 days</li>
                                            </ul>
                                        </div>
                                        <div class="product__action">
                                            <label>Quantity: </label>
                                            <div class="def-number-input number-input safari_only">
                                                <button class="minus" onclick="this.parentNode.querySelector('input[type=number]').stepDown()"><i class="icon-minus"></i></button>
                                                <input class="quantity" min="0" name="quantity" value="1" type="number">
                                                <button class="plus" onclick="this.parentNode.querySelector('input[type=number]').stepUp()"><i class="icon-plus"></i></button>
                                            </div>
                                            <button class="btn product__addcart"> <i class="icon-cart"></i>Add to cart</button>
                                            <button class="btn button-icon icon-md"><i class="icon-repeat"></i></button>
                                            <button class="btn button-icon icon-md"><i class="icon-heart"></i></button>
                                        </div>
                                        <div class="product__footer">
                                            <div class="ps-social--share"><a class="ps-social__icon facebook" href="#"><i class="fa fa-thumbs-up"></i><span>Like</span><span class="ps-social__number">0</span></a><a class="ps-social__icon facebook" href="#"><i class="fa fa-facebook-square"></i><span>Like</span><span class="ps-social__number">0</span></a><a class="ps-social__icon twitter" href="#"><i class="fa fa-twitter"></i><span>Like</span></a><a class="ps-social__icon" href="#"><i class="fa fa-plus-square"></i><span>Like</span></a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="popupAddToCart" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl ps-addcart">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="alert__success"><i class="icon-checkmark-circle"></i> "Morrisons The Best Beef Topside" successfully added to you cart. <a href="shopping-cart.html">View cart(3)</a></div>
                            <hr>
                            <h3 class="cart__title">CUSTOMERS WHO BOUGHT THIS ALSO BOUGHT:</h3>
                            <div class="cart__content">
                                <div class="owl-carousel" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="false" data-owl-dots="true" data-owl-item="4" data-owl-item-xs="2" data-owl-item-sm="2" data-owl-item-md="2" data-owl-item-lg="4" data-owl-item-xl="4" data-owl-duration="1000" data-owl-mousedrag="on">
                                    <div class="cart-item">
                                        <div class="ps-product--standard"><a href="product-default.html"><img class="ps-product__thumbnail" src="img/products/01-Fresh/01_35a.jpg" alt="alt" /></a>
                                            <div class="ps-product__content">
                                                <p class="ps-product__type"><i class="icon-store"></i>Farmart</p><a href="product-default.html">
                                                    <h5 class="ps-product__name">Extreme Budweiser Light Can</h5>
                                                </a>
                                                <p class="ps-product__unit">500g</p>
                                                <div class="ps-product__rating">
                                                    <select class="rating-stars">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4" selected="selected">4</option>
                                                        <option value="5">5</option>
                                                    </select><span>(4)</span>
                                                </div>
                                                <p class="ps-product-price-block"><span class="ps-product__sale">$8.90</span><span class="ps-product__price">$9.90</span><span class="ps-product__off">15% Off</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cart-item">
                                        <div class="ps-product--standard"><a href="product-default.html"><img class="ps-product__thumbnail" src="img/products/01-Fresh/01_16a.jpg" alt="alt" /></a>
                                            <div class="ps-product__content">
                                                <p class="ps-product__type"><i class="icon-store"></i>Karery Store</p><a href="product-default.html">
                                                    <h5 class="ps-product__name">Honest Organic Still Lemonade</h5>
                                                </a>
                                                <p class="ps-product__unit">100g</p>
                                                <div class="ps-product__rating">
                                                    <select class="rating-stars">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5" selected="selected">5</option>
                                                    </select><span>(14)</span>
                                                </div>
                                                <p class="ps-product-price-block"><span class="ps-product__price-default">$1.99</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cart-item">
                                        <div class="ps-product--standard"><a href="product-default.html"><img class="ps-product__thumbnail" src="img/products/01-Fresh/01_12a.jpg" alt="alt" /></a>
                                            <div class="ps-product__content">
                                                <p class="ps-product__type"><i class="icon-store"></i>John Farm</p><a href="product-default.html">
                                                    <h5 class="ps-product__name">Natures Own 100% Wheat</h5>
                                                </a>
                                                <p class="ps-product__unit">100g</p>
                                                <div class="ps-product__rating">
                                                    <select class="rating-stars">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select><span>(0)</span>
                                                </div>
                                                <p class="ps-product-price-block"><span class="ps-product__price-default">$4.49</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cart-item">
                                        <div class="ps-product--standard"><a href="product-default.html"><img class="ps-product__thumbnail" src="img/products/01-Fresh/01_15a.jpg" alt="alt" /></a>
                                            <div class="ps-product__content">
                                                <p class="ps-product__type"><i class="icon-store"></i>Farmart</p><a href="product-default.html">
                                                    <h5 class="ps-product__name">Avocado, Hass Large</h5>
                                                </a>
                                                <p class="ps-product__unit">300g</p>
                                                <div class="ps-product__rating">
                                                    <select class="rating-stars">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3" selected="selected">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                    </select><span>(6)</span>
                                                </div>
                                                <p class="ps-product-price-block"><span class="ps-product__sale">$6.99</span><span class="ps-product__price">$9.90</span><span class="ps-product__off">25% Off</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cart-item">
                                        <div class="ps-product--standard"><a href="product-default.html"><img class="ps-product__thumbnail" src="img/products/06-SoftDrinks-TeaCoffee/06_3a.jpg" alt="alt" /></a>
                                            <div class="ps-product__content">
                                                <p class="ps-product__type"><i class="icon-store"></i>Sun Farm</p><a href="product-default.html">
                                                    <h5 class="ps-product__name">Kevita Kom Ginger</h5>
                                                </a>
                                                <p class="ps-product__unit">200g</p>
                                                <div class="ps-product__rating">
                                                    <select class="rating-stars">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4" selected="selected">4</option>
                                                        <option value="5">5</option>
                                                    </select><span>(6)</span>
                                                </div>
                                                <p class="ps-product-price-block"><span class="ps-product__sale">$4.90</span><span class="ps-product__price">$3.99</span><span class="ps-product__off">15% Off</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    @include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection