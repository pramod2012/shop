@if($products->count() > 0)
                                <div class="section-shop--listing">
                                    
@foreach($products as $product)
                                <div class="product-item">
                                        <div class="ps-product--list">
                                            <div class="ps-product__left">
                                                <div class="ps-product__extent"><a href="{{route('product',$product->slug)}}">
                                                	<img class="ps-product__thumbnail" src="{{asset('storage/'.$product->cover)}}" alt="alt" /></a>

                                                    <div class="ps-product__footer">
                                                        <div class="def-number-input number-input safari_only">
                                                            <button class="minus" onclick="this.parentNode.querySelector('input[type=number]').stepDown()"><i class="icon-minus"></i></button>
                                                            <input class="quantity" min="0" name="quantity{{$product->id}}" value="1" type="number" />
                                                            <button class="plus" onclick="this.parentNode.querySelector('input[type=number]').stepUp()"><i class="icon-plus"></i></button>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="ps-product__content">
                                                    <div class="ps-product__type">
                                                    	<i class="icon-store"></i>
                                                    Royal Agro Farm
                                                </div>
                                                <a class="ps-product__name" href="{{route('product',$product->slug)}}">{{$product->name}}
                                                </a>
                            <div class="ps-product__rating">
                                                <select class="rating-stars">
                            <?php $rating_avg = number_format($product->ratings()->avg('rating'), 0); ?>
                            <option value="1" @if($rating_avg == 1) selected @endif>1</option>
                            <option value="2" @if($rating_avg == 2) selected @endif>2</option>
                            <option value="3" @if($rating_avg == 3) selected @endif>3</option>
                            <option value="4" @if($rating_avg == 4) selected @endif>4</option>
                            <option value="5" @if($rating_avg == 5) selected @endif>5</option>
                        </select>
                        <span>({{$product->ratings()->count()}})</span>
                                            </div>
                                                    
                                                    <div class="ps-product__des">
                                                        <p>{!! substr($product->description,0,30) !!}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ps-product__right">
                                                <div class="ps-product__price">{{config('settings.currency_code')}} {{number_format($product->price)}} @if($product->weight > 0) per {{number_format($product->weight)}} {{$product->mass_unit}} @endif</div>
                                                <div class="ps-product__unit">{{$product->quantity}} in Stock</div>
                                                
                                                <button class="ps-product__addcart" type="button" data-product="{{$product->id}}" id="{{'btn_'.$product->id}}" onclick="addToCart(this)"><i class="icon-cart"></i>Add to cart</button>
                                                <div class="ps-product__box">
                                                    <a class="ps-product__wishlist" data-product1="{{$product->id}}" id="{{'btn___'.$product->id}}" onclick="addToCart1(this)" href="">Wishlist</a>
                                                    
                                                    <a id="" class="ps-product__compare" href="{{route('product',$product->slug)}}">View</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        @endforeach      
                                    
                                </div>
                    @endif