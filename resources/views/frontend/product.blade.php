@section('title')
    {{$product->name}}
@endsection
@section('meta-tags')
<meta property="og:title" content="{{$product->name}}" />
@if($product->description == !null)
<meta name="description" content="<?php
$str = $product->description;
echo html_entity_decode($str);
?>">@endif
<meta property='og:image' content="{{asset('storage/'.$product->cover)}}"/>
@if($product->description == !null)
<meta property='og:description' content="<?php
$str = $product->description;
echo html_entity_decode($str);
?>"/>@endif
<meta property='og:url' content="{{app('request')->url()}}">
<meta property="og:type" content="website"/>
<meta property="og:site_name" content="{{app('request')->url()}}"/>
<meta name="twitter:card" content="summary_large_image" />
@if($product->title == !null)
<meta name="twitter:title" content="{{$product->title}}" />@endif
@if($product->description == !null)
<meta name="twitter:description" content="<?php
$str = $product->description;
echo html_entity_decode($str);
?>" />@endif
<meta name="twitter:image" content="{{asset('storage/'.$product->cover)}}" />
@endsection

@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="ps-breadcrumb__list">
                    <li class="active"><a href="{{route('index')}}">Home</a></li>
                    <li class="active"><a href="{{route('category',$category->slug)}}">{{$category->name}}</a></li>
                    <li><a href="javascript:void(0);">{{$product->name}}</a></li>
                </ul>
            </div>
        </div>
        <section class="section--product-type">
            <div class="container">
                <div class="product__header">
                    <h3 class="product__name">{{$product->name}}</h3>
                    <div class="row">
                        <div class="col-12 col-lg-7 product__code">
                        <select class="rating-stars">
                            <?php $rating_avg = number_format($product->ratings()->avg('rating'), 0); ?>
                            <option value="1" @if($rating_avg == 1) selected @endif>1</option>
                            <option value="2" @if($rating_avg == 2) selected @endif>2</option>
                            <option value="3" @if($rating_avg == 3) selected @endif>3</option>
                            <option value="4" @if($rating_avg == 4) selected @endif>4</option>
                            <option value="5" @if($rating_avg == 5) selected @endif>5</option>
                        </select>
                            <span class="product__review">{{number_format($product->ratings()->avg('rating'), 1)}}   Customer Review ( {{$product->ratings()->count()}} )</span><span class="product__id">SKU: <span>#{{$product->sku}}</span></span>
                        </div>
                        
                    </div>
                </div>
                <div class="product__detail">
                    <div class="row">
                        <div class="col-12 col-lg-9">
                            <div class="ps-product--detail">
                                <div class="row">
                                    <div class="col-12 col-lg-6">
                                        <div class="ps-product__variants">
                                            <div class="ps-product__gallery">
                                                <div class="ps-gallery__item active">
                                                    <img src="{{asset('storage/'.$product->cover)}}" alt="alt" />
                                                </div>
                                @foreach($product->images as $image)
                                                <div class="ps-gallery__item">
                                                    <img src="{{asset('storage/'.$image->src)}}" alt="alt" />
                                                </div>
                                            @endforeach
                                            </div>
                                            <div class="ps-product__thumbnail">
                                                <div class="ps-product__zoom">
                                                    <a target="_blank" href="{{asset('storage/'.$product->cover)}}">
                                                    <img id="ps-product-zoom" src="{{asset('storage/'.$product->cover)}}" alt="alt" />
                                                    </a>
                                                    
                                                    <ul class="ps-gallery--poster" id="ps-lightgallery-videos" data-video-url="#">
                                                        <li data-html="#video-play"><span></span><i class="fa fa-play-circle"></i></li>
                                                    </ul>
                                                </div>
                                                <div style="display:none;" id="video-play">
                                                    <video class="lg-video-object lg-html5" controls="controls" preload="none">
                                                        <source src="#" type="video/mp4" />Your browser does not support HTML5 video.
                                                    </video>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-6">
                                        <div class="ps-product__sale"><span class="price-sale">{{config('settings.currency_code')}} {{number_format($product->price)}} @if($product->weight > 0) per {{number_format($product->weight)}} {{$product->mass_unit}} @endif</span>
                                            @if($product->sale_price == !null || $product->sale_price == !0)
                                            <span class="price">{{config('settings.currency_code')}} {{number_format($product->sale_price)}}</span>@endif
                                            @if($product->discount == !null)
                                            <span class="ps-product__off">{{number_format(($product->discount)/($product->sale_price) * 100, 2, '.', '') }}% Off</span>@endif
                                        </div>
                                        <div class="ps-product__unit">{{$product->views}} views</div>
                                        <div class="ps-product__avai alert__success">Availability: <span>{{$product->quantity}} in stock</span>
                                        </div>
                                        <div class="ps-product__info">
                                            <ul class="ps-list--rectangle">
                                                <li></li>
                                                
                                            </ul>
                                        </div>
                                        
                                        <div class="ps-product__shopping">
                                            <div class="ps-product__quantity">
                                                <label>Quantity: </label>
                                                <div class="def-number-input number-input safari_only">
                                                    <button class="minus" onclick="this.parentNode.querySelector('input[type=number]').stepDown()"><i class="icon-minus"></i></button>
                                                    <input class="quantity" min="0" name="quantity{{$product->id}}" value="1" type="number" />
                                                    <button class="plus" onclick="this.parentNode.querySelector('input[type=number]').stepUp()"><i class="icon-plus"></i></button>
                                                </div>
                                            </div>
                                            <button class="ps-product__addcart ps-button" data-product="{{$product->id}}" id="{{'btn_'.$product->id}}" onclick="addToCart(this)">
                                                <i class="icon-cart"></i>Add to cart
                                            </button>
                                            <a class="ps-product__icon" data-product1="{{$product->id}}" id="{{'btn___'.$product->id}}" onclick="addToCart1(this)" href="">
                                                <i class="icon-heart"></i>
                                            </a>

                                        </div>
                                        <div class="ps-product__category">
                                            <ul>@if($product->brand_id == !null)
                                                <li>Brand: <a class='text-success'>{{$product->brand->name}}</a></li>
                                                @endif
                                            </ul>
                                        </div>
                                        <div class="ps-product__footer">
                                            <a class="ps-product__shop" href="{{route('index')}}">
                                                <i class="icon-store"></i>
                                                <span>Store</span>
                                            </a>
                                            <button class="ps-product__addcart ps-button" data-product2="{{$product->id}}" id="{{'btn_2'.$product->id}}" onclick="addToCart2(this)">
                                                <i class="icon-cart"></i>Add to cart
                                            </button>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-3">
                            <div class="ps-product--extention">
                                <div class="extention__block">
                                    <div class="extention__item">
                                        <div class="extention__icon"><i class="icon-truck"></i></div>
                                        <div class="extention__content"> <b class="text-black">Free Shipping </b>apply to all orders over <span class="text-success">$100</span></div>
                                    </div>
                                </div>
                                <div class="extention__block">
                                    <div class="extention__item">
                                        <div class="extention__icon"><i class="icon-leaf"></i></div>
                                        <div class="extention__content">Guranteed <b class="text-black">100% Organic </b>from natural farmas </div>
                                    </div>
                                </div>
                                <div class="extention__block">
                                    <div class="extention__item border-none">
                                        <div class="extention__icon"><i class="icon-repeat-one2"></i></div>
                                        <div class="extention__content"> <b class="text-black">1 Day Returns </b>if you change your mind</div>
                                    </div>
                                </div>
                                <div class="extention__block extention__contact">
                                    <p> <span class="text-black">Hotline : </span></p>
                                    <h4 class="extention__phone">{{config('settings.phone')}}</h4>
                                </div>
                                <p class="extention__footer">Become a Vendor? <a href="{{url('register')}}">Register now</a></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product__content">
                    <ul class="nav nav-pills" role="tablist" id="productTabDetail">
                        <li class="nav-item"><a class="nav-link active" id="description-tab" data-toggle="tab" href="#description-content" role="tab" aria-controls="description-content" aria-selected="true">Description</a></li>
                        <li class="nav-item"><a class="nav-link" id="reviews-tab" data-toggle="tab" href="#reviews-content" role="tab" aria-controls="reviews-content" aria-selected="false">Reviews ({{$product->ratings()->count()}})</a></li>
                        
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane fade show active" id="description-content" role="tabpanel" aria-labelledby="description-tab">
                            <p class="block-content">
                                {!! $product->description !!}

                            </p>
                            
                        </div>
                        
                        <div class="tab-pane fade" id="reviews-content" role="tabpanel" aria-labelledby="reviews-tab">
                            <div class="ps-product--reviews">
                                <div class="row">
                                    <div class="col-12 col-lg-5">
                                        <div class="review__box">
                                            <div class="product__rate">{{number_format($product->ratings()->avg('rating'), 1)}}</div>
                        <select class="rating-stars">
                            <?php $rating_avg = number_format($product->ratings()->avg('rating'), 0); ?>
                            <option value="1" @if($rating_avg == 1) selected @endif>1</option>
                            <option value="2" @if($rating_avg == 2) selected @endif>2</option>
                            <option value="3" @if($rating_avg == 3) selected @endif>3</option>
                            <option value="4" @if($rating_avg == 4) selected @endif>4</option>
                            <option value="5" @if($rating_avg == 5) selected @endif>5</option>
                        </select>
                                            <p>Avg. Star Rating: <b class="text-black">({{$product->ratings()->count()}} reviews)</b></p>
            <?php $total_count = $product->ratings()->count(); ?>
        @if($total_count == !0)
                                            <div class="review__progress">

        <?php 
        $average_5 = ($product->ratings()->where('rating',5)->count()/$total_count)* 100; ?>
                                                <div class="progress-item"><span class="star">5 Stars</span>
                                                    <div class="progress">
        
                                                        <div class="progress-bar bg-warning" role="progressbar" style="width: {{$average_5}}%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div><span class="percent">{{number_format($average_5,2)}}%
                                                    </span>

                                                </div>
            <?php $average_4 = ($product->ratings()->where('rating',4)->count()/$total_count)* 100; ?>
                                                <div class="progress-item"><span class="star">4 Stars</span>
                                                    <div class="progress">
                                                        <div class="progress-bar bg-warning" role="progressbar" style="width: {{$average_4}}%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div><span class="percent">{{number_format($average_4,2)}}%</span>
                                                </div>
                    <?php $average_3 = ($product->ratings()->where('rating',3)->count()/$total_count)* 100; ?>
                                                <div class="progress-item"><span class="star">3 Stars</span>
                                                    <div class="progress">
                                                        <div class="progress-bar bg-warning" role="progressbar" style="width: {{$average_3}}%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div><span class="percent">{{number_format($average_3,2)}}%</span>
                                                </div>
                    <?php $average_2 = ($product->ratings()->where('rating',2)->count()/$total_count)* 100; ?>
                                                <div class="progress-item"><span class="star">2 Stars</span>
                                                    <div class="progress">
                                                        <div class="progress-bar bg-warning" role="progressbar" style="width: {{$average_2}}%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div><span class="percent">{{number_format($average_2,2)}}%</span>
                                                </div>
                    <?php $average_1 = ($product->ratings()->where('rating',1)->count()/$total_count)* 100; ?>
                                                <div class="progress-item"><span class="star">1 Stars</span>
                                                    <div class="progress">
                                                        <div class="progress-bar bg-warning" role="progressbar" style="width: {{$average_1}}%" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div><span class="percent">{{number_format($average_1,2)}}%</span>
                                                </div>
                                            </div>
                                @endif
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-7">
                                        
                                        <div class="review__title">Add A Review</div>
                                        <p class="mb-0">Your email will not be published. Required fields are marked <span class="text-danger">*</span></p>
                                        <div id="ticket"></div>
                                        
                                        <form id="contactForm1" method="POST" action="{{route('rating.store')}}">
                                            @csrf
                                <input type="hidden" name="product_id" value="{{$product->id}}">
                                <input type="hidden" name="status" value="1">
                                            <div class="form-row">
                                                <div class="col-12 form-group--block">
                                                    <div class="input__rating">
                                                        <label>Your rating: <span>*</span></label>
                                                        <select class="rating-stars" name="rating">
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4" selected="selected">4</option>
                                                            <option value="5">5</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-12 form-group--block">
                                                    <label>Review: <span>*</span></label>
                                                    <textarea name="body" class="form-control"></textarea>
                                                </div>
                                                <div class="col-12 col-lg-6 form-group--block">
                                                    <label>Name: <span>*</span></label>
                                                    <input name="name" class="form-control" type="text" required>
                                                </div>
                                                <div class="col-12 col-lg-6 form-group--block">
                                                    <label>Email:</label>
                                                    <input name="email" class="form-control" type="email">
                                                </div>
                                                <div class="col-12 form-group--block">
                                                    <button type="submit" class="btn ps-button ps-btn-submit">Submit Review</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="ps--comments">
                                    <h5 class="comment__title">{{$product->ratings()->count()}} Comments</h5>
                                    <ul class="comment__list">
                            @foreach($product->ratings as $rating)
                                        <li class="comment__item">
                                            <div class="item__avatar"><img src="img/blogs/comment_avatar1.png" alt="alt" /></div>
                                            <div class="item__content">
                                                <div class="item__name">{{$rating->name}}</div>
                                                <div class="item__date">- {{$rating->created_at->format('F j, Y')}}</div>
                                                
                                                <div class="item__rate">
                                                    <select class="rating-stars">
                                                        <option value="1" @if($rating->rating == 1) selected @endif>1</option>
                                                        <option value="2" @if($rating->rating == 2) selected @endif>2</option>
                                                        <option value="3" @if($rating->rating == 3) selected @endif>3</option>
                                                        <option value="4" @if($rating->rating == 4) selected @endif>4</option>
                                                        <option value="5" @if($rating->rating == 5) selected @endif>5</option>
                                                    </select>
                                                </div>
                                                <p class="item__des">{{$rating->body}}</p>
                                            </div>
                                        </li>
                                @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="product__related" id="productTabDetailContent">
                    <h3 class="product__name">Related Products</h3>
                    <div class="owl-carousel" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true" data-owl-item="5" data-owl-item-xs="2" data-owl-item-sm="2" data-owl-item-md="3" data-owl-item-lg="5" data-owl-item-xl="5" data-owl-duration="1000" data-owl-mousedrag="on">
            <?php  $id = $category->id; ?>
@foreach(\App\Shop\Products\Product::orderBy('views','desc')->where('status',1)->whereHas('categories',function($query) use ($id){
                    $query->whereCategoryId($id);
        })->limit(10)->get() as $product )
                        <div class="ps-post--product">
                            <div class="ps-product--standard">
                                <a href="{{route('product',$product->slug)}}">
                                    <img class="ps-product__thumbnail" src="{{asset('storage/'.$product->cover)}}" alt="alt" />
                                </a>
                                <a class="ps-product__expand" href="javascript:void(0);"></i></a>
                                <div class="ps-product__content">
                                    <p class="ps-product__type"><i class="icon-store"></i>Royal Green Agro</p>
                                    
                                    <h5><a class="ps-product__name" href="{{route('product',$product->slug)}}">{{$product->name}}</a></h5>
                                    <p class="ps-product__unit">{{$product->quantity}} in Stock</p>
                    <div class="ps-product__rating">
                                                <select class="rating-stars">
                            <?php $rating_avg = number_format($product->ratings()->avg('rating'), 0); ?>
                            <option value="1" @if($rating_avg == 1) selected @endif>1</option>
                            <option value="2" @if($rating_avg == 2) selected @endif>2</option>
                            <option value="3" @if($rating_avg == 3) selected @endif>3</option>
                            <option value="4" @if($rating_avg == 4) selected @endif>4</option>
                            <option value="5" @if($rating_avg == 5) selected @endif>5</option>
                        </select>
                        <span>({{$product->ratings()->count()}})</span>
                    </div>
                                    
                                    
                                    <p class="ps-product-price-block">
                                        <span class="ps-product__sale">{{config('settings.currency_code')}} {{$product->price}}</span>
                                        @if($product->sale_price == !null)
                                        <span class="ps-product__price">{{config('settings.currency_code')}} {{$product->sale_price}}</span>
                                        @endif
                                        @if($product->discount == !null)
                                        <span class="ps-badge ps-product__offbadge">{{number_format(($product->discount)/($product->sale_price) * 100, 2, '.', '') }}% Off </span>@endif
                                    </p>
                                </div>
                                <div class="ps-product__footer">
                                    <a href="{{route('product',$product->slug)}}">
                                    <button class="ps-product__addcart"><i class="icon-cart"></i>View</button></a>
                                    
                                </div>
                            </div>
                        </div>
                @endforeach
                        
                        
                        


                    </div>
                </div>
            </div>
            
            
        </section>
        
    </main>

    @include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection
    