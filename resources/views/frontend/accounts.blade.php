@section('title')
    Accounts | {{config('settings.site_title')}}
@endsection
@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    
    <main class="no-main">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="ps-breadcrumb__list">
                    <li class="active"><a href="{{route('index')}}">Home</a></li>
                    <li><a href="javascript:void(0);">Account</a></li>
                </ul>
            </div>
        </div>
        <section class="section--faq">
            <div class="container">
                <h2 class="page__title">Account</h2>
                <div class="faq__content">
                    <div class="row">
                        <div class="col-12 col-lg-4">
                            <div class="faq__category">
                                <ul class="nav nav-tabs">
                                    <li><a data-toggle="tab" href="#profile" @if(request()->input('tab') == 'profile') class="active" @endif>Personal Information</a></li>
                                    <li><a data-toggle="tab" href="#orders" @if(request()->input('orders') == 'profile') class="active" @endif>Orders</a></li>
                                    <li><a data-toggle="tab" href="#address" @if(request()->input('tab') == 'address') class="active" @endif>Address</a></li>
                                    <li><a href="{{route('logout')}}">Logout</a></li>
                                </ul>
                                
                            </div>
                        </div>
                        <div class="col-12 col-lg-8">
                            <div class="tab-content">
                                <div class="tab-pane fade @if(request()->input('tab') == 'profile')active show @endif" id="profile">
                                    <div class="row">
                                        <div class="col-12 col-lg-12">
                                            <div class="faq__item">
                                                {{$customer->name}} <br /><small>{{$customer->email}}</small>
                                            </div>
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="tab-pane fade @if(request()->input('tab') == 'orders')active show @endif" id="orders">
                                    <div class="row">
                                        <div class="col-12 col-lg-12">


                                            @if(!$orders->isEmpty())
                                <table class="table">
                                <thead class="thead-light">
                                    <tr>
                                        <th>
                                            <b class="text-black">Date</b>
                                        </th>
                                        <th><b class="text-black">Total</b></th>
                                        <th><b class="text-black">Status</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($orders as $order)
                                    <tr>
                                        <td>
                                            <a data-toggle="modal" data-target="#order_modal_{{$order['id']}}" title="Show order" href="javascript: void(0)">{{ date('M d, Y h:i a', strtotime($order['created_at'])) }}</a>
                                            <!-- Button trigger modal -->
                                            <!-- Modal -->
                                            <div class="modal fade" id="order_modal_{{$order['id']}}" tabindex="-1" role="dialog" aria-labelledby="MyOrders">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                            
                                                        </div>
                                                        <h4 class="modal-title" id="myModalLabel">Reference #{{$order['reference']}}</h4>
                                                        <div class="modal-body">
                                                            <table class="table">
                                                                <thead>
                                                                    <th>Address</th>
                                                                    <th>Payment Method</th>
                                                                    <th>Total</th>
                                                                    <th>Status</th>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <address>
                                                                                <strong>
                                                                                    {{$order['address']->alias}}
                                                                                </strong>
                                                                                <br />
                                                                                {{$order['address']->address_1}} {{$order['address']->address_2}}<br>
                                                                            </address>
                                                                        </td>
                                                                        <td>{{$order['payment']}}</td>
                                                                        <td>{{ config('cart.currency_symbol') }} {{number_format($order['total'])}}</td>
                                                                        <td><p class="text-center" style="color: #ffffff; background-color: {{ $order['status']->color }}">{{ $order['status']->name }}</p></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <hr>
                                                            <p>Order details:</p>
                                                            <table class="table">
                                                              <thead>
                                                                  <th>Name</th>
                                                                  <th>Qty</th>
                                                                  <th>Price</th>
                                                                  <th>Cover</th>
                                                              </thead>
                                                              <tbody>
                                                              @foreach ($order['products'] as $product)
                                                                  <tr>
                                                                      <td>{{$product['name']}}</td>
                                                                      <td>{{$product['pivot']['quantity']}}</td>
                                                                      <td>{{number_format($product['price'])}}</td>
                                                                      <td><img src="{{ asset("storage/".$product['cover']) }}" width=50px height=50px alt="{{ $product['name'] }}" class="img-orderDetail"></td>
                                                                  </tr>
                                                              @endforeach
                                                              </tbody>
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td><span class="label @if($order['total'] != $order['total_paid']) label-danger @else label-success @endif">{{ config('cart.currency') }} {{ number_format($order['total']) }}</span></td>
                                        <td><p class="text-center" style="color: #ffffff; background-color: {{ $order['status']->color }}">{{ $order['status']->name }}</p></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                                {{ $orders->links() }}
                            @else
                                <p class="alert alert-warning">No orders yet. <a href="{{ route('index') }}">Shop now!</a></p>
                            @endif
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                                <div class="tab-pane fade @if(request()->input('tab') == 'address')active show @endif" id="address">
                                    <div class="row">
                                        <div class="col-12 col-lg-12 faq__colLeft">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="ps-social--share">
                                        <a href="{{ route('customer.address.create', auth()->user()->id) }}" class="ps-social__icon twitter"><i class="fa fa-plus"></i> Create Address </a>
                                    </div>
                                </div>
                            </div>
                                            @if(!$addresses->isEmpty())
                                <table class="table">
                                <thead class="thead-light">
                                    <tr>
                                        <th><b class="text-black">Alias</b></th>
                                        <th><b class="text-black">Address</b></th>
                                        <th><b class="text-black">Actions</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($addresses as $address)
                                        <tr class="deleteAddressRow{{$address->id}}">
                                            <td>{{$address->alias}}</td>
                                            <td>{{$address->address_1}} {{$address->address_2}} {{ $address->country->name}} {{$address->zip}} {{$address->phone}} </td>
                                            <td>
                                                
                                                    <div class="btn-group">
                                                        <input type="hidden" name="_method" value="delete">
                                                        {{ csrf_field() }}
                                                        <div class="ps-social--share">
                                                            <a href="{{ route('customer.address.edit', [auth()->user()->id, $address->id]) }}" class="ps-social__icon facebook" href="#">
                                                                <i class="fa fa-pencil"></i><span>Edit</span>
                                                            </a>
                                                            <a id="{{auth()->user()->id}}" address_id="{{$address->id}}" class="ps-social__icon deleteAddress" type="submit">
                                                                <i class="icon-trash2"></i><span>Delete</span>
                                                            </a>
                                                            
                                                        </div>
                                                    
                                                        
                                                    </div>
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            @else
                                <br /> <p class="alert alert-warning">No address created yet.</p>
                            @endif
                                        </div>
                                        


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
    
@include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection
    