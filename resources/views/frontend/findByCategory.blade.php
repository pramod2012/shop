@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
        <section class="section--blogThumbnail">
            <div class="container">
                <div class="blog--header">
                    <h2 class="page__title">{{$mcategory->name}}</h2>
                    <div class="ps-breadcrumb">
                        <div class="container">
                            <ul class="ps-breadcrumb__list">
                                <li class="active"><a href="{{route('index')}}">Home</a></li>
                                <li><a href="{{route('blog')}}">Blog &amp; News</a></li>
                                <li><a href="javascript:void(0);">{{$mcategory->name}}</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-9">
                        <div class="blog__content">

                        	@foreach($posts as $post)
                            <div class="post__item">
                                <div class="post-row">
                                    <div class="post--thumbnail">
                                        <div class="post__img">
                                        	<a href="{{route('blog.show',$post->slug)}}">
                            					<img src="{{@asset('storage/'. $post->image)}}" alt="image">
                        					</a>
                                        </div>
                                        <div class="post__content">
                                            <div class="post__archives">
                                            	<span class="archive__item post__type">
                                            		<i class="icon-folder"></i>{{$post->mcategory->name}}
                                            	</span>
                                            	
                                            <span class="archive__item">{{$post->created_at->format('F j, Y')}} by 
                                            	<span class="blog__auth">
                                            		<a href="{{route('blog.author',$post->author->slug)}}">{{$post->author->name}}</a>
                                            	</span>
                                            </span>
                                            </div>
                                            <a class="post__title" href="{{route('blog.show',$post->slug)}}">{{$post->name}}</a>
                                            <p class="post__des">{!! substr($post->summary, 0, 100) !!}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>

                        <div class="ps-pagination blog--pagination">
                        	{{$posts->links()}}
                        </div>
                    </div>
                    <div class="col-12 col-lg-3">
                        <div class="blog--widget">
                            <div class="widget__search">
                                <div class="input-group">
                                    <input class="form-control" type="text" placeholder="Search in post...">
                                    <div class="input-group-append"><i class="icon-magnifier"></i></div>
                                </div>
                            </div>
                            <div class="widget__category">
                                <div class="widget__title">CATEGORIES</div>
                                <ul class="category-list">
@foreach(\App\Shop\Mcategories\Mcategory::orderBy('order','asc')->where('status',1)->get() as $value)
                                    <li>
                                    	<a href="{{route('blog.category',$value->slug)}}">
                                    		<span class="name">{{$value->name}}</span>
                                    	</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="widget__posts">
                                <div class="widget__title">LATEST POSTS</div>
                                <ul class="post-list">
                               @foreach($post_related as $value)
                                    <li>
                                    	<a href="{{route('blog.show',$value->slug)}}">
                                    		<img src="{{asset('storage/'.$value->image)}}" alt="alt" />
                                            <div class="post__content">
                                                <p class="post__date">{{$value->created_at->format('F j, Y')}} by <span>{{$value->author->name}}</span></p>
                                                <h5 class="post__title">{{$value->name}}</h5>
                                            </div>
                                        </a>
                                    </li>@endforeach
                                    
                                </ul>
                            </div>
                            <div class="widget__tags">
                                <div class="widget__title">POPULAR TAGS</div>
                                <ul>
                       @foreach(\App\Shop\Tags\Tag::all() as $tag)
                                    <li><a href="{{route('blog.tag',$tag->slug)}}">{{$tag->name}}</a></li>
                                    @endforeach
                                </ul>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

    @include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection