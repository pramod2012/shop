@section('title')
    Cart Items | {{config('settings.site_title')}}
@endsection
@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="ps-breadcrumb__list">
                    <li class="active"><a href="{{route('index')}}">Home</a></li>
                    <li><a href="javascript:void(0);">Shopping Cart</a></li>
                </ul>
            </div>
        </div>
        <section class="section--shopping-cart">
            <div class="container shopping-container">
                <h2 class="page__title">Shopping Cart</h2>
                @if($cartItems->count() > 0)
                <div class="shopping-cart__content">
                    <div class="row m-0">
                        <div class="col-12 col-lg-8">
                            <div class="shopping-cart__products">
                                <div class="shopping-cart__table">
                                    <div class="shopping-cart-light">
                                        <div class="shopping-cart-row">
                                            <div class="cart-product">Name</div>
                                            <div class="cart-price">Price</div>
                                            <div class="cart-quantity">Quantity</div>
                                            <div class="cart-total">Total</div>
                                            <div class="cart-action"> </div>
                                        </div>
                                    </div>
                                    <div class="shopping-cart-body">
                                   @foreach($cartItems as $cartItem)
                                        <div class="shopping-cart-row delete{{$cartItem->rowId}}">
                                            <div class="cart-product">
                                                <div class="ps-product--mini-cart">
                                                	<a href="{{ route('product', [$cartItem->product->slug]) }}">
                                                	@if(isset($cartItem->cover))
                                                		<img class="ps-product__thumbnail" src="{{$cartItem->cover}}" alt="alt" />
                                                	@else
                                                    	<img src="https://placehold.it/120x120" alt="" class="img-responsive img-thumbnail">
                                                	@endif
                                                	</a>
                                                    <div class="ps-product__content">
                                                        <h5><a class="ps-product__name" href="#">{{ $cartItem->name }}</a></h5>
                                           @if($cartItem->options->has('combination'))
                                                <div style="margin-bottom:5px;">
                                                @foreach($cartItem->options->combination as $option)
                                                    <small class="label label-primary">{{$option['value']}}</small>
                                                @endforeach
                                                </div>
                                            @endif
                                                        <p class="ps-product__meta">Price: <span class="ps-product__price">{{config('cart.currency')}}{{ number_format($cartItem->price) }}</span></p>
                                                    
                                                        <div class="def-number-input number-input safari_only">
                                                            
                                                            <button class="minus" onclick="this.parentNode.querySelector('input[type=number]').stepDown()"><i class="icon-minus"></i></button>
                                                            <input class="quantity" min="0" name="quantity" value="{{ $cartItem->qty }}" type="number" />
                                                            <button class="plus" onclick="this.parentNode.querySelector('input[type=number]').stepUp()"><i class="icon-plus"></i></button>
                                                        
                                                        </div>
                                                        
                                                        <span class="ps-product__total">Total: <span>{{config('cart.currency')}}{{ number_format($cartItem->qty*$cartItem->price) }} </span></span>
                                                    </div>
                                                    <div class="ps-product__remove">
                                                    	<a id="{{$cartItem->rowId}}" class="trash" type="submit"><i class="icon-trash2"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="cart-price"><span class="ps-product__price">{{config('cart.currency')}}{{ number_format($cartItem->price) }}</span>
                                            </div>
                                            <div class="cart-quantity">
                                    <form action="{{ route('cart.update', $cartItem->rowId) }}" method="post">
                                        @csrf
                                        {{ method_field('PUT') }}
                                                <div class="def-number-input number-input safari_only">
                                                    <button class="minus" onclick="this.parentNode.querySelector('input[type=number]').stepDown()"><i class="icon-minus"></i></button>
                                                    <input class="quantity" min="0" name="quantity" value="{{ $cartItem->qty }}" type="number" />
                                                    <button class="plus" onclick="this.parentNode.querySelector('input[type=number]').stepUp()"><i class="icon-plus"></i></button>
                                                    <button class="plus"><i class="icon-sync"></i></button>
                                                </div>
                                                </form>
                                            </div>
                                            <div class="cart-total"> <span class="ps-product__total">{{config('cart.currency')}}{{ number_format($cartItem->qty*$cartItem->price) }} </span>
                                            </div>

                                            <div class="cart-action">
                                                <a id="{{$cartItem->rowId}}" class="trash" type="submit"><i class="icon-trash2"></i></a>
                                            </div>
                                        </div>
                                      @endforeach
                                        
                                    </div>
                                </div>
                                <div class="shopping-cart__step">
                                	
                                	
                                	<a class="button left" href="{{route('index')}}">
                                		<i class="icon-arrow-left"></i>Continue Shopping
                                	</a>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-12 col-lg-4">
                            <div class="shopping-cart__right">
                                <div class="shopping-cart__total">
                                    <p class="shopping-cart__subtotal">
                                    	<span>Subtotal</span>
                                    	<span class="price">{{config('cart.currency')}} {{ number_format($subtotal) }}</span>
                                    </p>
                                    @if(isset($shippingFee) && $shippingFee != 0)
                                    <p class="shopping-cart__shipping">Shipping<br>
                                    	<span class="price">{{config('cart.currency')}} {{ $shippingFee }}</span>
                                    </p>
                                    @endif
                                    <p class="shopping-cart__subtotal">
                                    	<span><b>TOTAL</b></span>
                                    	<span class="price-total">{{config('cart.currency')}} {{ number_format($total) }}</span>
                                    </p>
                                </div><a class="btn shopping-cart__checkout" href="{{ route('checkout.index') }}">Proceed to Checkout</a>
                            </div>
                        </div>
                    </div>
                </div>

        @else
            <div class="row">
                <div class="col-md-12">
                    <p class="alert alert-warning">No products in cart yet. <a href="{{ route('index') }}">Show now!</a></p>
                </div>
            </div>
            @endif



            </div>
        </section>
    </main>
@include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection
    