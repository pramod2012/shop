<div class="sidebar__sort">
                                    
                                    <div class="sidebar__block open">
                                        <div class="sidebar__title">BY PRICE<span class="shop-widget-toggle"><i class="icon-minus"></i></span></div>
                                        <form action="{{route('search')}}" method="get">
                <input type="hidden" name="sort" value="{{Request::get('sort')}}">
                @if(Request::get('price') ==!null )
                <input type="hidden" name="price" value="{{Request::get('price')}}">@endif
                @if(Request::get('views') ==!null )
                <input type="hidden" name="views" value="{{Request::get('views')}}">@endif

                                            <div class="input-group">
                                                <input name="name" class="form-control" type="text" value="{{Request::get('name')}}" placeholder="Search title...">
                                                <div class="input-group-append"><i class="icon-magnifier"></i></div>
                                            </div>
                                            <div class="input-group">
                                            <select class="single-select2-no-search" name="category_id">
                                                <option value="" disabled selected>--Choose Category--</option>
        @foreach(\App\Shop\Categories\Category::where('status',1)->orderBy('order','asc')->where('parent_id',null)->get() as $value)
                        @if(@$category->id == $value->id || Request::get('category_id') == $value->id)
                        <option value="{{$value->id}}" selected>{{$value->name}}</option>
                        @else
                        <option value="{{$value->id}}">{{$value->name}}</option>
                        @endif
                        @foreach($value->children as $subcat)
                        @if(@$category->id == $subcat->id || Request::get('category_id') == $subcat->id)
                        <option value="{{$subcat->id}}" selected>---{{$subcat->name}}</option>
                        @else
                        <option value="{{$subcat->id}}">---{{$subcat->name}}</option>
                        @endif
                        @endforeach
                        @endforeach
                                            </select>
                                        </div>
                                        <div class="block__content">
                                            <div class="block__price">
                                                <div id="slide-price"></div>
                                            </div>
                                            <div class="block__input">
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">Rs</span></div>
                                                    <input name="min_price" class="form-control" type="text" id="input-with-keypress-0">
                                                </div>
                                                <div class="input-group">
                                                    <div class="input-group-prepend"><span class="input-group-text">Rs</span></div>
                                                    <input name="max_price" class="form-control" type="text" id="input-with-keypress-1">
                                                </div>
                                                <button type="submit">Go</button>
                                            </div>
                                        </div>
                                        <div class="block__input">
                                       
                                            <button type="submit">Search</button>
                                        
                                        </div>
                                        </form>
                                    </div>
                                    
                                    
                                    
                                    
                                </div>