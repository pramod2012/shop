<section class="section-featured--default ps-home--block">
            <div class="container">
                <div class="ps-block__header">
                    <h3 class="ps-block__title">Featured Categories</h3>
                </div>
                <div class="featured--content">
@foreach(\App\Shop\Categories\Category::where('feature',1)->where('status',1)->limit(5)->get() as $value)
                    <div class="featured__first">
                        <div class="ps-product--vertical">
                            <a href="{{route('category',$value->slug)}}">
                                <img class="ps-product__thumbnail" src="{{asset('storage/'.$value->cover)}}" alt="alt" />
                            </a>
                            <div class="ps-product__content"><a class="ps-product__name" href="{{route('category',$value->slug)}}">{{$value->name}}</a>
                                <p class="ps-product__quantity">{{$value->products->count()}} items</p>
                            </div>
                        </div>
                    </div>
@endforeach       
                    </div>
                </div>
                <div class="featured--content-mobile">
                    <div class="owl-carousel" data-owl-auto="true" data-owl-loop="true" data-owl-speed="10000" data-owl-gap="0" data-owl-nav="false" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000" data-owl-mousedrag="on">
@foreach(\App\Shop\Categories\Category::where('feature',1)->where('status',1)->get()->chunk(4) as $chunk)
                        <div class="product-slide">
        @foreach($chunk as $value)
                            <a class="ps-product--vertical" href="{{route('category',$value->slug)}}"><img class="ps-product__thumbnail" src="{{asset('storage/'.$value->cover)}}" alt="alt" />
                                <div class="ps-product__content">
                                    <h5 class="ps-product__name">{{$value->name}}</h5>
                                    <p class="ps-product__quantity">{{$value->products->count()}} items</p>
                                </div>
                            </a>
                @endforeach
                        </div>
@endforeach
                    </div>
                </div>
            </div>
        </section>