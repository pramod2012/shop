<header class="header">
        <div class="ps-top-bar">
            <div class="container">
                <div class="top-bar">
                    <div class="top-bar__left">
                        <ul class="nav-top">
                            <li class="nav-top-item"> <a class="nav-top-link" href="{{url('login')}}">Login</a>
                            </li>
                            <li class="nav-top-item"> <a class="nav-top-link text-success" href="{{route('register')}}">Register Now</a>
                            </li>
                        </ul>
                    </div>
                    <div class="top-bar__right">
                        <ul class="nav-top">
                            <li class="nav-top-item contact"><a class="nav-top-link" href="tel:{{config('settings.phone')}}"> <i class="icon-telephone"></i><span>Hotline:</span><span class="text-success font-bold">{{config('settings.phone')}}</span></a></li>
                            
                            
                            
                            <li class="nav-top-item account">
                                @if(auth()->check())
                                <a class="nav-top-link" href="/accounts?tab=profile"> <i class="icon-user"></i>Hi! <span class="font-bold">{{auth()->user()->name}}</span>
                                </a>
                                @endif
                                @if(auth()->check())
                                <div class="account--dropdown">
                                    <div class="account-anchor">
                                        <div class="triangle"></div>
                                    </div>
                                    <div class="account__content">
                                        <ul class="account-list">
                                            <li class="title-item"> <a href="javascript:void(0);">My Account</a>
                                            </li>
                                            <li> <a href="/accounts?tab=profile">Dasdboard</a>
                                            </li>
                                            <li> <a href="accounts?tab=address">Address</a>
                                            <li> <a href="/accounts?tab=orders">Orders</a>
                                            </li>
                                            
                                            
                                            </li>
                                        </ul>
                                        
                                        <hr><a class="account-logout" href="{{ route('logout') }}"><i class="icon-exit-left"></i>Log out</a>
                                    </div>
                                    @endif
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="ps-header--center header--mobile">
            <div class="container">
                <div class="header-inner">
                    <div class="header-inner__left">
                        <button class="navbar-toggler"><i class="icon-menu"></i></button>
                    </div>
                    <div class="header-inner__center"><a class="logo open" href="{{url('/')}}"><img src="{{asset('storage/'.config('settings.logo'))}}" height="58px"></a></div>
                    <div class="header-inner__right">
                        <button class="button-icon icon-sm search-mobile"><i class="icon-magnifier"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <section class="ps-header--center header-desktop">
            <div class="container">
                
                <div class="header-inner">
                    <div class="header-inner__left"><a class="logo" href="{{url('/')}}"><img src="{{asset('storage/'.config('settings.logo'))}}" height="70px"></a>
                        <ul class="menu">
                            <li class="menu-item-has-children has-mega-menu">
                                <button class="category-toggler"><i class="icon-menu"></i></button>
                    <form method="get" action="{{route('search')}}">
                                <div class="mega-menu mega-menu-category">
                                    <ul class="menu--mobile menu--horizontal">
                     
@foreach(\App\Shop\Categories\Category::where('parent_id',null)->where('status',1)->get() as $category)
                                        <li class="has-mega-menu category-item"><a href="javascript:void(0);">{{$category->name}}</a><span class="sub-toggle"><i class="icon-chevron-down"></i></span>
                                            <div class="mega-menu">
                                                <div class="mega-anchor"></div>
                                                <div class="mega-menu__column">
                                                    <ul class="sub-menu--mega">
                                        @foreach($category->children as $cat)
                                                        <li> <a href="{{route('category',$cat->slug)}}">{{$cat->name}}</a>
                                                        </li>
                                        @endforeach
                                                    </ul>
                                                </div>
                                                
                                                <div class="mega-menu__column">
                                        @foreach($category->promotions as $promo)
                                                    <a class="mega-menu__thumbnail" href="//{{$promo->link}}">
                                                        <img src="{{asset('storage/'.$promo->image)}}" alt="alt" />
                                                    </a>
                                        @endforeach
                                                </div>
                                            </div>
                                        </li>
                    @endforeach
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                    
                    <div class="header-inner__center">
                        <div class="input-group">
                            
                            <div class="input-group-prepend">
                                <i class="icon-magnifier search"></i>
                            </div>
                            
                            <input name="name" class="form-control input-search" placeholder="I'm searchching for...">
                            <div class="input-group-append">
                                <button class="btn" type="submit">Search</button>

                            </div>
                            
                        </div>
                        <div class="trending-search">
                            <ul class="list-trending">
                                <li class="title"> <a>Featured searches: </a>
                                </li>
                                <li class="trending-item"> <a href="https://royalgreensagro.com/">Back to Website</a>
                                </li>
                                @foreach(\App\Shop\Categories\Category::where('feature',1)->where('status',1)->get() as $value)
                                <li class="trending-item"> <a href="{{route('category',$value->slug)}}">{{$value->name}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        
                        
                    </div>
                    </form>
                    <div class="header-inner__right">
                        <a class="button-icon icon-md" href="{{url('wishlist')}}">
                            <i class="icon-heart"></i>
                            <div id="wishlistCount">
                                <span class="badge bg-warning">{{$wishlistItems->count()}}</span>
                            </div>
                        </a>
                        <div class="button-icon btn-cart-header">
                            <i class="icon-cart icon-shop5"></i>
                            <div id="count">
                                <span class="badge bg-warning">{{$cartCount}}</span>
                            </div>
                            
                            <div class="mini-cart">
                                <div class="mini-cart--content">
                                    <div class="mini-cart--overlay"></div>
                                    <div class="mini-cart--slidebar cart--box">
                                        <div class="mini-cart__header">
                                            <div class="cart-header-title">
                                                <h5>Shopping Cart({{$cartCount}})</h5><a class="close-cart" href="javascript:void(0);"><i class="icon-arrow-right"></i></a>
                                            </div>
                                        </div>
                                        <div class="mini-cart__products">
                                            <div class="out-box-cart">
                                                <div class="triangle-box">
                                                    <div class="triangle"></div>
                                                </div>
                                            </div>
                                            <ul class="list-cart">
                        <div id="cartItems">

                @foreach($cartItems as $cartItem)
                                                <li class="cart-item delete{{$cartItem->rowId}}">
                                                    <div class="ps-product--mini-cart"><a href="{{ route('product', [$cartItem->product->slug]) }}">
                                                        @if(isset($cartItem->cover))
                                                        <img class="ps-product__thumbnail" src="{{$cartItem->cover}}" alt="alt" />
                                                    @else
                                                        <img src="https://placehold.it/120x120" alt="" class="img-responsive img-thumbnail">
                                                    @endif
                                                    </a>
                                                        <div class="ps-product__content"><a class="ps-product__name" href="{{ route('product', [$cartItem->product->slug]) }}">{{ $cartItem->name }}</a>
                                                            
                                                            <p class="ps-product__meta"> <span class="ps-product__price">{{config('cart.currency')}} {{ number_format($cartItem->price) }}</span><span class="ps-product__quantity">(x{{$cartItem->qty}})</span>
                                                            </p>
                                                        </div>
                                                        <div class="ps-product__remove">
                                                            <a id="{{$cartItem->rowId}}" class="trash" type="submit"><i class="icon-trash2"></i></a>
                                                        </div>
                                                    </div>
                                                </li>
                                    @endforeach
                                    </div>
                                            </ul>
                                        </div>
                                        <div class="mini-cart__footer row">
                                            <div class="col-6 title">SUBTOTAL</div>
                                            <div id="total">
                                                <div class="col-6 text-right total">{{config('cart.currency')}} {{ number_format($subtotal, 2, '.', ',') }}</div>
                                            </div>
                                            
                                            <div class="col-12 d-flex"><a class="view-cart" href="{{url('/cart')}}">View cart</a><a class="checkout" href="{{url('checkout')}}">Checkout</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <nav class="navigation">
            <div class="container">
                <ul class="menu">
                    <li class="menu-item-has-children has-mega-menu active"> <a class="nav-link" href="https://royalgreensagro.com/">Back to website</a></li>
                    <li class="menu-item-has-children has-mega-menu active"> <a class="nav-link" href="{{url('/')}}">Home</a></li>
                        
    <?php $category = \App\Shop\Categories\Category::where('parent_id',null)->where('status',1)->orderBy('order','asc')->get();  ?>
                    
@foreach($category as $key => $value)
@if($key < 5)
                    <li class="menu-item-has-children has-mega-menu"><a class="nav-link" href="{{route('category',$value->slug)}}" @if($value->children->count() > 0)href="javascript:void(0);" @endif>{{$value->name}}</a>
                    @if($value->children->count() > 0)
                        <span class="sub-toggle"><i class="icon-chevron-down"></i></span>

                        <div class="mega-menu">
                            <div class="mega-anchor"></div>
                            <div class="mega-menu__column">
                                <h4>{{$value->name}}<span class="sub-toggle"></span></h4>
                                <ul class="sub-menu--mega">
                                @foreach($value->children as $cat)
                                    <li> <a href="{{route('category',$cat->slug)}}">{{$cat->name}}</a>
                                    </li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    </li>
    @endif
    @if($key === 5)
    <li class="menu-item-has-children has-mega-menu">
    <a class="nav-link" href="javascript:void(0);">More</a>
    <span class="sub-toggle">
        <i class="icon-chevron-down"></i>
    </span>
                        <div class="mega-menu">
                            <div class="mega-anchor"></div>
                            <div class="mega-menu__column">
                                
                                <ul class="sub-menu--mega">
                                    @foreach($category as $key => $value)
                                    @if($key>=5 && $key < 12)
                                    <li> <a href="{{route('category',$value->slug)}}">{{$value->name}}</a>
                                    </li>
                                    @endif
                                    @endforeach

                                    
                                </ul>
                            </div>
                        </div>
                    </li>
@endif
               
@endforeach


                </ul>
                
            </div>
        </nav>
        <div class="mobile-search--slidebar">
            <form method="get" action="{{route('search')}}">
            <div class="mobile-search--content">
                <div class="mobile-search__header">
                    <div class="mobile-search-box">
                        <div class="input-group">
                            <input name="name" class="form-control" placeholder="I'm searching for..." id="inputSearchMobile">
                            <div class="input-group-append">
                                <button class="btn"> <i class="icon-magnifier"></i></button>
                            </div>
                            
                        </div>
                        <button class="cancel-search"><i class="icon-cross"></i></button>
                    </div>
                </div>
                </form>
                <div class="mobile-search__trendding">
                    <h5> <i class="icon-power"></i>Featured search</h5>
                    <div class="trending-content">
                        <ul class="mobile-list-trending">
                            <li class="title"> <a>Featured search: </a>
                            </li>
@foreach(\App\Shop\Categories\Category::where('feature',1)->where('status',1)->get() as $value)
                            <li class="trending-item"> <a href="{{route('category',$value->slug)}}">{{$value->name}}</a>
                            </li>
                            @endforeach
                            
                        </ul>
                    </div>
                </div>
                
                
                
            </div>
        </div>
    </header>