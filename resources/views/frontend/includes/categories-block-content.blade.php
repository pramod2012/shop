<?php $categories = \App\Shop\Categories\Category::where('status',1)->where('homepage',1)->orderBy('order','asc')->get(); ?>

@foreach($categories as $category)
<div class="categories--block">
                    <h3><a class="categories__title" id="{{$category->slug}}Block">{{$category->name}}</a></h3>
                    <div class="categories__content">
                        
                        <div class="categories__promotion">
                            <div class="slick-single-item">
                           @foreach($category->promotions as $promotion)
                                <div class="categories-carousel">
                                    <a href="//{{$promotion->link}}">
                                        <img class="carousel__thumbnail" src="{{asset('storage/'.$promotion->image)}}" alt="alt" />
                                    </a>
                                </div>
                         @endforeach
                            </div>
                            
                            <div class="row categories__list">
                                @foreach($category->children->chunk(4) as $chunk)
                                <div class="col-6">
                    @foreach($chunk as $cat)
                                    <div class="categories__list-item"><a href="{{route('category',$cat->slug)}}">{{$cat->name}}</a></div>
                        @endforeach
                                </div>
                                @endforeach
                            </div>
                            
                            <div class="categories__footer"><a href="{{route('category',$category->slug)}}">
                                    <u>View all</u><i class="icon-chevron-right"></i></a></div>
                        </div>
                        <div class="categories__products">
                            <div class="row m-0">
                    @foreach($category->products->where('status',1)->take(6) as $product)
                                <div class="col-6 col-md-4 col-lg-4 p-0">
                                    <div class="ps-product--standard">
                                        @if($product->discount == !null)
                                        <span class="ps-badge ps-product__offbadge">{{number_format(($product->discount)/($product->sale_price) * 100, 2, '.', '') }}% Off </span>@endif
                                    <a href="{{route('product',$product->slug)}}">
                                        <img class="ps-product__thumbnail" src="{{asset('storage/'.$product->cover)}}" alt="alt" style="width: 200px" />
                                    </a>
                                    
                                        <div class="ps-product__content">
                                            <p class="ps-product__type"><i class="icon-store"></i>Royal Greens Agro</p>
                                            <h5><a class="ps-product__name" href="{{route('product',$product->slug)}}">{{$product->name}}</a></h5>
                                            <p class="ps-product__unit">{{$product->quantity}} in Stock</p>

                                            <div class="ps-product__rating">
                                                <select class="rating-stars">
                            <?php $rating_avg = number_format($product->ratings()->avg('rating'), 0); ?>
                            <option value="1" @if($rating_avg == 1) selected @endif>1</option>
                            <option value="2" @if($rating_avg == 2) selected @endif>2</option>
                            <option value="3" @if($rating_avg == 3) selected @endif>3</option>
                            <option value="4" @if($rating_avg == 4) selected @endif>4</option>
                            <option value="5" @if($rating_avg == 5) selected @endif>5</option>
                        </select>
                        <span>({{$product->ratings()->count()}})</span>
                                            </div>
                                            
                                            <p class="ps-product-price-block"><span class="ps-product__sale">{{config('settings.currency_code')}} {{number_format($product->price)}} @if($product->weight > 0) per {{number_format($product->weight)}} {{$product->mass_unit}} @endif</span>
                                            @if($product->sale_price == !null || $product->sale_price == !0)
                                            <span class="ps-product__price">{{config('settings.currency_code')}} {{number_format($product->sale_price)}} @if($product->weight > 0) per {{number_format($product->weight)}} {{$product->mass_unit}} @endif</span>@endif
                                            </p>
                                        </div>
                                        <div class="ps-product__footer">
                                            <div class="def-number-input number-input safari_only">
                                                <button class="minus" onclick="this.parentNode.querySelector('input[type=number]').stepDown()"><i class="icon-minus"></i></button>
                                                <input class="quantity" min="0" name="quantity{{$product->id}}" value="1" type="number" />
                                                <button class="plus" onclick="this.parentNode.querySelector('input[type=number]').stepUp()"><i class="icon-plus"></i></button>
                                            </div>
                                            
                                            <button class="ps-product__addcart" data-product="{{$product->id}}" id="{{'btn_'.$product->id}}" onclick="addToCart(this)"><i class="icon-cart"></i>Add to cart</button>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                        @endforeach

                            </div>
                        </div>
                    </div>
                </div>
@endforeach