<section class="section-recommendations--default ps-home--block">
            <div class="container">
                <div class="ps-block__header mobile">
                    <h3 class="ps-block__title">Recommendations</h3>
                    
                </div>
                <div class="recommendations__content">
                    <div class="owl-carousel" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true" data-owl-item="3" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="3" data-owl-item-xl="3" data-owl-duration="1000" data-owl-mousedrag="on">
@foreach(\App\Shop\Products\Product::orderBy('created_at','desc')->where('status',1)->limit(12)->get()->chunk(3) as $chunk)
                        <div class="recommendation-carousel">
            @foreach($chunk as $product)
                            <div class="ps-product--vertical"><a href="{{route('product',$product->slug)}}"><img class="ps-product__thumbnail" src="{{asset('storage/'.$product->cover)}}" alt="alt" /></a>
                                <div class="ps-product__content">
                                    <h5><a class="ps-product__name" href="{{route('product',$product->slug)}}">{{$product->name}}</a></h5>
                                    <p class="ps-product__unit">
                                        @if($product->discount == !null)
                                            <span class="ps-product__off">{{number_format(($product->discount)/($product->sale_price) * 100, 2, '.', '') }}% Off</span>@endif
                                    </p>
                                    <p class="ps-product-price-block"><span class="ps-product__price-default">{{config('settings.currency_code')}} {{$product->price}}</span>
                                    </p>
                                </div>
                            </div>
                @endforeach
                        </div>
            @endforeach
                        
                    </div>
                </div>
            </div>
        </section>