<div class="ps-footer--categories">
@foreach(\App\Shop\Categories\Category::where('status',1)->get() as $category)
                <div class="categories__list"><b>{{$category->name}}: </b>
                    <ul class="menu--vertical">
            @foreach($category->children as $cat)
                        <li class="menu-item"> <a href="#">{{$cat->name}}</a>
                        </li>
            @endforeach
                    </ul>
                </div>
@endforeach
            </div>