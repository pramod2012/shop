<div class="section-slide--default">
            <div class="owl-carousel" data-owl-auto="false" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="0" data-owl-nav="true" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1" data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000" data-owl-mousedrag="on">
@foreach(\App\Shop\Sliders\Slider::where('status',1)->orderBy('order','asc')->get() as $value)
                <div class="ps-banner"><img class="mobile-only" src="{{asset('storage/'.$value->mobile_image)}}" alt="alt" /><img class="desktop-only" src="{{asset('storage/'.$value->image)}}" alt="alt" />
                    <div class="ps-content">
                        <div class="container">
                            <div class="ps-content-box">
                                {!! $value->body !!}
                            </div>
                        </div>
                    </div>
                </div>
@endforeach
            </div>
            <div class="section-slide__footer">
                <div class="container">
                    <div class="row m-0">
@foreach(\App\Shop\Flashes\Flash::orderBy('order','asc')->get() as $value)
                        <div class="col-4">
                            <p><a href="//{{$value->link}}"><b>{{$value->name}}</b></a></p>
                        </div>
@endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="ps-promotion--default">
            <div class="container">
                <div class="row m-0">
@foreach(\App\Shop\Promotions\Promotion::where('category_id',null)->orderBy('order','asc')->where('status',1)->where('feature',1)->get() as $value)
                    <div class="col-6 col-lg-3">
                        @if($value->link == !null)
                        <a href="//{{$value->link}}">@endif
                            <img src="{{asset('storage/'.$value->image)}}" alt="alt" />
                            @if($value->link == !null)
                        </a>@endif
                    </div>
@endforeach
                </div>
            </div>
        </div>