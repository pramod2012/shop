<div class="ps-footer-mobile">
        <div class="menu__content">
            <ul class="menu--footer">
                <li class="nav-item"><a class="nav-link" href="{{route('index')}}">
                    <i class="icon-home3"></i>
                    <span>Home</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link footer-category" href="javascript:void(0);">
                        <i class="icon-list"></i><span>Category</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link footer-cart" href="{{url('/cart')}}">
                        <i class="icon-cart"></i>
                        <div id="count1">
                            <span class="badge bg-warning">{{$cartCount}}</span>
                        </div>
                        
                        <span>Cart</span>
                    </a>
                </li>
                @if(auth()->check())
                <li class="nav-item">
                    <a class="nav-link" href="/accounts?tab=profile">
                        <i class="icon-user"></i>
                        <span>Account</span>
                    </a>
                </li>
                @else
                <li class="nav-item">
                    <a class="nav-link" href="{{route('login')}}">
                        <i class="icon-user"></i>
                        <span>Login</span>
                    </a>
                </li>
                @endif
            </ul>
        </div>
    </div>
    <button class="btn scroll-top"><i class="icon-chevron-up"></i></button>
    
    <div class="ps-category--mobile">
        <div class="category__header">
            <div class="category__title">All Categories</div><span class="category__close"><i class="icon-cross"></i></span>
        </div>
        <div class="category__content">
            <ul class="menu--mobile">
@foreach(\App\Shop\Categories\Category::where('status',1)->orderBy('order','asc')->get() as $value)
                <li class="menu-item-has-children">
                    <a @if($value->children->count() > 0) href="{{route('category',$value->slug)}}" @else href="{{route('category',$value->slug)}}" @endif>{{$value->name}}</a>
                    @if($value->children->count() > 0)
                    <span class="sub-toggle"><i class="icon-chevron-down"></i></span>

                    <ul class="sub-menu">@endif
                    @foreach($value->children as $cat)
                        <li>
                            <a href="{{route('category',$cat->slug)}}">{{$cat->name}}
                            </a>
                        </li>
                    @endforeach
                        @if($value->children->count() > 0)
                    </ul>@endif
                </li>
@endforeach          
            </ul>
        </div>
    </div>
    <nav class="navigation--mobile">
        <div class="navigation__header">
            <div class="navigation__select">
                <div class="languages"><a class="nav-top-link" href="javascript:void(0);"> <span class="current-languages">English</span><i class="icon-chevron-down"></i></a>
                    
                </div>
                
            </div>
            <div class="navigation-title">
                <button class="close-navbar-slide"><i class="icon-arrow-left"></i></button>
                <div>@if(auth()->check())
                    <span> <i class="icon-user"></i>Hi, </span>
                    <span class="account">{{auth()->user()->name}}</span>
                    @else
                    <span><a href="/login">Login</a></span>

                    @endif
                </div>
            </div>
        </div>
        <div class="navigation__content">
            <ul class="menu--mobile">
@foreach(\App\Shop\Categories\Category::where('status',1)->orderBy('order','asc')->get() as $value)
                <li class="menu-item-has-children">
                    <a @if($value->children->count() > 0) href="{{route('category',$value->slug)}}" @else href="{{route('category',$value->slug)}}" @endif>{{$value->name}}</a>
                    @if($value->children->count() > 0)
                    <span class="sub-toggle"><i class="icon-chevron-down"></i></span>

                    <ul class="sub-menu">@endif
                    @foreach($value->children as $cat)
                        <li>
                            <a href="{{route('category',$cat->slug)}}">{{$cat->name}}
                            </a>
                        </li>
                    @endforeach
                        @if($value->children->count() > 0)
                    </ul>@endif
                </li>
@endforeach          
            </ul>
        </div>
        
    </nav>