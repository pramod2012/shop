<footer class="ps-footer">
        <div class="container">
            <div class="ps-footer--contact">
                <div class="row">
                    <div class="col-12 col-lg-4">
                        <p class="contact__title">Contact Us</p>
                        <p><b><i class="icon-telephone"> </i>Phone: </b></p>
                        <p class="telephone">{{config('settings.phone')}}</p>
                        <p> <b>Head office: </b>{{config('settings.location')}}</p>
                        <p> <b>Email us: </b>{{config('settings.email')}}</p>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="row">
                            <div class="col-12 col-lg-6">
                                <p class="contact__title">Useful Links<span class="footer-toggle"><i class="icon-chevron-down"></i></span></p>
                                <ul class="footer-list">
                                    <li><a href="{{route('login')}}">Login</a></li>
                                    <li><a href="{{route('register')}}">Sign Up</a></li>
        @foreach(\App\Shop\Posts\Post::where('is_blog',0)->orderBy('order','asc')->get() as $value)
                                    <li> <a href="{{route('page.show',$value->slug)}}">{{$value->name}}</a>
                                    </li>@endforeach
                                </ul>
                                <hr>
                            </div>
                            
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="ps-footer--service">
                        <div class="service--block">
                            <p class="service__item"> <i class="icon-speed-fast"></i><span> <b>Fast Delivery </b>& Shipping</span></p>
                            <p class="service__item"> <i class="icon-color-sampler"></i><span>Top <b>Offers</b></span></p>
                            <p class="service__item"> <i class="icon-wallet"></i><span>Money <b>Cashback</b></span></p>
                            <p class="service__item"> <i class="icon-bubble-user"></i><span>Friendly <b>Support 24/7</b></span></p>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
            <div class="row ps-footer__copyright">
                <div class="col-12 col-lg-6 ps-footer__text">{{config('settings.footer_text')}}</div>
                <div class="col-12 col-lg-6 ps-footer__social">
                    <a class="icon_social facebook" href="//{{config('settings.facebook')}}">
                        <i class="fa fa-facebook-f"></i>
                    </a>
                    <a class="icon_social twitter" href="//{{config('settings.twitter')}}">
                        <i class="fa fa-twitter"></i>
                    </a>
                </div>
            </div>
        </div>
    </footer>