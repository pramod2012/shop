<div class="categories--floating">
@foreach(\App\Shop\Categories\Category::where('parent_id',null)->where('status',1)->orderBy('order','asc')->get() as $value)
	<a class="floating-item" href="#{{$value->slug}}Block">
		{!! $value->icon !!}
	</a>
@endforeach
</div>