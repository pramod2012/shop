@section('title')
    Complete Order | {{config('settings.site_title')}}
@endsection

@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="ps-breadcrumb__list">
                    <li class="active"><a href="{{route('index')}}">Home</a></li>
                    <li class="active"><a href="{{url('cart')}}">Shopping Cart</a></li>
                    <li class="active"><a href="{{url('checkout')}}">Checkout</a></li>
                    <li><a href="{{url('checkout')}}">Complete Order</a></li>
                </ul>
            </div>
        </div>
        <section class="section--checkout">
            <div class="container">
                <h2 class="page__title">Complete Order</h2>
                
                <div class="checkout__content">
                    <div class="row">
                        <div class="col-12 col-lg-7">
                            
                            <h3 class="checkout__title">Your Order </h3>
                            <div class="checkout__products">
                                <div class="row">
                                    <div class="col-8">
                                        <div class="checkout__label">PRODUCT</div>
                                    </div>
                                    <div class="col-4 text-right">
                                        <div class="checkout__label">TOTAL</div>
                                    </div>
                                </div>
                                <div class="checkout__list">
                                @foreach($cartItems as $cartItem)
                                    <div class="checkout__product__item">
                                        <div class="checkout-product">
                                            <div class="product__name">{{ $cartItem->name }}<span>(x{{ $cartItem->qty }})</span></div>
                                            <div class="product__unit"></div>
                                        </div>
                                        <div class="checkout-price">{{config('cart.currency')}} {{ number_format($cartItem->price, 2) }}</div>
                                    </div>
                                @endforeach
                                </div>
                                <div class="row">
                                    <div class="col-8">
                                        <div class="checkout__label">Subtotal</div>
                                    </div>
                                    <div class="col-4 text-right">
                                        <div class="checkout__label">{{config('cart.currency')}} {{ number_format($subtotal) }}</div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-8">
                                        <div class="checkout__label">Shipping Fee</div>
                                    </div>
                                    <div class="col-4 text-right">
                                        <div class="checkout__label">{{ config('cart.currency_symbol') }} {{ $shipping }}</div>
                                    </div>
                                </div>
                                <hr>
                                @if(isset($shippingFee) && $shippingFee != 0)
                                <div class="checkout__label">Shipping</div>
                                <p>{{config('cart.currency')}} {{ $shippingFee }}</p>
                                @endif
                                <div class="row">
                                    <div class="col-8">
                                        <div class="checkout__total">Total</div>
                                    </div>
                                    <div class="col-4 text-right">
                                        <div class="checkout__money">{{config('cart.currency')}} {{ number_format($total) }}</div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-12 col-lg-5">
                            <h3 class="checkout__title">Complete your order</h3>
                            <form action="{{ route('bank-transfer.store') }}" method="POST">
                                {{ csrf_field() }}
                            <input type="hidden" id="billing_address" name="billing_address" value="{{ $billingAddress }}">
                            <input type="hidden" name="shipment_obj_id" value="{{ $shipmentObjId }}">
                            <input type="hidden" name="payment" value="{{ $paymentMethod }}">
                            @if(request()->has('courier'))
                                    <input type="hidden" name="courier" value="{{ request()->input('courier') }}">
                            @endif
                            <div class="checkout__payment">
                                <div class="checkout__label mb-3">PAYMENT METHOD</div>
                                @if($paymentMethod == 'bank transfer')
                                <div class="form-group--block">
                                    <input class="form-check-input" type="checkbox" name="payment_method" id="checkboxBank" value="bank transfer" checked>
                                    <label class="label-checkbox" for="checkboxBank"><b class="text-heading">Direct bank transfer</b></label>
                                </div>
                                <div class="checkout__payment__detail">
                                	<h3>Bank Name: {{ config('bank-transfer.bank_name') }}</h3><hr>
                                	<p>Account Type: {{ config('bank-transfer.account_type') }}</p>
                                	<p>Account Name:{{ config('bank-transfer.account_name') }}</p>
                                	<p>Accont Number:{{ config('bank-transfer.account_number') }}</p>
                                	<div class="triangle-box">
                                        <div class="triangle"></div>
                                    </div>
                                </div>
                                @endif
                                @if($paymentMethod == 'cash on delivery')
                                <div class="form-group--block">
                                    <input class="form-check-input" type="checkbox" name="payment_method" value="cash on delivery" id="checkboxCash" checked="checked">
                                    <label class="label-checkbox" for="checkboxCash"><b class="text-heading">Cash on delivery</b></label>
                                </div>
                                <div class="checkout__payment__detail">Pay with cash upon delivery <div class="triangle-box">
                                        <div class="triangle"></div>
                                    </div>
                                </div>
                                @endif
                            </div>
                            <p>Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our <span class="text-success">privacy policy.</span></p>
                            <div class="form-group--block">
                                <input class="form-check-input" type="checkbox" id="checkboxAgree" value="agree" checked required>
                                <label class="label-checkbox" for="checkboxAgree"><b class="text-heading">I have read and agreed to the website <u class="text-success">terms and conditions </u><span>*</span></b></label>
                            </div>
                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn ps-button">Order Now</button>
                            </form>
                        </div>
                    </div>
                    <div class="checkout__header">
                        <div class="row">
                            <div class="col-12 col-lg-7">
                                <div class="checkout__header__box">
                                    <p><i class="icon-user"></i>Returning customer? <a href="{{route('login')}}">Click here to login</a></p><i class="icon-user"></i>
                                </div>
                            </div>
                            <div class="col-12 col-lg-5">
                                <div class="checkout__header__box">
                                    <p><i class="icon-users"></i>New Member? <a href="{{route('register')}}">Register Here</a></p><i class="icon-register"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </section>
    </main>
@include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection