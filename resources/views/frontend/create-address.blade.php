@section('title')
    Create Address | {{config('settings.site_title')}}
@endsection
@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="ps-breadcrumb__list">
                    <li><a href="{{route('index')}}">Home</a></li>
                    <li class="active"><a href="javascript:void(0);">Create Address</a></li>
                </ul>
            </div>
        </div>
        <section class="section--registration">
            <div class="container">
                <h2 class="page__title">Create Address</h2>
                
                <div class="registration__content">
                    <div class="row">
                        <div class="col-12 col-lg-10">
                            <div class="registration__info">
                                <h3 class="registration__title">Address Details</h3>
                                <form action="{{ route('customer.address.store', $customer->id) }}" method="POST" role="form">
                                    @csrf
                                    <input type="hidden" name="status" value="1">
                                    <div class="form-row">
                                        <div class="col-12 col-lg-6 form-group--block">
                                            <label>Alias: <span>*</span></label>
                                            <input name="alias" class="form-control @if ($errors->has('alias')) is-invalid @endif" type="text" placeholder="home or office">
                                            @if ($errors->has('alias'))
                                            <div class="invalid-feedback">{{ $errors->first('alias') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-12 col-lg-6 form-group--block">
                                            <label>Address 1<span>*</span></label>
                                            <input name="address_1" class="form-control @if ($errors->has('address_1')) is-invalid @endif" type="text">
                                    @if ($errors->has('address_1'))
                                            <div class="invalid-feedback">{{ $errors->first('address_1') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-12 col-lg-6 form-group--block">
                                            <label>Address 2<span>*</span></label>
                                            <input name="address_2" class="form-control @if ($errors->has('address_2')) is-invalid @endif" type="text">
                                    @if ($errors->has('address_2'))
                                            <div class="invalid-feedback">{{ $errors->first('address_2') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-12 col-lg-6 form-group--block">
                                            <label>Country: </label>
                                            <select class="single-select2" name="country_id">
                                                @foreach($countries as $country)
                                					<option @if(env('SHOP_COUNTRY_ID') == $country->id) selected="selected" @endif value="{{ $country->id }}">{{ $country->name }}</option>
                            					@endforeach
                                            </select>
                                            @if ($errors->has('country_id'))
                                            <div class="invalid-feedback">{{ $errors->first('country_id') }}</div>
                                            @endif
                                        </div>
                                        
                                        <div class="col-12 col-lg-6 form-group--block">
                                            <label>Zip Code<span>*</span></label>
                                            <input name="zip" class="form-control @if ($errors->has('zip')) is-invalid @endif" type="text">
                                    @if ($errors->has('zip'))
                                            <div class="invalid-feedback">{{ $errors->first('zip') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-12 col-lg-6 form-group--block">
                                            <label>Your Phone<span>*</span></label>
                                            <input name="phone" class="form-control @if ($errors->has('phone')) is-invalid @endif" type="text">
                                    @if ($errors->has('phone'))
                                            <div class="invalid-feedback">{{ $errors->first('phone') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-12 form-group--block">
                                        <label> </label>
                                        
                                            <button type="submit" class="btn ps-button">Create</button>
                                        </div>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </main>
@include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection
    