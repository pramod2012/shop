@section('title')
    Checkout Items | {{config('settings.site_title')}}
@endsection

@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="ps-breadcrumb__list">
                    <li class="active"><a href="{{route('index')}}">Home</a></li>
                    <li class="active"><a href="{{url('cart')}}">Shopping Cart</a></li>
                    <li><a href="{{url('checkout')}}">Checkout</a></li>
                </ul>
            </div>
        </div>
        <section class="section--checkout">
            <div class="container">
                <h2 class="page__title">Checkout</h2>
                @if(!$products->isEmpty())
                @if(count($addresses) > 0)
                <div class="checkout__content">
                    <div class="row">
                        <div class="col-12 col-lg-7">
                            
                            <h3 class="checkout__title">Your Order </h3>
                            <div class="checkout__products">
                                <div class="row">
                                    <div class="col-8">
                                        <div class="checkout__label">PRODUCT</div>
                                    </div>
                                    <div class="col-4 text-right">
                                        <div class="checkout__label">TOTAL</div>
                                    </div>
                                </div>
                                <div class="checkout__list">
                                @foreach($cartItems as $cartItem)
                                    <div class="checkout__product__item">
                                        <div class="checkout-product">
                                            <div class="product__name">{{ $cartItem->name }}<span>(x{{ $cartItem->qty }})</span></div>
                                            <div class="product__unit"></div>
                                        </div>
                                        <div class="checkout-price">{{config('cart.currency')}} {{ number_format($cartItem->price, 2) }}</div>
                                    </div>
                                @endforeach
                                </div>
                                <div class="row">
                                    <div class="col-8">
                                        <div class="checkout__label">Subtotal</div>
                                    </div>
                                    <div class="col-4 text-right">
                                        <div class="checkout__label">{{config('cart.currency')}} {{ number_format($subtotal) }}</div>
                                    </div>
                                </div>
                                <hr>
                                @if(isset($shippingFee) && $shippingFee != 0)
                                <div class="checkout__label">Shipping</div>
                                <p>{{config('cart.currency')}} {{ $shippingFee }}</p>
                                @endif
                                <div class="row">
                                    <div class="col-8">
                                        <div class="checkout__total">Total</div>
                                    </div>
                                    <div class="col-4 text-right">
                                        <div class="checkout__money">{{config('cart.currency')}} {{ number_format($total) }}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="checkout__form">
                                <h3 class="checkout__title">Your billing Details </h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="ps-social--share">
                                        <a href="{{ route('customer.address.create', auth()->user()->id) }}" class="ps-social__icon twitter"><i class="fa fa-plus"></i> Create Address </a>
                                    </div>
                                </div>
                            </div>
                            <table class="table">
                                <thead class="thead-light">
                                    <tr>
                                        <th><b class="text-black">Alias</b></th>
                                        <th><b class="text-black">Address</b></th>
                                        <th><b class="text-black">Actions</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($addresses as $address)
                                        <tr class="deleteAddressRow{{$address->id}}">
                                            <td>{{$address->alias}}</td>
                                            <td>{{$address->address_1}} {{$address->address_2}} {{ $address->country->name}} {{$address->zip}} {{$address->phone}} </td>
                                            <td>
                                                
                                                    <div class="btn-group">
                                                        <div class="ps-social--share">
                                                            <a href="{{ route('customer.address.edit', [auth()->user()->id, $address->id]) }}" class="ps-social__icon facebook">
                                                                <i class="fa fa-pencil"></i><span>Edit</span>
                                                            </a>
                                                            <a id="{{auth()->user()->id}}" address_id="{{$address->id}}" class="ps-social__icon deleteAddress" type="submit">
                                                                <i class="icon-trash2"></i><span>Delete</span>
                                                            </a>
                                                            
                                                        </div>
                                                    
                                                        
                                                    </div>
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                        </div>
                        <div class="col-12 col-lg-5">
                            <h3 class="checkout__title">Complete your order</h3>
                            <form action="{{ route('bank-transfer.index') }}" method="">
                            <div class="checkout__products">
                                <div class="shopping-cart__block">
                                    <div class="checkout__label mb-3">SELECT DELIVERY ADDRESS</div>
                                    @if(count($addresses) > 0)
                                    <div class="input-group">
                                        <select class="single-select2" name="billing_address">
                                        @foreach($addresses as $address)
                                            <option value="{{$address->id}}">{{$address->address_1}} {{$address->address_2}} {{ $address->country->name}} {{$address->zip}} {{$address->phone}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                    @else
                                        <p class="alert alert-danger"><a href="{{ route('customer.address.create', [$customer->id]) }}">No address found. Create  the billing details in order to complete the order.</a></p>
                                    @endif
                                </div>
                            </div>
                            <div class="checkout__payment">
                                <div class="checkout__label mb-3">SELECT PAYMENT METHOD</div>
                                <div class="form-group--block">
                                    <input class="form-check-input" type="checkbox" name="payment_method" id="checkboxBank" value="bank transfer">
                                    <label class="label-checkbox" for="checkboxBank"><b class="text-heading">Direct bank transfer</b></label>
                                </div>
                                <div class="form-group--block">
                                    <input class="form-check-input" type="checkbox" name="payment_method" value="cash on delivery" id="checkboxCash" checked="checked">
                                    <label class="label-checkbox" for="checkboxCash"><b class="text-heading">Cash on delivery</b></label>
                                </div>
                                <div class="checkout__payment__detail">Pay with cash upon delivery <div class="triangle-box">
                                        <div class="triangle"></div>
                                    </div>
                                </div>
                            </div>
                            <p>Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our <span class="text-success">privacy policy.</span></p>
                            <div class="form-group--block">
                                <input class="form-check-input" type="checkbox" id="checkboxAgree" value="agree" required>
                                <label class="label-checkbox" for="checkboxAgree"><b class="text-heading">I have read and agree to the website <u class="text-success">terms and conditions </u><span>*</span></b></label>
                            </div>
                            <button type="submit" class="btn ps-button">Place an order</button>
                            </form>
                        </div>
                    </div>
                    <div class="checkout__header">
                        <div class="row">
                            <div class="col-12 col-lg-7">
                                <div class="checkout__header__box">
                                    <p><i class="icon-user"></i>Returning customer? <a href="{{route('login')}}">Click here to login</a></p><i class="icon-user"></i>
                                </div>
                            </div>
                            <div class="col-12 col-lg-5">
                                <div class="checkout__header__box">
                                    <p><i class="icon-users"></i>New Member? <a href="{{route('register')}}">Register Here</a></p><i class="icon-register"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @else
                <p class="alert alert-danger"><a href="{{ route('customer.address.create', [$customer->id]) }}">No address found. Create  the billing details in order to complete the order.</a></p>
                @endif
                @else
                <div class="row">
                    <div class="col-md-12">
                        <p class="alert alert-warning">No products in cart yet. <a href="{{ route('index') }}">Shop now!</a></p>
                    </div>
                </div>
                @endif
            </div>
        </section>
    </main>
@include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection