@section('title')
    Wishlist | {{config('settings.site_title')}}
@endsection
@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="ps-breadcrumb__list">
                    <li class="active"><a href="{{route('index')}}">Home</a></li>
                    <li><a href="javascript:void(0);">Wish List</a></li>
                </ul>
            </div>
        </div>
        <section class="section--shopping-cart">
            <div class="container shopping-container">
                <h2 class="page__title">Wish List</h2>

                @if($items->count() > 0)
                <div class="shopping-cart__content">
                    <div class="row m-0">
                        <div class="col-12 col-lg-10">
                            <div class="shopping-cart__products">
                                <div class="shopping-cart__table">
                                    <div class="shopping-cart-light">
                                        <div class="shopping-cart-row">
                                            <div class="cart-product"></div>
                                            <div class="cart-price">Price</div>
                                            <div class="cart-action"> Action</div>
                                        </div>
                                    </div>
                                    <div class="shopping-cart-body">
                                   @foreach($items as $cartItem)
                                        <div class="shopping-cart-row deletewishlist{{$cartItem->rowId}}">
                                            <div class="cart-product">
                                                <div class="ps-product--mini-cart">
                                                	<a href="{{ route('product', [$cartItem->product->slug]) }}">
                                                        @if(isset($cartItem->cover))
                                                        <img class="ps-product__thumbnail" src="{{$cartItem->cover}}" alt="alt" />
                                                    @else
                                                        <img src="https://placehold.it/120x120" alt="" class="img-responsive img-thumbnail">
                                                    @endif
                                                	</a>
                                                    <div class="ps-product__content">
                                                        <h5><a class="ps-product__name" href="{{ route('product', [$cartItem->product->slug]) }}">{{$cartItem->name}}</a></h5>
                                           
                                                        <p class="ps-product__meta">Price: <span class="ps-product__price">{{config('cart.currency')}} {{ number_format($cartItem->price, 2) }}</span></p>
                                                        
                                                    </div>
                                                    <div class="ps-product__remove">
                                                        <a id="{{$cartItem->rowId}}" class="wishlisttrash" type="submit"><i class="icon-trash2"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="cart-price"><span class="ps-product__price">{{config('cart.currency')}} {{ number_format($cartItem->price) }}</span>
                                            </div>
                                            <div class="cart-action">
                                                <a id="{{$cartItem->rowId}}" class="wishlisttrash" type="submit"><i class="icon-trash2"></i></a>
                                         </div>
                                        </div>
                                        @endforeach
                                     
                                        
                                    </div>
                                </div>
                                
                                
                            </div>
                        </div>

                    @else


            <div class="row">
                <div class="col-md-12">
                    <p class="alert alert-warning">No products in wishlist yet. <a href="{{ route('index') }}">Show now!</a></p>
                </div>
            </div>

            @endif


                        
            </div>
        </section>
    </main>
@include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection
    