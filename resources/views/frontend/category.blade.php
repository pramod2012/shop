@section('title')
@if($category->meta_title == !null)
    {{$category->meta_title}}
@else
    {{$category->name}}
@endif
@endsection
@section('meta-tags')
@if($category->meta_desc == !null)
<meta name="description" content="{{$category->meta_desc}}">@endif
@if($category->meta_keywords == !null)
<meta name="keywords" content="{{$category->meta_keywords}}">@endif
<meta property="og:title" content="{{$category->name}}" />
<meta property='og:image' content="{{asset('storage/'.$category->cover)}}"/>
@if($category->meta_desc == !null)
<meta property='og:description' content="{{$category->meta_desc}}"/>@endif
<meta property='og:url' content="{{app('request')->url()}}">
<meta property="og:type" content="website"/>
<meta property="og:site_name" content="{{app('request')->url()}}"/>
<meta name="twitter:card" content="summary_large_image" />
@if($category->meta_title == !null)
<meta name="twitter:title" content="{{$category->meta_title}}" />@endif
@if($category->meta_desc == !null)
<meta name="twitter:description" content="{{$category->meta_desc}}" />@endif
<meta name="twitter:image" content="{{asset('storage/'.$category->cover)}}" />
@endsection

@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="ps-breadcrumb__list">
                    <li class="active"><a href="{{route('index')}}">Home</a></li>
                    <li><a href="javascript:void(0);">Shop</a></li>
                </ul>
            </div>
        </div>
        <section class="section-shop">
            <div class="container">
                <div class="shop__content">
                    <div class="row">
                        <div class="col-12 col-lg-3">
                            <div class="ps-shop--sidebar">
                                <div class="sidebar__category">
                                    <div class="sidebar__title">ALL CATEGORIES</div>
                                    <ul class="menu--mobile">
@foreach(\App\Shop\Categories\Category::where('status',1)->orderBy('order','asc')->get() as $value)
                                        <li class="menu-item-has-children category-item">
                                        	<a href="{{route('category',$value->slug)}}">{{$value->name}}</a>
                                            @if($value->children->count() > 0)
                                            <span class="sub-toggle">
                                                <i class="icon-chevron-down"></i>
                                            </span>
                                            
                                            <ul class="sub-menu">
                                     @foreach($value->children as $cat)
                                                <li> <a href="{{route('category',$cat->slug)}}">{{$cat->name}}</a>
                                                </li>
                                     @endforeach
                                                
                                            </ul>
                                            @endif
                                    
                                        </li>
@endforeach


                                    </ul>
                                </div>
                @include('frontend.includes.sidebar-search')
                                
                            </div>
                        </div>
                        <div class="col-12 col-lg-9">    
                            <div class="result__header">
                                <h4 class="title">{{$products->count()}}<span>Product Found in {{$category->name}}</span></h4>
                                
                                <div class="page">page
                                    <input type="text" value="{{$products->currentPage()}}" disabled>of {{ $products->lastPage() }}
                                </div>
                                
                            </div>
                            <div class="filter__mobile">
                                <div class="viewtype--block">
                                    <div class="viewtype__sortby">
                                        <div class="select">
                                            <select class="single-select2-no-search" name="state" onchange="location = this.value;">
                                                <option value="{{$category->slug}}?views=asc" @if(Request::get('views') == 'asc') selected @endif>Sort by popularity</option>
                                                
                                                <option value="{{$category->slug}}?views=desc" @if(Request::get('views') == 'desc') selected @endif>View Desc</option>
                                                <option value="{{$category->slug}}?price=desc" @if(Request::get('price') == 'desc') selected @endif>Price High to Low</option>
                                                <option value="{{$category->slug}}?price=asc" @if(Request::get('price') == 'asc') selected @endif>Price Low to High</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="viewtype__select"> <span class="text ps-mobile-filter"><i class="icon-equalizer"></i>Filter</span>
                                        <div class="type"><a href="/category/{{$category->slug}}?sort=grid"><span class="active"><i class="icon-icons"></i></span></a><a href="/category/{{$category->slug}}?sort=list"><span><i class="icon-list4"></i></span></a></div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="result__sort">
                                <div class="viewtype--block">
                                    <div class="viewtype__sortby">
                                        <div class="select">
                                            <select class="single-select2-no-search" name="state" onchange="location = this.value;">
                                                <option value="{{$category->slug}}?views=asc" @if(Request::get('views') == 'asc') selected @endif>Sort by popularity</option>
                                                
                                                <option value="{{$category->slug}}?views=desc" @if(Request::get('views') == 'desc') selected @endif>View Desc</option>
                                                <option value="{{$category->slug}}?price=desc" @if(Request::get('price') == 'desc') selected @endif>Price High to Low</option>
                                                <option value="{{$category->slug}}?price=asc" @if(Request::get('price') == 'asc') selected @endif>Price Low to High</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="viewtype__select">
                                        <div class="type">
                                            <a href="/category/{{$category->slug}}?sort=grid">
                                                <span><i class="icon-icons"></i></span>
                                            </a>
                                            
                                            <a href="/category/{{$category->slug}}?sort=list">
                                                <span>
                                                    <i class="icon-list4"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="result__header mobile">
                                <h4 class="title">{{$products->count()}}<span>Product Found</span></h4>
                            </div>
                            <div class="result__content">
                    
                    @if(Request::get('sort') == 'list')
                            @include('frontend.category.list')
                    @else
                            @include('frontend.category.grid')
                    @endif
                                <div class="ps-pagination blog--pagination">
        
                                    {{$products->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        
        </div>
    </main>
@include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection