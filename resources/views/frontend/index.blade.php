@section('title')
{{config('settings.site_title')}}
@endsection
@section('meta-tags')
<meta name="description" content="{{config('settings.seo_meta_description')}}">
<meta property='og:image' content="{{asset('storage/'.config('settings.contactus_image'))}}"/>
<meta property='og:description' content="{{config('settings.seo_meta_description')}}"/>
<meta property='og:url' content="{{app('request')->url()}}">
<meta property="og:type" content="website"/>
<meta property="og:site_name" content="{{app('request')->url()}}"/>
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:title" content="{{config('settings.site_title')}}" />
<meta name="twitter:description" content="{{config('settings.seo_meta_description')}}" />
<meta name="twitter:image" content="{{asset('storage/'.config('settings.contactus_image'))}}" />
@endsection

@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
       @include('frontend.includes.slider') 
        @include('frontend.includes.featured-categories')
        
        <section class="section-categories--default">
            <div class="container">
                @include('frontend.includes.categories-block-content')
            </div>
        </section>
        @include('frontend.includes.recommendations')
        @include('frontend.includes.recently-viewed')
        
    </main>
@include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection