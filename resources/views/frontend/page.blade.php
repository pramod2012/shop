@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
        <section class="section--single-post">
            
            <div class="container">
                <div class="post__header">
                    <div class="ps-breadcrumb">
                        <div class="container">
                            <ul class="ps-breadcrumb__list">
                                <li class="active"><a href="{{route('index')}}">Home</a></li>
                                <li class="active"><a href="javascript:void(0);">{{$page->name}}</a></li>
                            </ul>
                        </div>
                    </div>
                    <h2 class="post__title">{{$page->name}}</h2>
                    <div class="post__archives">
                    	
                    	<span class="archive__item">{{$page->created_at->format('F j, Y')}}</span>

                        
                    </div>
                </div>
                
                <div class="post__content">
                    <div class="post__blog">
                        <p><?php
$str = $page->body;
echo html_entity_decode($str);
?> </p>
                        
                    </div>
                    
                        
                        
                    </div>
                    
                    
                    
                    
                    
                </div>
            </div>
            
        </section>
        
    </main>

@include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection
    