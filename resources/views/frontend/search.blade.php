@section('title')
    Search Products | {{config('settings.site_title')}}
@endsection

@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="ps-breadcrumb__list">
                    <li class="active"><a href="{{route('index')}}">Home</a></li>
                    <li><a href="javascript:void(0);">Shop</a></li>
                </ul>
            </div>
        </div>
        <section class="section-shop">
            <div class="container">
                <div class="shop__content">
                    <div class="row">
                        <div class="col-12 col-lg-3">
                            <div class="ps-shop--sidebar">
                                <div class="sidebar__category">
                                    <div class="sidebar__title">ALL CATEGORIES</div>
                                    <ul class="menu--mobile">
@foreach(\App\Shop\Categories\Category::where('status',1)->orderBy('order','asc')->get() as $value)
                                        <li class="menu-item-has-children category-item">
                                            <a href="{{route('category',$value->slug)}}">{{$value->name}}</a>
                                            @if($value->children->count() > 0)
                                            <span class="sub-toggle">
                                                <i class="icon-chevron-down"></i>
                                            </span>
                                            
                                            <ul class="sub-menu">
                                     @foreach($value->children as $cat)
                                                <li> <a href="{{route('category',$cat->slug)}}">{{$cat->name}}</a>
                                                </li>
                                     @endforeach
                                                
                                            </ul>
                                            @endif
                                    
                                        </li>
@endforeach


                                    </ul>
                                </div>
                @include('frontend.includes.sidebar-mainsearch')
                                
                            </div>
                        </div>
                        <div class="col-12 col-lg-9">
                                
                            
                            
                            <div class="result__header">
                                <h4 class="title">{{$products->count()}}<span>Product Found</span></h4>
                                
                                <div class="page">page
                                    <input type="text" value="{{$products->currentPage()}}" disabled>of {{ $products->lastPage() }}
                                </div>
                                
                            </div>
                            <div class="filter__mobile">
                                <div class="viewtype--block">
                                    <div class="viewtype__sortby">
                                        <div class="select">
                                            <select class="single-select2-no-search" name="state" onchange="location = this.value;">
                                                <option value="{{ route('search',['views' =>'asc', 'category_id' => Request::get('category_id'), 'min_price' => Request::get('min_price'), 'max_price' => Request::get('max_price'), 'name' => Request::get('name')]) }}" @if(Request::get('views') == 'asc') selected @endif>Sort by popularity</option>
                                                
                                                <option value="{{ route('search',['views' =>'desc', 'category_id' => Request::get('category_id'), 'min_price' => Request::get('min_price'), 'max_price' => Request::get('max_price'), 'name' => Request::get('name')]) }}" @if(Request::get('views') == 'desc') selected @endif>View Desc</option>
                                                <option value="{{ route('search',['price' =>'desc','sort'=>Request::get('sort'), 'category_id' => Request::get('category_id'), 'min_price' => Request::get('min_price'), 'max_price' => Request::get('max_price'), 'name' => Request::get('name')]) }}" @if(Request::get('price') == 'desc') selected @endif>Price High to Low</option>
                                                <option value="{{ route('search',['price' =>'asc','sort'=>Request::get('sort'), 'category_id' => Request::get('category_id'), 'min_price' => Request::get('min_price'), 'max_price' => Request::get('max_price'), 'name' => Request::get('name')]) }}" @if(Request::get('price') == 'asc') selected @endif>Price Low to High</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="viewtype__select"> <span class="text ps-mobile-filter"><i class="icon-equalizer"></i>Filter</span>
                                        <div class="type">
                                            <a href="{{ route('search',['sort'=>'grid' , 'views' =>Request::get('views'), 'price' => Request::get('price'), 'category_id' => Request::get('category_id'), 'min_price' => Request::get('min_price'), 'max_price' => Request::get('max_price'), 'name' => Request::get('name')]) }}">
                                                <span class="active"><i class="icon-icons"></i></span>
                                            </a>
                                            <a href="{{ route('search',['sort'=>'list' , 'views' =>Request::get('views'), 'price' => Request::get('price'), 'category_id' => Request::get('category_id'), 'min_price' => Request::get('min_price'), 'max_price' => Request::get('max_price'), 'name' => Request::get('name')]) }}">
                                                <span><i class="icon-list4"></i></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="result__sort">
                                <div class="viewtype--block">
                                    <div class="viewtype__sortby">
                                        <div class="select">
                                            <select class="single-select2-no-search" name="state" onchange="location = this.value;">
                                                <option value="{{ route('search',['views' =>'asc','sort'=>Request::get('sort'), 'category_id' => Request::get('category_id'), 'min_price' => Request::get('min_price'), 'max_price' => Request::get('max_price'), 'name' => Request::get('name')]) }}" @if(Request::get('views') == 'asc') selected @endif>Sort by popularity</option>
                                                
                                                <option value="{{ route('search',['views' =>'desc','sort'=>Request::get('sort'), 'category_id' => Request::get('category_id'), 'min_price' => Request::get('min_price'), 'max_price' => Request::get('max_price'), 'name' => Request::get('name')]) }}" @if(Request::get('views') == 'desc') selected @endif>View Desc</option>
                                                <option value="{{ route('search',['price' =>'desc','sort'=>Request::get('sort'), 'category_id' => Request::get('category_id'), 'min_price' => Request::get('min_price'), 'max_price' => Request::get('max_price'), 'name' => Request::get('name')]) }}" @if(Request::get('price') == 'desc') selected @endif>Price High to Low</option>
                                                <option value="{{ route('search',['price' =>'asc','sort'=>Request::get('sort'), 'category_id' => Request::get('category_id'), 'min_price' => Request::get('min_price'), 'max_price' => Request::get('max_price'), 'name' => Request::get('name')]) }}" @if(Request::get('price') == 'asc') selected @endif>Price Low to High</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="viewtype__select">
                                        <div class="type">
                                            <a href="{{ route('search',['sort'=>'grid' , 'views' =>Request::get('views'), 'price' => Request::get('price'), 'category_id' => Request::get('category_id'), 'min_price' => Request::get('min_price'), 'max_price' => Request::get('max_price'), 'name' => Request::get('name')]) }}">
                                                <span><i class="icon-icons"></i></span>
                                            </a>
                                            
                                            <a href="{{ route('search',['sort'=>'list' , 'views' =>Request::get('views'), 'price' => Request::get('price'), 'category_id' => Request::get('category_id'), 'min_price' => Request::get('min_price'), 'max_price' => Request::get('max_price'), 'name' => Request::get('name')]) }}">
                                                <span>
                                                    <i class="icon-list4"></i>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="result__header mobile">
                                <h4 class="title">{{$products->count()}}<span>Product Found</span></h4>
                            </div>
                            <div class="result__content">
                    
                    @if(Request::get('sort') == 'list')
                            @include('frontend.category.list')
                    @else
                            @include('frontend.category.grid')
                    @endif
                                <div class="ps-pagination blog--pagination">
        
                                    {{$products->links()}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        
        </div>
    </main>
@include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection