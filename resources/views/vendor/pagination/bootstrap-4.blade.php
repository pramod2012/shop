@if ($paginator->hasPages())
    <nav>
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="chevron">
                    <a>
                        <i class="icon-chevron-left"></i>
                    </a>
                </li>
            @else
                <li class="chevron">
                    <a href="{{ $paginator->previousPageUrl() }}">
                        <i class="icon-chevron-left"></i>
                    </a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="page-item disabled" aria-disabled="true"><span class="page-link">{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="active">
                                <a>{{ $page }}</a>
                            </li>
                        @else
                            <li>
                                <a href="{{ $url }}">{{ $page }}</a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="chevron">
                    <a href="{{ $paginator->nextPageUrl() }}">
                        <i class="icon-chevron-right"></i>
                    </a>
                </li>
            @else
                <li class="chevron">
                    <a>
                        <i class="icon-chevron-right"></i>
                    </a>
                </li>
            @endif
        </ul>
    </nav>
@endif
