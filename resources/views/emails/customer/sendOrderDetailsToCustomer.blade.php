<!DOCTYPE html>
<html lang="en-US">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Submit Order To Customer</title>
  <style type="text/css">
  body {margin: 0; padding: 0; min-width: 100%!important;}
  img {height: auto;}
  .content {width: 100%; max-width: 600px;}
  .header {padding: 40px 30px 20px 30px;}
  .innerpadding {padding: 30px 30px 30px 30px;}
  .borderbottom {border-bottom: 1px solid #f2eeed;}
  .subhead {font-size: 15px; color: #ffffff; font-family: sans-serif; letter-spacing: 10px;}
  .h1, .h2, .bodycopy {color: #153643; font-family: sans-serif;}
  .h1 {font-size: 33px; line-height: 38px; font-weight: bold;}
  .h2 {padding: 0 0 15px 0; font-size: 24px; line-height: 28px; font-weight: bold;}
  .bodycopy {font-size: 16px; line-height: 22px;}
  .button {text-align: center; font-size: 18px; font-family: sans-serif; font-weight: bold; padding: 0 30px 0 30px;}
  .button a {color: #ffffff; text-decoration: none;}
  .footer {padding: 20px 30px 15px 30px;}
  .footercopy {font-family: sans-serif; font-size: 14px; color: #ffffff;}
  .footercopy a {color: #ffffff; text-decoration: underline;}

  @media only screen and (max-width: 550px), screen and (max-device-width: 550px) {
  body[yahoo] .hide {display: none!important;}
  body[yahoo] .buttonwrapper {background-color: transparent!important;}
  body[yahoo] .button {padding: 0px!important;}
  body[yahoo] .button a {background-color: #e05443; padding: 15px 15px 13px!important;}
  body[yahoo] .unsubscribe {display: block; margin-top: 20px; padding: 10px 50px; background: #2f3942; border-radius: 5px; text-decoration: none!important; font-weight: bold;}
  }

  /*@media only screen and (min-device-width: 601px) {
    .content {width: 600px !important;}
    .col425 {width: 425px!important;}
    .col380 {width: 380px!important;}
    }*/

  </style>
</head>

<body yahoo bgcolor="#f6f8f1">
<table width="100%" bgcolor="#f6f8f1" border="0" cellpadding="0" cellspacing="0">
<tr>
  <td>
    <!--[if (gte mso 9)|(IE)]>
      <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td>
    <![endif]-->     
    <table bgcolor="#ffffff" class="content" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td bgcolor="#28a745" class="header">
          
          <!--[if (gte mso 9)|(IE)]>
            <table width="425" align="left" cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td>
          <![endif]-->
          <table class="col425" align="left" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 425px;">  
            <tr>
              <td height="70">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="subhead" style="padding: 0 0 0 3px;">
                      Dear {{$customer->name}},
                    </td>
                  </tr>
                  <tr>
                    <td class="h1" style="color: white; padding: 5px 0 0 0;">
                      Welcome to Royal Greens Agro
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
          <!--[if (gte mso 9)|(IE)]>
                </td>
              </tr>
          </table>
          <![endif]-->
        </td>
      </tr>
      <tr>
        <td class="innerpadding borderbottom">
            <p>Hello {{$customer->name}}, </p>

@php($country = \App\Shop\Countries\Country::find($address->country_id))
            <p>This order is for deliver to your: <strong>{{ ucfirst($address->alias) }} <br /></strong></p>
        <p>Address: {{$address->address_1}} {{$address->address_2}} {{$address->city}}, {{$country->name}}, {{$address->zip}}</p>
        <p>{{$address->phone}}</p><br>

            <table border="1" style="width:100%;margin:0 0px 10px 0px;border-collapse:collapse">  

                                   <tbody><tr cellspacing="25">
                                       <td style="text-align:center;font-size:13px">
                                           ID                      
                                       </td>
                                       <td style="text-align:center;font-size:13px">
                                           Name                
                                       </td>
                                       <td style="text-align:center;font-size:13px">
                                           Description                 
                                       </td>
                                       <td style="text-align:center;font-size:13px">
                                           Quantity
                                       </td>
                                       <td style="text-align:center;font-size:13px">
                                           Price            
                                       </td>
                                       <td style="text-align:center;font-size:13px">
                                           Total            
                                       </td>
                                   </tr>
                                   @foreach($products as $product)
                                   <tr cellspacing="15" style="color:red">
                                       <td style="text-align:center;font-size:13px" align="center">
                                          {{$product->id}}
                                       </td>                                
                                       <td style="text-align:center;font-size:13px" align="center">
                                           {{$product->name}}
                                           
                                           {{$product->productattribute_id}}
                                           
                                           @php($pattr = \App\Shop\ProductAttributes\ProductAttribute::find($product->pivot->product_attribute_id))
                                        @if(!is_null($pattr))<br>
                                            @foreach($pattr->attributesValues as $it)
                                                <p class="label label-primary">{{ $it->attribute->name }} : {{ $it->value }}</p>
                                            @endforeach
                                        @endif
                                           
                                          
                                       </td>
                                       <td style="text-align:center;font-size:13px" align="center">
                                           {!!substr($product->description,0,150)!!}..
                                       </td>
                                       <td style="text-align:center;font-size:13px" align="center">
                                           {{$product->pivot->quantity}}
                                       </td>
                                       <td style="text-align:center;font-size:13px" align="center">
                                           @if($product->pivot->productattribute_id == null)
                                           Rs. {{$product->price}}
                                           @else
                                           Rs.{{$pattr->price}}
                                           @endif
                                       </td>
                                       <td style="text-align:center;font-size:13px" align="center">
                                           @if($product->pivot->productattribute_id == null)
                                           Rs. {{number_format($product->price * $product->pivot->quantity, 2)}}
                                           @else
                                           Rs. {{number_format($pattr->price * $product->pivot->quantity, 2)}}
                                           @endif
                                           
                                       </td>                                
                                   </tr>
                                   @endforeach
                               </tbody>

                               </table>
                               <table>

                               <tfoot>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Subtotal:</td>
                <td class="text-right">Rs.{{number_format($order->total_products, 2)}}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Shipping:</td>
                <td class="text-right">Rs.{{number_format($order->total_shipping, 2)}}</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Discounts:</td>
                <td class="text-right">(Rs. {{number_format($order->discounts, 2)}})</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Tax:</td>
                <td class="text-right">Rs. {{number_format($order->tax, 2)}}</td>
            </tr>
            <tr class="bg-warning">
                <td></td>
                <td></td>
                <td></td>
                <td><strong>Total:</strong></td>
                <td class="text-right"><strong>Rs. {{number_format($order->total, 2)}}</strong></td>
            </tr>
            </tfoot></table>

                               

                           



          
        </td>
      </tr>

      
      <tr>
        <td class="innerpadding bodycopy">
            Regards,
            <h3>{{config('settings.company_name')}}</h3>

            DISCLAIMER:
This is a system-generated e-mail. Please do not reply to this e-mail.
        </td>
      </tr>
      <tr>
        <td class="footer" bgcolor="#44525f">
          <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td align="center" class="footercopy">
                &reg; All Rights Reserved, {{ config('app.url') }}<br/>
                <span class="hide">{{config('settings.company_name')}}</span>
              </td>
            </tr>
            <tr>
              <td align="center" style="padding: 20px 0 0 0;">
                <table border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                      <a href="//{{ config('settings.facebook') }}">
                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/210284/facebook.png" width="37" height="37" alt="Facebook" border="0" />
                      </a>
                    </td>
                    <td width="37" style="text-align: center; padding: 0 10px 0 10px;">
                      <a href="http://www.twitter.com/">
                        <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/210284/twitter.png" width="37" height="37" alt="Twitter" border="0" />
                      </a>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <!--[if (gte mso 9)|(IE)]>
          </td>
        </tr>
    </table>
    <![endif]-->
    </td>
  </tr>
</table>
</body>
</html>