@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.categories.store') }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="parent">Parent Category</label>
                        <select name="parent" id="parent" class="form-control select2">
                            <option value="">-- Select --</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{{ old('name') }}">
                    </div>
                    <div class="form-group">
                        <label for="description">Description </label>
                        <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Description">{{ old('description') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="cover">Cover </label>
                        <input type="file" name="cover" id="cover" class="form-control">
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="status" value="0">
                            <input name="status" type="checkbox" value="1">
                            <b> Status</b>
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="feature" value="0">
                            <input name="feature" type="checkbox" value="1">
                            <b> Feature</b>
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="homepage" value="0">
                            <input name="homepage" type="checkbox" value="1">
                            <b> Homepage</b>
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Icon</label>
                        <input type="text" name="icon" id="icon" placeholder="icon" class="form-control" value="{{ old('icon') }}">
                    </div>

                    <div class="form-group">
                        <label for="name">Meta Title</label>
                        <input type="text" name="meta_title" id="meta_title" placeholder="meta title" class="form-control" value="{{ old('meta_title') }}">
                    </div>
                    <div class="form-group">
                            <label for="description">Meta Description </label>
                            <textarea class="form-control" name="meta_desc" id="meta_desc" rows="5" placeholder="170 characters with white space">{{ old('meta_desc') }}</textarea>
                    </div>
                    <div class="form-group">
                            <label for="description">Meta Keywords </label>
                            <textarea class="form-control" name="meta_keywords" id="meta_keywords" rows="5" placeholder="Max 12 phrases separated by comma(,)">{{ old('meta_keywords') }}</textarea>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.categories.index') }}" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
