@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.categories.update', $category->id) }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    <input type="hidden" name="_method" value="put">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="parent">Parent Category</label>
                        <select name="parent" id="parent" class="form-control select2">
                            <option value="0">No parent</option>
                            @foreach($categories as $cat)
                                <option @if($cat->id == $category->parent_id) selected="selected" @endif value="{{$cat->id}}">{{$cat->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{!! $category->name ?: old('name')  !!}">
                    </div>
                    <div class="form-group">
                        <label for="description">Description </label>
                        <textarea class="form-control ckeditor" name="description" id="description" rows="5" placeholder="Description">{!! $category->description ?: old('description')  !!}</textarea>
                    </div>
                    @if(isset($category->cover))
                    <div class="form-group">
                        <img height="100" src="{{ asset("storage/$category->cover") }}" alt=""> <br/>
                        <a onclick="return confirm('Are you sure?')" href="{{ route('admin.category.remove.image', ['category' => $category->id]) }}" class="btn btn-danger">Remove image?</a>
                    </div>
                    @endif
                    <div class="form-group">
                        <label for="cover">Cover </label>
                        <input type="file" name="cover" id="cover" class="form-control">
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="status" value="0">
                            <input name="status" type="checkbox" value="1" {{$category->status == 1?"checked":""}}>
                            <b> Status</b>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="feature" value="0">
                            <input name="feature" type="checkbox" value="1" {{$category->feature == 1?"checked":""}}>
                            <b> Feature</b>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="homepage" value="0">
                            <input name="homepage" type="checkbox" value="1" {{$category->homepage == 1?"checked":""}}>
                            <b> Homepage</b>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Slug <span class="text-danger">*</span></label>
                        <input type="text" name="slug" id="icon" placeholder="slug" class="form-control" value="{{ old('slug',$category->slug) }}">
                    </div>
                    <div class="form-group">
                        <label for="name">Icon <span class="text-danger">*</span></label>
                        <input type="text" name="icon" id="icon" placeholder="icon" class="form-control" value="{{ old('icon',$category->icon) }}">
                    </div>

                    <div class="form-group">
                        <label for="name">Meta Title <span class="text-danger">*</span></label>
                        <input type="text" name="meta_title" id="meta_title" placeholder="meta title" class="form-control" value="{{ old('meta_title',$category->meta_title) }}">
                    </div>
                    <div class="form-group">
                            <label for="description">Meta Description </label>
                            <textarea class="form-control" name="meta_desc" id="meta_desc" rows="5" placeholder="170 characters with white space">{{ old('meta_desc', $category->meta_desc) }}</textarea>
                    </div>
                    <div class="form-group">
                            <label for="description">Meta Keywords </label>
                            <textarea class="form-control" name="meta_keywords" id="meta_keywords" rows="5" placeholder="Max 12 phrases separated by comma(,)">{{ old('meta_keywords', $category->meta_keywords) }}</textarea>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.categories.index') }}" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
