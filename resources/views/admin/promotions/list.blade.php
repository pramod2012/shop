@extends('layouts.admin.app')

@section('content')
<section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          <h3 class="box-title">Promotion</h3><br>
        </div>
        <div class="col-sm-4">
          @include('layouts.errors-and-messages')
          <a class="btn btn-primary" href="{{route('admin.promotions.create')}}">Add new Promotion</a>
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>
      @include('layouts.search', ['route' => route('admin.promotions.index')])
      
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <table id="examplePromotion" class="table table-bordered table-menu dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">ID</th>
                <th width="10%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Name</th>
                <th width="10%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Image</th>
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Status</th>
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Feature</th>
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Promotion of Category</th>
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Order</th>
                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="2" aria-label="Action: activate to sort column ascending">Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($promotions as $promotion)  
                <tr data-promotion-id="{{$promotion->id}}" role="row" class="odd">
                  <td>{{$promotion->id}}</td>
                  <td>{{ $promotion->name }}</td>
                  <td>
                      @if(isset($promotion->image))
                        <img src="{{ $promotion->image }}" alt="" class="img-responsive" height="100">
                      @endif
                  </td>
                   <td>
                     <i class="{{$promotion->status == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                   </td>
                   <td>
                     <i class="{{$promotion->feature == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                   </td>
                   <td>@if($promotion->category_id == !null){{$promotion->category->name}} @endif</td>
                   <td>{{$promotion->order}}</td>
                  <td>
                    <form action="{{ route('admin.promotions.destroy', $promotion['id']) }}" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        {{method_field('DELETE')}}
                        <div class="btn-group">
                            <a href="{{ route('admin.promotions.show', $promotion['id']) }}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> Show</a>
                            <a href="{{ route('admin.promotions.edit', $promotion['id']) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Delete</button>
                          </div>
                    </form>
                  </td>
              </tr>
        @endforeach
            </tbody>
          </table>
          
        </div>
      </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->
    </section>
    <!-- /.content -->

@section('script-sortable')

    <script>
  var apiBase = "{{ url('admin/') }}"

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
      }
  });

  $(document).ready(function () {
    //Initialize Select2 Elements

    $('#examplePromotion tbody').sortable({
      beforeStop: function(e, ui){
        var orders = []
        $(this).find('tr').each(function(obj, i){
          var id = $(i).attr('data-promotion-id')
          if( id ){
            orders.push({
              id: parseInt(id),
              order: obj + 1
            })
          }
        });
        
        if( orders.length > 0){
          $.ajax({
            'method': 'POST',
            'dataType': 'application/json',
            'url': apiBase + '/promotions/save_orders',
            'data': {'promotions': orders},
            'success': function(res){
              console.log(res)
            }
          })
        }
      } 
    })
  });
</script>
@endsection

    
@endsection
