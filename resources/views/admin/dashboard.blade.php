@extends('layouts.admin.app')
@section('content')
    <!-- Main content -->
    <section class="content">
     
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          
        </div>
        <div class="col-sm-4">


          @if(Session::has('success'))
    <div class="alert alert-info fade in">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{Session::get('success')}}</strong>
     </div>
     @endif
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      
      
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{\App\Shop\Posts\Post::count()}}</h3>

              <p>Total Blog Post</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{\App\Shop\Products\Product::count()}}</h3>

              <p>Total Products</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">

              <h3> {{\App\Shop\Categories\Category::count()}}</h3>

              <p>Total Categories</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3>{{\App\Shop\Employees\Employee::count()}}</h3>
              <p>Total Employees</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>
      
     <div class="row">
      
      <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-edit"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"><a href="{{route('admin.posts.index')}}"> <i class="fa fa-edit"></i> Blog</a></span>
              <span class="info-box-text"><a href="{{route('admin.authors.index')}}"> <i class="fa fa-user"></i> Author</a></span>
              <span class="info-box-text"> <a href="{{route('admin.mcategories.index')}}"> <i class="fa fa-bars"></i> Blog Category</a></span>
              <span class="info-box-text"><a href="{{route('admin.tags.index')}}"> <i class="fa fa-tag"></i> Tags</a></span>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-gift"></i></span>

            <div class="info-box-content">
            
            <span class="info-box-text"> <a href="{{route('admin.products.index')}}">Products</a> </span>
            <span class="info-box-text"> <a href="{{route('admin.ratings.index')}}">Ratings</a> </span>
            <span class="info-box-text"> <a href="{{route('admin.categories.index')}}">Product Categories</a> </span>
            <span class="info-box-number">{{\App\Shop\Categories\Category::count()}}</span>
            
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>       <!-- /.col -->
        
        <div class="col-md-3 col-sm-6 col-xs-12">

          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"> <a href="{{route('admin.customers.index')}}">Customers</a> </span>
              <span class="info-box-number">{{\App\Shop\Customers\Customer::count()}}</span>
              <span class="info-box-text"> <a href="{{route('admin.customers.create')}}">Create Customer</a> </span>
              
            </div>
            <!-- /.info-box-content -->
          </div>

          <!-- /.info-box -->
        </div>
        <!-- ./col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-people-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text"> <a href="{{route('admin.employees.index')}}"> Employee</a></span>
              <span class="info-box-number">{{\App\Shop\Employees\Employee::count()}}</span>
              <span class="info-box-text"> <a href="{{route('admin.employees.create')}}">Create Employee</a> </span>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          
          <!-- /.info-box -->
        </div>
        
        <!-- ./col -->
      </div>
      <div class="row">
        <!-- ./col -->
         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-opencart"></i></span>

            <div class="info-box-content">
            
            <span class="info-box-text"> <a href="{{route('admin.orders.index')}}">List of Order</a> </span>
            <span class="info-box-number">{{\App\Shop\Orders\Order::count()}}</span>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-cogs"></i></span>

            <div class="info-box-content">
            <span class="info-box-text"> <a href="{{ route('admin.settings') }}">Settings</a> </span>
            
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
         <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-image"></i></span>
            <div class="info-box-content">
              <span class="info-box-text"> <a href="{{route('admin.sliders.index')}}">Sliders</a> </span>
              <span class="info-box-text"> <a href="{{route('admin.partners.index')}}">Partners</a> </span>
              <span class="info-box-text"> <a href="{{route('admin.promotions.index')}}">Promotions</a> </span>
              <span class="info-box-text"> <a href="{{route('admin.flashes.index')}}">FLash Messages</a> </span>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- ./col -->
  <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-cog"></i></span>

            <div class="info-box-content">
            <span class="info-box-text"> <a href="{{route('admin.attributes.index')}}">Product Attributes</a> </span>
            <span class="info-box-text"><a href="{{route('admin.brands.index')}}">Product Brands</a></span>
              
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

           
         

        
 
       
          
        
      </div>
    </div>
  </div>
  

    </section>
@endsection
