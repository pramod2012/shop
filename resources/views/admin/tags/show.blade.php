@extends('layouts.admin.app')

@section('content')
    <section class="content">
    <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <h4> tag</h4>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="col-md-2">Data</td>
                            <th class="col-md-10">Value</td>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{{$tag->name}}</td>
                    </tr>
                    <tr>
                        <td>Body</td>
                        <td>{{ _display_string($tag->body)}}</td>
                    </tr>
                    <tr>
                        <td>Image</td>
                        <td><img src="{{$tag->image}}"></td>
                    </tr>
                    <tr>
                        <td>Feature</td>
                        <td><i class="{{$tag->feature == 1?"fa fa-check-square":"fa fa-close"}}"></i></td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td><i class="{{$tag->status == 1?"fa fa-check-square":"fa fa-close"}}"></i></td>
                    </tr>
                    
                    <tr>
                        <td>Created At</td>
                        <td>{{$tag->created_at}}</td>
                    </tr>
                    <tr>
                        <td>Updated At</td>
                        <td>{{$tag->updated_at}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box-footer">
                <div class="btn-group">
                    <a href="{{route('admin.tags.index')}}" class="btn btn-default">Back</a>
                    
                </div>
            </div>
    </section>    
  </div>
@endsection
