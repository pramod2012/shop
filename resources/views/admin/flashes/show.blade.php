@extends('layouts.admin.app')

@section('content')
    <section class="content">
    <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <h4> Flash</h4>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="col-md-2">Data</td>
                            <th class="col-md-10">Value</td>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{{$flash->name}}</td>
                    </tr>
                    <tr>
                        <td>Body</td>
                        <td>{!! $flash->body !!}</td>
                    </tr>
                    <tr>
                        <td>Image</td>
                        <td><img src="{{$flash->image}}"></td>
                    </tr>
                    <tr>
                        <td>Feature</td>
                        <td><i class="{{$flash->feature == 1?"fa fa-check-square":"fa fa-close"}}"></i></td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td><i class="{{$flash->status == 1?"fa fa-check-square":"fa fa-close"}}"></i></td>
                    </tr>
                    
                    <tr>
                        <td>Created At</td>
                        <td>{{$flash->created_at}}</td>
                    </tr>
                    <tr>
                        <td>Updated At</td>
                        <td>{{$flash->updated_at}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box-footer">
                <div class="btn-group">
                    <a href="{{route('admin.flashes.index')}}" class="btn btn-default">Back</a>
                    
                </div>
            </div>
    </section>    
@endsection
