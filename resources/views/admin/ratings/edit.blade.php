@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.ratings.update', $rating->id) }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="form-group">
                        <label for="parent">Package</label>
                        <select name="package_id" id="package_id" class="form-control select2">
                            <option value="">-- Select --</option>
@foreach(\App\Shop\Packages\Package::all() as $value)
                                @if ($itinerary->package_id == $value->id)
                                    <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                @else
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{!! $rating->name ?: old('name')  !!}">
                    </div>
                    <div class="form-group">
                        <label for="body">Body </label>
                        <textarea class="form-control ckeditor" name="body" id="body" rows="5" placeholder="Body">{!! $rating->body ?: old('body')  !!}</textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="name">Mobile </label>
                        <input type="text" name="mobile" id="mobile" placeholder="Name" class="form-control" value="{{ old('mobile',$rating->mobile) }}">
                    </div>

                    <div class="form-group">
                        <label for="name">Email</label>
                        <input type="text" name="email" id="mobile" placeholder="Name" class="form-control" value="{{ old('email',$rating->email) }}">
                    </div>

                    <div class="form-group">
                        <label for="name">Subject</label>
                        <input type="text" name="subject" id="mobile" placeholder="Name" class="form-control" value="{{ old('subject',$rating->subject) }}">
                    </div>

                    <div class="form-group">
                        <label for="name">Rating</label>
                        <input type="text" name="rating" id="mobile" placeholder="Name" class="form-control" value="{{ old('rating',$rating->rating) }}">
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="status" value="0">
                            <input name="status" type="checkbox" value="1" {{$rating->status == 1?"checked":""}}>
                            <b> Status</b>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="feature" value="0">
                            <input name="feature" type="checkbox" value="1" {{$rating->feature == 1?"checked":""}}>
                            <b> Feature</b>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Meta Title <span class="text-danger">*</span></label>
                        <input type="text" name="meta_title" id="meta_title" placeholder="meta title" class="form-control" value="{{ old('meta_title',$rating->meta_title) }}">
                    </div>
                    <div class="form-group">
                            <label for="description">Meta Description </label>
                            <textarea class="form-control" name="meta_desc" id="meta_desc" rows="5" placeholder="170 characters with white space">{{ old('meta_desc', $rating->meta_desc) }}</textarea>
                    </div>
                    <div class="form-group">
                            <label for="description">Meta Keywords </label>
                            <textarea class="form-control" name="meta_keywords" id="meta_keywords" rows="5" placeholder="Max 12 phrases separated by comma(,)">{{ old('meta_keywords', $rating->meta_keywords) }}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="name">Slug <span class="text-danger">*</span></label>
                        <input type="text" name="slug" id="slug" placeholder="Slug" class="form-control" value="{!! $rating->slug ?: old('slug')  !!}">
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.ratings.index') }}" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
