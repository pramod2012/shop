@extends('layouts.admin.app')

@section('content')
    <section class="content">
    <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <h4> rating</h4>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="col-md-2">Data</td>
                            <th class="col-md-10">Value</td>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{{$rating->name}}</td>
                    </tr>
                    <tr>
                        <td>Body</td>
                        <td>{!! $rating->body !!}</td>
                    </tr>
                    <tr>
                        <td>Mobile</td>
                        <td>{{$rating->mobile}}</td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>{{$rating->email}}</td>
                    </tr>
                    <tr>
                        <td>Subject</td>
                        <td>{{$rating->subject}}</td>
                    </tr>
                    <tr>
                        <td>Feature</td>
                        <td><i class="{{$rating->feature == 1?"fa fa-check-square":"fa fa-close"}}"></i></td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td><i class="{{$rating->status == 1?"fa fa-check-square":"fa fa-close"}}"></i></td>
                    </tr>
                    
                    <tr>
                        <td>Created At</td>
                        <td>{{$rating->created_at}}</td>
                    </tr>
                    <tr>
                        <td>Updated At</td>
                        <td>{{$rating->updated_at}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box-footer">
                <div class="btn-group">
                    <a href="{{route('admin.ratings.index')}}" class="btn btn-default">Back</a>
                    
                </div>
            </div>
    </section>    
  </div>
@endsection
