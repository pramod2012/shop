@extends('layouts.admin.app')

@section('content')
    <section class="content">
    <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <h4> slider</h4>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="col-md-2">Data</td>
                            <th class="col-md-10">Value</td>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{!! $slider->name !!}</td>
                    </tr>
                    <tr>
                        <td>Body</td>
                        <td>{!! $slider->body !!}</td>
                    </tr>
                    <tr>
                        <td>Image</td>
                        <td><img src="{{$slider->image}}" height="100px"></td>
                    </tr>
                    <tr>
                        <td>Mobile Image</td>
                        <td><img src="{{$slider->mobile_image}}" height="500px"></td>
                    </tr>
                    <tr>
                        <td>Feature</td>
                        <td><i class="{{$slider->feature == 1?"fa fa-check-square":"fa fa-close"}}"></i></td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td><i class="{{$slider->status == 1?"fa fa-check-square":"fa fa-close"}}"></i></td>
                    </tr>
                    
                    <tr>
                        <td>Created At</td>
                        <td>{{$slider->created_at}}</td>
                    </tr>
                    <tr>
                        <td>Updated At</td>
                        <td>{{$slider->updated_at}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box-footer">
                <div class="btn-group">
                    <a href="{{route('admin.sliders.index')}}" class="btn btn-default">Back</a>
                    
                </div>
            </div>
    </section>    
  </div>
@endsection
