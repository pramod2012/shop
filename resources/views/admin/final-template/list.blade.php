@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('layouts.errors-and-messages')
    <!-- Default box -->
        @if($sliders)
            <div class="box">
                <div class="box-body">
                    <h2>sliders</h2>
                    @include('layouts.search', ['route' => route('admin.sliders.index')])
                    <table class="table">
                        <thead>
                            <tr>
                                <td class="col-md-1">Id</td>
                                <td class="col-md-2">Name</td>
                                <td class="col-md-3">Image</td>
                                <td class="col-md-3">Status</td>
                                <td class="col-md-3">Actions</td>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($sliders as $slider)
                            <tr>
                                <td>{{$slider->id}}</td>
                                <td>
                                    <a href="{{ route('admin.sliders.show', $slider->id) }}">{{ $slider->name }}</a></td>
                                <td>
                                    @if(isset($slider->image))
                                        <img src="{{ asset("storage/$slider->image") }}" alt="" class="img-responsive">
                                    @endif
                                </td>
                                <td>@include('layouts.status', ['status' => $slider->status])</td>
                                <td>
                                    <form action="{{ route('admin.sliders.destroy', $slider->id) }}" method="post" class="form-horizontal">
                                        {{ csrf_field() }}
                                        {{method_field('DELETE')}}
                                        <div class="btn-group">
                                            <a href="{{ route('admin.sliders.edit', $slider->id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Delete</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $sliders->links() }}
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        @endif

    </section>
    <!-- /.content -->
@endsection
