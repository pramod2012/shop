@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.sliders.store') }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{{ old('name') }}">
                    </div>

                    <div class="form-group">
                            <label for="description">Body </label>
                            <textarea class="form-control ckeditor" name="body" id="body" rows="5" placeholder="body">{{ old('body') }}</textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="cover">Image </label>
                        <input type="file" name="image" id="imgInp" class="form-control">
                        <img id="blah" width="150px">
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="status" value="0">
                            <input name="status" type="checkbox" value="1">
                            <b> Status</b>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="feature" value="0">
                            <input name="feature" type="checkbox" value="1">
                            <b> Feature</b>
                            </label>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.sliders.index') }}" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
