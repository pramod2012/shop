@extends('layouts.admin.app')

@section('content')
    <section class="content">
    <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <h4> Slider</h4>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="col-md-2">Data</td>
                            <th class="col-md-10">Value</td>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{{$slider->name}}</td>
                    </tr>
                    <tr>
                        <td>Body</td>
                        <td>{{ _display_string($slider->body)}}</td>
                    </tr>
                    <tr>
                        <td>sdf</td>
                        <td><img src="sdfsdf"></td>
                    </tr>
                    <tr>
                        <td>Username</td>
                        <td>sdfsdf</td>
                    </tr>
                    
                    <tr>
                        <td>Created At</td>
                        <td>{{$data->created_at}}</td>
                    </tr>
                    <tr>
                        <td>Updated At</td>
                        <td>{{$data->updated_at}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box-footer">
                <div class="btn-group">
                    <a href="{{route('author.index')}}" class="btn btn-default">Back</a>
                    
                </div>
            </div>
    </section>    
  </div>
@endsection
