@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.posts.update', $post->id) }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    {{ csrf_field() }}
                    {{ method_field('PATCH') }}
                    <div class="form-group">
                        <label for="name">Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{!! $post->name ?: old('name')  !!}">
                    </div>
                    <div class="form-group">
                        <label for="body">Summary </label>
                        <textarea class="form-control" name="summary" id="summary" rows="5" placeholder="summary">{!! $post->summary ?: old('summary')  !!}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="body">Body </label>
                        <textarea class="form-control ckeditor" name="body" id="body" rows="5" placeholder="Body">{!! $post->body ?: old('body')  !!}</textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="cover">Image </label>
                        <input type="file" name="image" id="imgInp" class="form-control">
                        <img id="blah" width="150px" src="{{$post->image}}">
                    </div>
                    <div class="form-group">
                        <label for="permissions">Tags *</label>
                        <select name="tag_id[]" id="tag_id" class="form-control select2" multiple="multiple">
        @foreach(\App\Shop\Tags\Tag::all() as $key => $value)
            @php $check = in_array($value->id, $post->tags->pluck('id')->toArray()) ? 'selected' : ''@endphp
                                
                                <option value="{{ $value->id }}" {{$check}}>{{ $value->name }}</option>
                                
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="permissions">Author *</label>
                        <select name="author_id" id="author_id" class="form-control">
                            <option value=""></option>
                            @foreach(\App\Shop\Authors\Author::all() as $key => $value)
                                @if ($post->author_id == $value->id)
                                <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                @else
                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="permissions">Blog Category *</label>
                        <select name="mcategory_id" id="mcategory_id" class="form-control">
                            <option value=""></option>
                            @foreach(\App\Shop\Mcategories\Mcategory::all() as $key => $value)
                                @if ($post->mcategory_id == $value->id)
                                <option value="{{ $value->id }}" selected>{{ $value->name }}</option>
                                @else
                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="is_blog" value="0">
                            <input name="is_blog" type="checkbox" value="1" {{$post->is_blog == 1?"checked":""}}>
                            <b> Is Blog</b>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="status" value="0">
                            <input name="status" type="checkbox" value="1" {{$post->status == 1?"checked":""}}>
                            <b> Status</b>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="feature" value="0">
                            <input name="feature" type="checkbox" value="1" {{$post->feature == 1?"checked":""}}>
                            <b> Feature</b>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="name">Meta Title <span class="text-danger">*</span></label>
                        <input type="text" name="meta_title" id="meta_title" placeholder="meta title" class="form-control" value="{{ old('meta_title',$post->meta_title) }}">
                    </div>
                    <div class="form-group">
                            <label for="description">Meta Description </label>
                            <textarea class="form-control" name="meta_desc" id="meta_desc" rows="5" placeholder="170 characters with white space">{{ old('meta_desc', $post->meta_desc) }}</textarea>
                    </div>
                    <div class="form-group">
                            <label for="description">Meta Keywords </label>
                            <textarea class="form-control" name="meta_keywords" id="meta_keywords" rows="5" placeholder="Max 12 phrases separated by comma(,)">{{ old('meta_keywords', $post->meta_keywords) }}</textarea>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.posts.index') }}" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
