@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
        @include('layouts.errors-and-messages')
        <div class="box">
            <form action="{{ route('admin.posts.store') }}" method="post" class="form" enctype="multipart/form-data">
                <div class="box-body">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="name">Name <span class="text-danger">*</span></label>
                        <input type="text" name="name" id="name" placeholder="Name" class="form-control" value="{{ old('name') }}">
                    </div>

                    <div class="form-group">
                        <label for="body">Summary </label>
                        <textarea class="form-control" name="summary" id="summary" rows="5" placeholder="summary">{!! old('summary')  !!}</textarea>
                    </div>

                    <div class="form-group">
                            <label for="description">Body </label>
                            <textarea class="form-control ckeditor" name="body" id="body" rows="5" placeholder="body">{{ old('body') }}</textarea>
                    </div>
                    
                    <div class="form-group">
                        <label for="cover">Image </label>
                        <input type="file" name="image" id="imgInp" class="form-control">
                        <img id="blah" width="150px">
                    </div>

                    <div class="form-group">
                        <label for="permissions">Tags *</label>
                        <select name="tag_id[]" id="tag_id" class="form-control select2" multiple="multiple">
                            @foreach(\App\Shop\Tags\Tag::all() as $key => $tag)
                                @if (old('tag_id[$key]') == $tag->id)
                                <option value="{{ $tag->id }}" selected>{{ $tag->name }}</option>
                                @else
                                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="permissions">Author *</label>
                        <select name="author_id" id="author_id" class="form-control">
                            <option value=""></option>
                            @foreach(\App\Shop\Authors\Author::all() as $key => $value)
                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="permissions">Blog Category *</label>
                        <select name="mcategory_id" id="mcategory_id" class="form-control">
                            <option value=""></option>
                            @foreach(\App\Shop\Mcategories\Mcategory::all() as $key => $value)
                                <option value="{{ $value->id }}">{{ $value->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="is_blog" value="0">
                            <input name="is_blog" type="checkbox" value="1">
                            <b> Is Blog</b>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="status" value="0">
                            <input name="status" type="checkbox" value="1">
                            <b> Status</b>
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox">
                            <label>
                            <input type="hidden" name="feature" value="0">
                            <input name="feature" type="checkbox" value="1">
                            <b> Feature</b>
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="name">Meta Title <span class="text-danger">*</span></label>
                        <input type="text" name="meta_title" id="meta_title" placeholder="meta title" class="form-control" value="{{ old('meta_title') }}">
                    </div>
                    <div class="form-group">
                            <label for="description">Meta Description </label>
                            <textarea class="form-control" name="meta_desc" id="meta_desc" rows="5" placeholder="170 characters with white space">{{ old('meta_desc') }}</textarea>
                    </div>
                    <div class="form-group">
                            <label for="description">Meta Keywords </label>
                            <textarea class="form-control" name="meta_keywords" id="meta_keywords" rows="5" placeholder="Max 12 phrases separated by comma(,)">{{ old('meta_keywords') }}</textarea>
                    </div>
                    
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <div class="btn-group">
                        <a href="{{ route('admin.posts.index') }}" class="btn btn-default">Back</a>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
@endsection
