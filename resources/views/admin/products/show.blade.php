@extends('layouts.admin.app')

@section('content')
    <section class="content">
        @include('layouts.errors-and-messages')
    <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <h4> Product</h4>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="col-md-2">Data</td>
                            <th class="col-md-10">Value</td>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{{$product->name}}</td>
                    </tr>
                    <tr>
                        <td>Description</td>
                        <td>{!! $product->description !!}</td>
                    </tr>
                    <tr>
                        <td>Image</td>
                        <td><img src="{{$product->cover}}" height="400px"></td>
                    </tr>

                    <tr>
                        <td>Quantity</td>
                        <td>{{$product->quantity}}</td>
                    </tr>

                    <tr>
                        <td>Price</td>
                        <td>{{ config('cart.currency') }} {{ $product->price }}</td>
                    </tr>
                    
                    
                    <tr>
                        <td>Status</td>
                        <td><i class="{{$product->status == 1?"fa fa-check-square":"fa fa-close"}}"></i></td>
                    </tr>
                    
                    <tr>
                        <td>Created At</td>
                        <td>{{$product->created_at}}</td>
                    </tr>
                    <tr>
                        <td>Updated At</td>
                        <td>{{$product->updated_at}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box-footer">
                <div class="btn-group">
                    <a href="{{route('admin.products.index')}}" class="btn btn-default">Back</a>
                    
                </div>
            </div>
    </section>    
@endsection