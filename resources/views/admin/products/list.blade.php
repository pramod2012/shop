@extends('layouts.admin.app')

@section('content')
<section class="content">
      <div class="box">
  <div class="box-header">
    <div class="row">
        <div class="col-sm-8">
          <h3 class="box-title">Product</h3><br>
        </div>
        <div class="col-sm-4">
          @include('layouts.errors-and-messages')
          <a class="btn btn-primary" href="{{route('admin.products.create')}}">Add new Product</a>
        </div>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>
      @include('layouts.search', ['route' => route('admin.products.index')])
      
    <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
      <div class="row">
        <div class="col-sm-12">
          <table id="exampleProduct" class="table table-bordered table-menu dataTable" role="grid" aria-describedby="example2_info">
            <thead>
              <tr role="row">
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">ID</th>
                <th width="10%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Name</th>
                <th width="10%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Sku</th>
                <th width="10%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Image</th>
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Quantity</th>
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Price</th>
                <th width="5%" class="sorting" tabindex="0" aria-controls="example2" rowspan="1" colspan="1" aria-label="city: activate to sort column ascending">Status</th>
                <th tabindex="0" aria-controls="example2" rowspan="1" colspan="2" aria-label="Action: activate to sort column ascending">Action</th>
              </tr>
            </thead>
            <tbody>
            @foreach ($products as $product)  
                <tr data-product-id="{{$product->id}}" role="row" class="odd">
                  <td>{{$product->id}}</td>
                  <td>{{ $product->name }}</td>
                  <td>{{ $product->sku }}</td>
                  <td>
                      @if(isset($product->cover))
                        <img src="{{$product->cover}}" alt="" class="img-responsive" height="100">
                      @endif
                  </td>
                  <td>{{ $product->quantity }}</td>
                  <td>{{ $product->price }}</td>
                   <td>
                     <i class="{{$product->status == 1?"fa fa-check-square":"fa fa-close"}}"></i>
                   </td>
                   
                  <td>
                    <form action="{{ route('admin.products.destroy', $product['id']) }}" method="post" class="form-horizontal">
                        {{ csrf_field() }}
                        {{method_field('DELETE')}}
                        <div class="btn-group">
@if($admin->hasPermission('view-product'))
                            <a href="{{ route('admin.products.show', $product['id']) }}" class="btn btn-default btn-sm"><i class="fa fa-eye"></i> Show</a>
                            @endif
@if($admin->hasPermission('update-product'))
                            <a href="{{ route('admin.products.edit', $product['id']) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                            @endif
@if($admin->hasPermission('delete-product'))
                            <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm"><i class="fa fa-times"></i> Delete</button>
                            @endif
                          </div>
                    </form>
                  </td>
              </tr>
        @endforeach
            </tbody>
          </table>
          {{ $products->links('vendor.pagination.custom') }}
        </div>
         
      </div>
      </div>
    </div>
  </div>
  <!-- /.box-body -->

    </section>
    <!-- /.content -->

@section('script-sortable')

    <script>
  var apiBase = "{{ url('admin/') }}"

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': "{{ csrf_token() }}"
      }
  });

  $(document).ready(function () {
    //Initialize Select2 Elements

    $('#exampleProduct tbody').sortable({
      beforeStop: function(e, ui){
        var orders = []
        $(this).find('tr').each(function(obj, i){
          var id = $(i).attr('data-product-id')
          if( id ){
            orders.push({
              id: parseInt(id),
              order: obj + 1
            })
          }
        });
        
        if( orders.length > 0){
          $.ajax({
            'method': 'POST',
            'dataType': 'application/json',
            'url': apiBase + '/products/save_orders',
            'data': {'products': orders},
            'success': function(res){
              console.log(res)
            }
          })
        }
      } 
    })
  });
</script>
@endsection

    
@endsection
