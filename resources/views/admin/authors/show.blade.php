@extends('layouts.admin.app')

@section('content')
    <section class="content">
    <!-- Default box -->
        <div class="box">
            <div class="box-body">
                <h4> author</h4>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="col-md-2">Data</td>
                            <th class="col-md-10">Value</td>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Name</td>
                        <td>{{$author->name}}</td>
                    </tr>
                    <tr>
                        <td>Body</td>
                        <td>{{ _display_string($author->body)}}</td>
                    </tr>
                    <tr>
                        <td>Image</td>
                        <td><img src="{{$author->image}}"></td>
                    </tr>
                    <tr>
                        <td>Feature</td>
                        <td><i class="{{$author->feature == 1?"fa fa-check-square":"fa fa-close"}}"></i></td>
                    </tr>
                    <tr>
                        <td>Status</td>
                        <td><i class="{{$author->status == 1?"fa fa-check-square":"fa fa-close"}}"></i></td>
                    </tr>
                    
                    <tr>
                        <td>Created At</td>
                        <td>{{$author->created_at}}</td>
                    </tr>
                    <tr>
                        <td>Updated At</td>
                        <td>{{$author->updated_at}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box-footer">
                <div class="btn-group">
                    <a href="{{route('admin.authors.index')}}" class="btn btn-default">Back</a>
                    
                </div>
            </div>
    </section>    
  </div>
@endsection
