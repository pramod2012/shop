@extends('layouts.admin.app')

@section('content')
    <!-- Main content -->
    <section class="content">
    @include('admin.partials.flash')

    <div class="box">
            
                <div class="box-body">
                    <div class="row">
                        {{ csrf_field() }}
                        <input type="hidden" name="_method" value="put">
                        <div class="col-md-12">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" role="tablist" id="tablist">
                    <li role="presentation" class="nav-item" class="active"><a class="nav-link active" aria-controls="home" href="#general" data-toggle="tab" role="tab">General</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#site-logo" data-toggle="tab">Site Logo</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#footer-seo" data-toggle="tab">Footer &amp; SEO</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#social-links" data-toggle="tab">Social Links</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#analytics" data-toggle="tab">Album Cover</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#payments" data-toggle="tab">Payments</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#quotes" data-toggle="tab">Quote</a></li>
                    <li role="presentation" class="nav-item"><a class="nav-link" href="#new" data-toggle="tab">Email</a></li>

                    <li role="presentation" class="nav-item"><a class="nav-link" href="#work" data-toggle="tab">Work</a></li>
                    
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content" id="tabcontent">

                                <div role="tabpanel" class="tab-pane active" id="general">
                                    <div class="row">
                                        <div class="col-md-8">
                                            @include('admin.settings.includes.general')
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="site-logo">
                                    <div class="row">
                                        <div class="col-md-8">
                                            @include('admin.settings.includes.logo')
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="footer-seo">
                                    <div class="row">
                                        <div class="col-md-8">
                                            @include('admin.settings.includes.footer_seo')
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="social-links">
                                    <div class="row">
                                        <div class="col-md-8">
                                            @include('admin.settings.includes.social_links')
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="analytics">
                                    <div class="row">
                                        <div class="col-md-8">
                                            @include('admin.settings.includes.analytics')
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="payments">
                                    <div class="row">
                                        <div class="col-md-8">
                                            @include('admin.settings.includes.payments')
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="quotes">
                                    <div class="row">
                                        <div class="col-md-8">
                                            @include('admin.settings.includes.quote')
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="new">
                                    <div class="row">
                                        <div class="col-md-8">
                                    @include('admin.settings.includes.email')
                                        </div>
                                    </div>
                                </div>

                                <div role="tabpanel" class="tab-pane" id="work">
                                    <div class="row">
                                        <div class="col-md-8">
                                            @include('admin.settings.includes.work')
                                        </div>
                                    </div>
                                </div>
                                



                            </div>
                        </div>
                    </div>
                </div>
            
        </div>
        <!-- /.box -->
    

    </section>
    <style type="text/css">
        label.checkbox-inline {
            padding: 10px 5px;
            display: block;
            margin-bottom: 5px;
        }
        label.checkbox-inline > input[type="checkbox"] {
            margin-left: 10px;
        }
        ul.attribute-lists > li > label:hover {
            background: #3c8dbc;
            color: #fff;
        }
        ul.attribute-lists > li {
            background: #eee;
        }
        ul.attribute-lists > li:hover {
            background: #ccc;
        }
        ul.attribute-lists > li {
            margin-bottom: 15px;
            padding: 15px;
        }
    </style>
    <!-- /.content -->
@endsection

