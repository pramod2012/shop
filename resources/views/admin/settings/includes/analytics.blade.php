<div class="tile">
    <form action="{{ route('admin.settings.update') }}" method="POST" role="form">
        @csrf
        <h3 class="tile-title">Index Album</h3>
        <hr>
        <div class="tile-body">
            <div class="form-group">
                <label class="control-label" for="google_analytics"> Index Title Album</label>
                <textarea
                    class="form-control"
                    rows="4"
                    placeholder="Index Title Album Cover"
                    id="google_analytics"
                    name="google_analytics"
                >{!! Config::get('settings.google_analytics') !!}</textarea>
            </div>
            <div class="form-group">
                <label class="control-label" for="facebook_pixels">Index Album Description</label>
                <textarea
                    class="form-control"
                    rows="4"
                    placeholder="Index album Description"
                    id="facebook_pixels"
                    name="facebook_pixels"
                >{{ Config::get('settings.facebook_pixels') }}</textarea>
            </div>
            <div class="form-group">
                <label class="control-label" for="quote"> Quote</label>
                <textarea
                    class="form-control"
                    rows="4"
                    placeholder="quote"
                    id="quote"
                    name="quote"
                >{{ Config::get('settings.quote') }}</textarea>
            </div>
            <div class="form-group">
                <label class="control-label" for="quote"> Quote By</label>
                <input
                    class="form-control"
                    rows="4"
                    placeholder="quote by"
                    id="quote_by"
                    name="quote_by" value="{{ Config::get('settings.quote_by') }}"
                >
            </div>
            
            <div class="form-group">
                <label class="control-label" for="quote"> Contactus Message</label>
                <textarea
                    class="form-control"
                    rows="4"
                    placeholder="contactus_message"
                    id="contactus_message"
                    name="contactus_message"
                >{{ Config::get('settings.contactus_message') }}</textarea>
            </div>
            
        </div>
        <div class="tile-footer">
            <div class="row d-print-none mt-2">
                <div class="col-12 text-right">
                    <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Settings</button>
                </div>
            </div>
        </div>
    </form>
</div>
@push('scripts')
    <script>
        loadFile = function(event, id) {
            var output = document.getElementById(id);
            output.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
@endpush
