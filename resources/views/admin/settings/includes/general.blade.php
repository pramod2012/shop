<div class="tile">
    <form action="{{ route('admin.settings.update') }}" method="POST" role="form" enctype="multipart/form-data">
        @csrf
        <h3 class="tile-title">General Settings</h3>
        <hr>
        <div class="tile-body">
            <div class="form-group">
                <label class="control-label" for="site_name">Site Name</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter site name"
                    id="site_name"
                    name="site_name"
                    value="{{ config('settings.site_name') }}"
                />
            </div>
            <div class="form-group">
                <label for="description">About Us </label>
                <textarea class="form-control ckeditor" name="about_us" id="about_us" rows="5" placeholder="Description">{!! config('settings.about_us') !!}</textarea>
            </div>
            <div class="form-group">
                <label class="control-label" for="phone">Phone</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter phone"
                    id="phone"
                    name="phone"
                    value="{{ config('settings.phone') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="description">Description Text</label>
                <textarea
                    class="form-control"
                    rows="4"
                    placeholder="Enter Description text"
                    id="description"
                    name="description"
                >{{ config('settings.description') }}</textarea>
            </div>
            <div class="form-group">
                <label class="control-label" for="location">Location</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter location"
                    id="location"
                    name="location"
                    value="{{ config('settings.location') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="site_title">Site Title</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter site title"
                    id="site_title"
                    name="site_title"
                    value="{{ config('settings.site_title') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="default_email_address">Default Email Address</label>
                <input
                    class="form-control"
                    type="email"
                    placeholder="Enter store default email address"
                    id="email"
                    name="email"
                    value="{{ config('settings.email') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="currency_code">Currency Code</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter store currency code"
                    id="currency_code"
                    name="currency_code"
                    value="{{ config('settings.currency_code') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="currency_symbol">Currency Symbol</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter store currency symbol"
                    id="currency_symbol"
                    name="currency_symbol"
                    value="{{ config('settings.currency_symbol') }}"
                />
            </div>
        </div>
        <div class="form-group">
                <label class="control-label" for="google_iframe">Google Iframe</label>
                <textarea
                    class="form-control"
                    rows="4"
                    placeholder="Enter google iframe"
                    id="google_iframe"
                    name="google_iframe"
                >{{ config('settings.google_iframe') }}</textarea>
            </div>

            <div class="form-group">
                <label class="control-label" for="fax">Fax</label>
                <input
                    class="form-control"
                    rows="4"
                    placeholder="Enter fax"
                    id="fax"
                    name="fax" value="{{ config('settings.fax') }}"
                >
            </div>

            <div class="form-group">
                <label class="control-label" for="fax">Developed By Company Name</label>
                <input
                    class="form-control"
                    rows="4"
                    placeholder="Enter School Name"
                    id="school_name"
                    name="school_name" value="{{ config('settings.school_name') }}"
                >
            </div>
            <div class="form-group">
                <label class="control-label" for="fax">Company Name</label>
                <input
                    class="form-control"
                    rows="4"
                    placeholder="Enter Company Name"
                    id="company_name"
                    name="company_name" value="{{ config('settings.company_name') }}"
                >
            </div>

            <div class="form-group">
                <label class="control-label" for="opening_hours">Opening Hours</label>
                <textarea
                    class="form-control ckeditor"
                    rows="4"
                    placeholder="Enter opening_hours"
                    id="opening_hours"
                    name="opening_hours"
                >{{ config('settings.opening_hours') }}</textarea>
            </div>

            <div class="form-group">
                <div class="col-3">
                    @if (config('settings.company_logo') != null)
                        <img src="{{ asset('storage/'.config('settings.company_logo')) }}" id="logoImg" style="width: 80px; height: auto;">
                    @else
                        <img src="https://via.placeholder.com/80x80?text=Placeholder+Image" id="logoImg" style="width: 80px; height: auto;">
                    @endif
                </div>
                <div class="col-9">
                    <div class="form-group">
                        <label class="control-label">Company Logo</label>
                        <input class="form-control" type="file" name="company_logo" onchange="loadFile(event,'logoImg')"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-3">
                    @if (config('settings.group_photo') != null)
                        <img src="{{ asset('storage/'.config('settings.group_photo')) }}" id="logo3Img" style="width: 80px; height: auto;">
                    @else
                        <img src="https://via.placeholder.com/80x80?text=Placeholder+Image" id="logoImg" style="width: 80px; height: auto;">
                    @endif
                </div>
                <div class="col-9">
                    <div class="form-group">
                        <label class="control-label">Group Photo</label>
                        <input class="form-control" type="file" name="group_photo" onchange="loadFile(event,'logo3Img')"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-3">
                    @if (config('settings.enroll_image') != null)
                        <img src="{{ asset('storage/'.config('settings.enroll_image')) }}" id="logo1Img" style="width: 80px; height: auto;">
                    @else
                        <img src="https://via.placeholder.com/80x80?text=Placeholder+Image" id="logo1Img" style="width: 80px; height: auto;">
                    @endif
                </div>
                <div class="col-9">
                    <div class="form-group">
                        <label class="control-label">Enroll Image</label>
                        <input class="form-control" type="file" name="enroll_image" onchange="loadFile(event,'logo1Img')"/>
                    </div>
                </div>
            </div>

        <div class="tile-footer">
            <div class="row d-print-none mt-2">
                <div class="col-12 text-right">
                    <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Settings</button>
                </div>
            </div>
        </div>
    </form>
</div>
