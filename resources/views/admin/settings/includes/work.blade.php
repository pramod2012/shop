<div class="tile">
    <form action="{{ route('admin.settings.update') }}" method="POST" role="form" enctype="multipart/form-data">
        @csrf
        <h3 class="tile-title">Work</h3>
        <hr>
        <div class="tile-body">
            <div class="form-group">
                <label class="control-label" for="work_title">Subscribe Title</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter work_title"
                    id="work_title"
                    name="work_title"
                    value="{{ config('settings.work_title') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="quote_button_name">work_subtitle</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter work_subtitle"
                    id="work_subtitle"
                    name="work_subtitle"
                    value="{{ config('settings.work_subtitle') }}"
                />
            </div>

            <div class="row">
                <div class="col-3">
                    @if (config('settings.work_image') != null)
                        <img src="{{ asset('storage/'.config('settings.work_image')) }}" id="work_image" style="width: 80px; height: auto;">
                    @else
                        <img src="https://via.placeholder.com/80x80?text=Placeholder+Image" id="work_image" style="width: 80px; height: auto;">
                    @endif
                </div>
                <div class="col-9">
                    <div class="form-group">
                        <label class="control-label">work_image</label>
                        <input class="form-control" type="file" name="work_image" onchange="loadFile(event,'work_image')"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="tile-footer">
            <div class="row d-print-none mt-2">
                <div class="col-12 text-right">
                    <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Settings</button>
                </div>
            </div>
        </div>
    </form>
</div>
