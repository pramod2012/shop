<div class="tile">
    <form action="{{ route('admin.settings.update') }}" method="POST" role="form">
        @csrf
        <h3 class="tile-title">Quote</h3>
        <hr>
        <div class="tile-body">
            <div class="form-group">
                <label class="control-label" for="quote">Quote</label>
                <textarea
                    class="form-control"
                    rows="4"
                    placeholder="Enter quote"
                    id="quote"
                    name="quote"
                >{{ config('settings.quote') }}</textarea>
            </div>
            <div class="form-group">
                <label class="control-label" for="quote_by">Quote By</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter quote by"
                    id="quote_by"
                    name="quote_by"
                    value="{{ config('settings.quote_by') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="quote_button_name">Quote Button Name</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter quote_button_name"
                    id="quote_button_name"
                    name="quote_button_name"
                    value="{{ config('settings.quote_button_name') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="quote_link">Quote Link</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter quote_link"
                    id="quote_link"
                    name="quote_link"
                    value="{{ config('settings.quote_link') }}"
                />
            </div>
        </div>
        <div class="tile-footer">
            <div class="row d-print-none mt-2">
                <div class="col-12 text-right">
                    <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Settings</button>
                </div>
            </div>
        </div>
    </form>
</div>
