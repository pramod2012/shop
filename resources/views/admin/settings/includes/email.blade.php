<div class="tile">
    <form action="{{ route('admin.settings.update') }}" method="POST" role="form">
        @csrf
        <h3 class="tile-title">Email Settings</h3>
        <hr>
        <div class="tile-body">
            <div class="form-group">
                <label class="control-label" for="mail_mailer">mail_mailer</label>
                <input class="form-control" type="text" placeholder="Enter mail mailer" id="mail_mailer" name="mail_mailer" value="{{ config('settings.mail_mailer') }}"
                />
            </div>
            
            
            <div class="form-group">
                <label class="control-label" for="location">mail_host </label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter mail_host"
                    id="mail_host"
                    name="mail_host"
                    value="{{ config('settings.mail_host') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="mail_port">mail_port</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter mail_port"
                    id="mail_port"
                    name="mail_port"
                    value="{{ config('settings.mail_port') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="mail_username">mail_username</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter mail_username"
                    id="mail_username"
                    name="mail_username"
                    value="{{ config('settings.mail_username') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="mail_password">mail_password</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter mail_password"
                    id="mail_password"
                    name="mail_password"
                    value="{{ config('settings.mail_password') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="mail_encryption">mail_encryption</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter mail_encryption"
                    id="mail_encryption"
                    name="mail_encryption"
                    value="{{ config('settings.mail_encryption') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="mail_from_address">mail_from_address</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter mail_from_address"
                    id="mail_from_address"
                    name="mail_from_address"
                    value="{{ config('settings.mail_from_address') }}"
                />
            </div>
            <div class="form-group">
                <label class="control-label" for="mail_from_name">mail_from_name</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter mail_from_name"
                    id="mail_from_name"
                    name="mail_from_name"
                    value="{{ config('settings.mail_from_name') }}"
                />
            </div>

            <div class="form-group">
                <label class="control-label" for="mail_from_email">mail_from_email</label>
                <input
                    class="form-control"
                    type="text"
                    placeholder="Enter mail_from_email"
                    id="mail_from_email"
                    name="mail_from_email"
                    value="{{ config('settings.mail_from_email') }}"
                />
            </div>
        </div>
        
        <div class="tile-footer">
            <div class="row d-print-none mt-2">
                <div class="col-12 text-right">
                    <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Settings</button>
                </div>
            </div>
        </div>
    </form>
</div>
