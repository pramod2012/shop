<div class="tile">
    <form action="{{ route('admin.settings.update') }}" method="POST" role="form" enctype="multipart/form-data">
        @csrf
        <h3 class="tile-title">Site Logo</h3>
        <hr>
        <div class="tile-body">
            <div class="form-group">
                <div class="col-3">
                    @if (config('settings.logo') != null)
                        <img src="{{ asset('storage/'.config('settings.logo')) }}" id="logoImg" style="width: 80px; height: auto;">
                    @else
                        <img src="https://via.placeholder.com/80x80?text=Placeholder+Image" id="logoImg" style="width: 80px; height: auto;">
                    @endif
                </div>
                <div class="col-9">
                    <div class="form-group">
                        <label class="control-label">Site Logo</label>
                        <input class="form-control" type="file" name="logo" onchange="loadFile(event,'logoImg')"/>
                    </div>
                </div>
            </div>
            <div class="form-group mt-4">
                <div class="col-3">
                    @if (config('settings.favicon') != null)
                        <img src="{{ asset('storage/'.config('settings.favicon')) }}" id="faviconImg" style="width: 80px; height: auto;">
                    @else
                        <img src="https://via.placeholder.com/80x80?text=Placeholder+Image" id="faviconImg" style="width: 80px; height: auto;">
                    @endif
                </div>
                <div class="col-9">
                    <div class="form-group">
                        <label class="control-label">Site Favicon</label>
                        <input class="form-control" type="file" name="favicon" onchange="loadFile(event,'faviconImg')"/>
                    </div>
                </div>
            </div>
            <div class="form-group mt-4">
                <div class="col-3">
                    @if (config('settings.error_image') != null)
                        <img src="{{ asset('storage/'.config('settings.error_image')) }}" id="error_imageImg" style="width: 80px; height: auto;">
                    @else
                        <img src="https://via.placeholder.com/80x80?text=Placeholder+Image" id="error_imageImg" style="width: 80px; height: auto;">
                    @endif
                </div>
                <div class="col-9">
                    <div class="form-group">
                        <label class="control-label">Page Image</label>
                        <input class="form-control" type="file" name="error_image" onchange="loadFile(event,'error_imageImg')"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-3">
                    @if (config('settings.footer_image') != null)
                        <img src="{{ asset('storage/'.config('settings.footer_image')) }}" id="footer_imageImg" style="width: 80px; height: auto;">
                    @else
                        <img src="https://via.placeholder.com/80x80?text=Placeholder+Image" id="footer_imageImg" style="width: 80px; height: auto;">
                    @endif
                </div>
                <div class="col-9">
                    <div class="form-group">
                        <label class="control-label">Footer Image</label>
                        <input class="form-control" type="file" name="footer_image" onchange="loadFile(event,'footer_imageImg')"/>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-3">
                    @if (config('settings.contactus_image') != null)
                        <img src="{{ asset('storage/'.config('settings.contactus_image')) }}" id="contactus_imageImg" style="width: 80px; height: auto;">
                    @else
                        <img src="https://via.placeholder.com/80x80?text=Placeholder+Image" id="contactus_imageImg" style="width: 80px; height: auto;">
                    @endif
                </div>
                <div class="col-9">
                    <div class="form-group">
                        <label class="control-label">OG Image</label>
                        <input class="form-control" type="file" name="contactus_image" onchange="loadFile(event,'contactus_imageImg')"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="tile-footer">
            <div class="row d-print-none mt-2">
                <div class="col-12 text-right">
                    <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Update Settings</button>
                </div>
            </div>
        </div>
    </form>
</div>
@push('scripts')
    <script>
        loadFile = function(event, id) {
            var output = document.getElementById(id);
            output.src = URL.createObjectURL(event.target.files[0]);
        };
    </script>
@endpush
