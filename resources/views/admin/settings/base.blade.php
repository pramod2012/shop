@extends('layouts.admin.app')
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
      Setting
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Details</a></li>
        <li class="active">Setting</li>
      </ol>
    </section>
    @yield('content')
    <!-- /.content -->
  </div>
@endsection