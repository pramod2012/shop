@extends('frontend.includes.main')
@section('title')
    Reset Password | {{config('settings.site_title')}}
@endsection
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="ps-breadcrumb__list">
                    <li class="active"><a href="{{route('index')}}">Home</a></li>
                    <li><a href="javascript:void(0);">Reset Password</a></li>
                </ul>
            </div>
        </div>
        <section class="section--login">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="login__box">
                            <div class="login__header">
                                <h3 class="login__login">Reset Password</h3>
                            </div>
                            @include('layouts.errors-and-messages')
                @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
            <form action="{{ route('password.email') }}" method="post" class="form-horizontal">
                {{ csrf_field() }}
                            <div class="login__content">
                                <div class="login__label">Reset Password.</div>
                                <div class="form-group--block">
                                    <input name="email" class="form-control" type="email" placeholder="Username/ Email">
                                </div>
                                
                                
                                <button class="btn btn-login" type="submit">Send Password Reset Link</button>
                    </form>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <h3 class="login__title">Advantages Of Becoming A Member</h3>
                        <p class="login__description"> <b> Buyer Protection </b>has you covered from click to delivery.<br>Sign up or sign in and you will be able to: </p>
                        <div class="login__orther">
                            <p> <i class="icon-truck"></i>Easily Track Orders, Hassle free Returns</p>
                            <p> <i class="icon-alarm2"></i>Get Relevant Alerts and Recommendation</p>
                            <p><i class="icon-star"></i>Wishlist, Reviews, Ratings and more.</p>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </main>
    @include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection