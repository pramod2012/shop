@section('title')
    Register | {{config('settings.site_title')}}
@endsection
@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="ps-breadcrumb__list">
                    <li class="active"><a href="{{route('index')}}">Home</a></li>
                    <li><a href="javascript:void(0);">Register</a></li>
                </ul>
            </div>
        </div>
        <section class="section--registration">
            <div class="container">
                <h2 class="page__title">Registration and Login</h2>
                <p class="page__subtitle">Get started by just filling out one simple form</p>
                <div class="registration__content">
                    <div class="row">
                        <div class="col-12 col-lg-7">
                            <div class="registration__info">
                                <h3 class="registration__title">Registration Form</h3>
                                <form action="{{ route('register') }}" method="POST" role="form">
                                    @csrf
                                    <div class="form-row">
                                        <div class="col-12 col-lg-6 form-group--block">
                                            <label>Full name: <span>*</span></label>
                                            <input name="name" class="form-control @if ($errors->has('name')) is-invalid @endif" type="text">
                                            @if ($errors->has('name'))
                                            <div class="invalid-feedback">Please type your full name!</div>
                                            @endif
                                        </div>
                                        <div class="col-12 col-lg-6 form-group--block">
                                            <label>Email<span>*</span></label>
                                            <input name="email" class="form-control @if ($errors->has('email')) is-invalid @endif" type="email">
                                    @if ($errors->has('email'))
                                            <div class="invalid-feedback">Please enter your email!</div>
                                            @endif
                                        </div>
                                        <div class="col-12 form-group--block">
                                            <label>Password: <span>*</span></label>
                                            <input name="password" class="form-control @if ($errors->has('password')) is-invalid @endif" type="password">
                                            @if ($errors->has('password'))
                                            <div class="invalid-feedback">{{ $errors->first('password') }}</div>
                                            @endif
                                        </div>
                                        <div class="col-12 form-group--block">
                                            <label>Retype password: <span>*</span></label>
                                            <input name="password_confirmation" class="form-control @if ($errors->has('password_confirmation')) is-invalid @endif" type="password">
                                            @if ($errors->has('password_confirmation'))
                                            <div class="invalid-feedback">{{ $errors->first('password_confirmation') }}</div>
                                            @endif
                                        </div><br>
                                        <div class="col-12 form-group--block">
                                        <label> </label>
                                        <p>Already a Member ? <a href="{{route('login')}}"><span class="text-success">Login Here .</span> By creating an account, you agree to Farmart's <span class="text-success">Conditions of Use </span>and <span class="text-success">Privacy Notice.</span> </a></p>
                                            <button type="submit" class="btn ps-button">Register</button>
                                        </div>
                                        
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-12 col-lg-5">
                            <h3 class="login__title">Advantages Of Becoming A Member</h3>
                        <p class="login__description"> <b> Buyer Protection </b>has you covered from click to delivery.<br>Sign up or sign in and you will be able to: </p>
                        <div class="login__orther">
                            <p> <i class="icon-truck"></i> Easily Track Orders, Hassle free Returns</p>
                            <p> <i class="icon-alarm2"></i> Get Relevant Alerts and Recommendation</p>
                            <p><i class="icon-star"></i> Wishlist, Reviews, Ratings and more.</p>
                            
                            
                            

                            
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
@include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection
    