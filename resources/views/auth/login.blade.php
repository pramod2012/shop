@section('title')
    Login | {{config('settings.site_title')}}
@endsection
@extends('frontend.includes.main')
@section('content')
@include('frontend.includes.header')
    <main class="no-main">
        <div class="ps-breadcrumb">
            <div class="container">
                <ul class="ps-breadcrumb__list">
                    <li class="active"><a href="{{route('index')}}">Home</a></li>
                    <li><a href="javascript:void(0);">Login</a></li>
                </ul>
            </div>
        </div>
        <section class="section--login">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-lg-6">
                        <div class="login__box">
                            <div class="login__header">
                                <h3 class="login__login">LOGIN</h3>
                            </div>
                            @include('layouts.errors-and-messages')
            <form action="{{ route('login') }}" method="post" class="form-horizontal">
                {{ csrf_field() }}
                            <div class="login__content">
                                <div class="login__label">Login to your account.</div>
                                <div class="form-group--block">
                                    <input name="email" class="form-control" type="email" placeholder="Username/ Email">
                                </div>
                                <div class="input-group group-password">
                                    <input name="password" class="form-control" type="password" placeholder="Password">
                                    <div class="input-group-append">
                        <a href="{{route('password.request')}}">
                                        <button class="btn forgot-pass" type="button">Forgot?</button>
                                    </a>
                                    </div>
                                </div>
                            <p>New Member ? <span class="text-success"><a href="{{route('register')}}">Register Here </a></span></p>
                                
                                <button class="btn btn-login" type="submit">Login</button>
                    </form>
                                <div class="login__conect">
                                    <hr>
                                    <p>Or login with</p>
                                    <hr>
                                </div>
                                
                                <a href="/login/facebook" class="btn btn-social btn-facebook"> <i class="fa fa-facebook-f"></i>Login with Facebook</a>
                                <a href="/login/google" class="btn btn-social btn-google"> <i class="fa fa-google-plus"></i>Login with Google</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <h3 class="login__title">Advantages Of Becoming A Member</h3>
                        <p class="login__description"> <b> Buyer's Protection </b>of data and Top Offers and notifications for free.<br>Sign up or sign in and you will be able to: </p>
                        <div class="login__orther">
                            <p> <i class="icon-truck"></i>Easily Track Orders, Hassle free Returns</p>
                            <p> <i class="icon-alarm2"></i>Get Relevant Alerts and Recommendation</p>
                            <p><i class="icon-star"></i>Wishlist, Reviews, Ratings and more.</p>
                        </div>
                        
                    </div>
                </div>
            </div>
        </section>
    </main>
    @include('frontend.includes.footer')
@include('frontend.includes.mobile-view')

@endsection
    