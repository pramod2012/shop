<table>
    <thead>
    <tr>
        <th>Date</th>
        <th>Customer</th>
        <th>Product</th>
        <th>Address</th>
        <th>Payment</th>
        <th>Phone</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        <tr>
            <td>{{ $order->created_at }}</td>
            <td>{{ $order->customer->name }}</td>
            <td>@foreach($order->products as $product)SKU: {{$product->sku}} <br>
            	Name: {{$product->name}} <br>
            	Qty: {{$product->pivot->quantity}} <br>
            	Price: {{$product->price}} <br>
            	Total: {{number_format($product->price * $product->pivot->quantity, 2)}} <br>@endforeach</td>
            <td>{{@$order->address->address_1}} {{@$order->address->address_2}} {{@$order->address->city}} {{@$order->address->zip}} {{ @$order->address->country->name }}</td>
            <td>{{ $order->payment }}</td>
            <td>{{@$order->address->phone}}</td>
            <td>{{ config('cart.currency') }} {{ $order->total }}</td>
        </tr>
    @endforeach
    </tbody>
</table>