<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('sku');
            $table->string('name');
            $table->string('slug');
            $table->bigInteger('views')->default(0);
            $table->text('description')->nullable();
            $table->string('cover')->nullable();
            $table->integer('quantity');
            $table->decimal('price',18,2)->nullable();
            $table->decimal('sale_price',18,2)->nullable();
            $table->decimal('discount',18,2)->nullable();
            $table->integer('status')->default(0);
            $table->decimal('length',18,2)->nullable();
            $table->decimal('width',18,2)->nullable();
            $table->decimal('height',18,2)->nullable();
            $table->string('distance_unit')->nullable();
            $table->decimal('weight',18,2)->default(0)->nullable();
            $table->string('mass_unit')->nullable();
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
