<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Employee;
use App\Models\Permission;
use App\Models\Role;
use App\Shop\Roles\Repositories\RoleRepository;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createProductPerm = Permission::factory()->create([
            'name' => 'create-product',
            'display_name' => 'Create product'
        ]);

        $viewProductPerm = Permission::factory()->create([
            'name' => 'view-product',
            'display_name' => 'View product'
        ]);

        $updateProductPerm = Permission::factory()->create([
            'name' => 'update-product',
            'display_name' => 'Update product'
        ]);

        $deleteProductPerm = Permission::factory()->create([
            'name' => 'delete-product',
            'display_name' => 'Delete product'
        ]);

        $updateOrderPerm = Permission::factory()->create([
            'name' => 'update-order',
            'display_name' => 'Update order'
        ]);

        $employee = Employee::factory()->create([
            'email' => 'john@doe.com'
        ]);

        $employee = Employee::factory()->create([
            'email' => 'admin@doe.com'
        ]);

        $employee = Employee::factory()->create([
            'email' => 'clerk@doe.com'
        ]);

        $super = Role::factory()->create([
            'name' => 'superadmin',
            'display_name' => 'Super Admin'
        ]);

        $admin = Role::factory()->create([
            'name' => 'admin',
            'display_name' => 'Admin'
        ]);

        $clerk = Role::factory()->create([
            'name' => 'clerk',
            'display_name' => 'Clerk'
        ]);

        \DB::table('role_user')->insert(array (
            0 =>
                array (
                    'role_id' => 1,
                    'user_id' => 1,
                    'user_type' => 'App\Shop\Employees\Employee',
                ),
            1 =>
               array (
                    'role_id' => 2,
                    'user_id' => 2,
                    'user_type' => 'App\Shop\Employees\Employee',
                ),
            2 =>
                array (
                    'role_id' => 3,
                    'user_id' => 3,
                    'user_type' => 'App\Shop\Employees\Employee',
                ),
        ));

        \DB::table('permission_role')->insert(array (
            0 =>
                array (
                    'role_id' => 1,
                    'permission_id' => 1,
                ),
            1 =>
               array (
                    'role_id' => 2,
                    'permission_id' => 1,
                ),
            2 =>
                array (
                    'role_id' => 3,
                    'permission_id' => 1,
                ),
            3 =>
                array (
                    'role_id' => 1,
                    'permission_id' => 2,
                ),
            4 =>
                array (
                    'role_id' => 2,
                    'permission_id' => 2,
                ),
            5 =>
                array (
                    'role_id' => 3,
                    'permission_id' => 2,
                ),
            6 =>
                array (
                    'role_id' => 1,
                    'permission_id' => 3,
                ),
            7 =>
                array (
                    'role_id' => 2,
                    'permission_id' => 3,
                ),
            8 =>
                array (
                    'role_id' => 3,
                    'permission_id' => 3,
                ),
            9 =>
                array (
                    'role_id' => 1,
                    'permission_id' => 4,
                ),
            10 =>
                array (
                    'role_id' => 2,
                    'permission_id' => 4,
                ),
            11 =>
                array (
                    'role_id' => 1,
                    'permission_id' => 5,
                ),
            12 =>
                array (
                    'role_id' => 2,
                    'permission_id' => 5,
                ),
        ));
    }
}
