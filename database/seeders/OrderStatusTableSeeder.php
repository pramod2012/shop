<?php

namespace Database\Seeders;
use App\Shop\OrderStatuses\OrderStatus;

use Illuminate\Database\Seeder;

class OrderStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::create([
            'name' => 'paid',
            'color' => 'green'
            ]);
        OrderStatus::create([
            'name' => 'pending',
            'color' => 'yellow'
            ]);
        OrderStatus::create([
            'name' => 'error',
            'color' => 'red'
            ]);
        OrderStatus::create([
            'name' => 'on-delivery',
            'color' => 'blue'
            ]);
        OrderStatus::create([
            'name' => 'ordered',
            'color' => 'violet'
            ]);
    }
}
