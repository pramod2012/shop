<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Setting;

class SettingsTableSeeder extends Seeder
{
	/**
     * @var array
     */
    protected $settings = [
        [
            'key'                       =>  'site_name',
            'value'                     =>  'Ecommerce Application',
        ],
        [
            'key'                       =>  'site_title',
            'value'                     =>  'Ecommerce CMS',
        ],
        [
            'key'                       =>  'email',
            'value'                     =>  'info@brightlandschool.edu.np',
        ],
        [
            'key'                       =>  'currency_code',
            'value'                     =>  'Rs.',
        ],
        [
            'key'                       =>  'currency_symbol',
            'value'                     =>  '£',
        ],
        [
            'key'                       =>  'location',
            'value'                     =>  'Nepalgunj-10, Banke',
        ],
        [
            'key'                       =>  'company_name',
            'value'                     =>  'Skyfall Technologies Pvt. Ltd.',
        ],
        [
            'key'                       =>  'error_image',
            'value'                     =>  'img/dummy.jpg',
        ],
        [
            'key'                       =>  'description',
            'value'                     =>  'This is ecommerce desctiption',
        ],
        [
            'key'                       =>  'about_us',
            'value'                     =>  'This is ecommerce about us',
        ],
        [
            'key'                       =>  'phone',
            'value'                     =>  '+977- 81-521324',
        ],
        [
            'key'                       =>  'fax',
            'value'                     =>  '+977- 34433434',
        ],
        [
            'key'                       =>  'logo',
            'value'                     =>  'img/logo.png',
        ],
        [
            'key'                       =>  'favicon',
            'value'                     =>  'img/favicon.png',
        ],
        [
            'key'                       =>  'footer_text',
            'value'                     =>  'Copyright © 2021 Brightland School. All Rights Reserved | Site Developed by Paradise IT Solution',
        ],
        [
            'key'                       =>  'footer_image',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'seo_meta_title',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'seo_meta_description',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'facebook',
            'value'                     =>  'https://www.facebook.com/brightlandofficial',
        ],
        [
            'key'                       =>  'twitter',
            'value'                     =>  '#',
        ],
        [
            'key'                       =>  'instagram',
            'value'                     =>  '#',
        ],
        [
            'key'                       =>  'linkedin',
            'value'                     =>  '#',
        ],
        [
            'key'                       =>  'google_analytics',
            'value'                     =>  'We are Gleam a small and enthusiastic photography',
        ],
        [
            'key'                       =>  'facebook_pixels',
            'value'                     =>  'We love photography and travel for meeting new beautiful people all over the world. Propriae voluptaria dissentias nam ei, posse diceret inciderint cum ut, gubergren sadipscing ei vim. Ancillae torquatos in n',
        ],
        [
            'key'                       =>  'stripe_payment_method',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'stripe_key',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'stripe_secret_key',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'paypal_payment_method',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'paypal_client_id',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'paypal_secret_id',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'quote',
            'value'                     =>  '" The creativity and talent of the Gleam team was amazing. Pro in hinc exerci gloriatur, ius ut agam consectetuer, quo te euismod corrumpit. "',
        ],
        [
            'key'                       =>  'quote_by',
            'value'                     =>  'Pramod BHandari',
        ],
        [
            'key'                       =>  'quote_button_name',
            'value'                     =>  'Click Here',
        ],
        [
            'key'                       =>  'quote_link',
            'value'                     =>  '#quote_link',
        ],
        [
            'key'                       =>  'work_title',
            'value'                     =>  '#work title',
        ],
        [
            'key'                       =>  'work_subtitle',
            'value'                     =>  '#work subtitle',
        ],
        [
            'key'                       =>  'work_image',
            'value'                     =>  '#work subtitle',
        ],
        [
            'key'                       =>  'contactus_message',
            'value'                     =>  'We love meeting new people. If you want to work with us send a message',
        ],
        [
            'key'                       =>  'google_iframe',
            'value'                     =>  '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3024.9762977047603!2d-73.96009086693977!3d40.69651965846399!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25bc1f922373f%3A0x398427d98fba822e!2sPark+Ave%2C+Brooklyn%2C+NY%2C+USA!5e0!3m2!1sen!2sro!4v1471514814808">
                    </iframe>',
        ],
        [
            'key'                       =>  'quote_image',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'contactus_image',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'mail_mailer',
            'value'                     =>  'smtp',
        ],
        [
            'key'                       =>  'mail_host',
            'value'                     =>  'smtp.mailtrap.io',
        ],
        [
            'key'                       =>  'mail_port',
            'value'                     =>  '2525',
        ],
        [
            'key'                       =>  'mail_username',
            'value'                     =>  'c2da9a4e599161',
        ],
        [
            'key'                       =>  'mail_password',
            'value'                     =>  'de504ec8d5c23a',
        ],
        [
            'key'                       =>  'mail_encryption',
            'value'                     =>  'null',
        ],
        [
            'key'                       =>  'mail_from_address',
            'value'                     =>  'null',
        ],
        [
            'key'                       =>  'mail_from_name',
            'value'                     =>  'Gyalchen Sherpa',
        ],
        [
            'key'                       =>  'mail_from_email',
            'value'                     =>  'hr@gyalchen.com',
        ],
        [
            'key'                       =>  'group_photo',
            'value'                     =>  '#',
        ],
        [
            'key'                       =>  'opening_hours',
            'value'                     =>  '+977-34433434',
        ],
        [
            'key'                       =>  'school_name',
            'value'                     =>  '+977-34433434',
        ],

        [
            'key'                       =>  'company_logo',
            'value'                     =>  '',
        ],
        [
            'key'                       =>  'enroll_image',
            'value'                     =>  '',
        ],
    ];

    public function run()
    {
        foreach ($this->settings as $index => $setting)
        {
            $result = Setting::create($setting);
            if (!$result) {
                $this->command->info("Insert failed at record $index.");
                return;
            }
        }
        $this->command->info('Inserted '.count($this->settings). ' records');
    }
}
